import Webpack from 'webpack';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import WebpackHotMiddleware from 'webpack-hot-middleware';
import * as Path from 'path';


export default (app, config) => {
    const compiler = Webpack(config);

    const middleware = WebpackDevMiddleware(compiler, {
        logLevel: 'warn',
        publicPath: config.output.publicPath,
        stats: 'errors-only',
    });

    app.use(middleware);
    app.use(WebpackHotMiddleware(compiler, {
        heartbeat: 2000,
    }));

    const fs = middleware.fileSystem;

    app.get('*', (req, res) => {
        fs.readFile(Path.join(compiler.outputPath, 'index.html'), (err, file) => {
            if (err) {
                res.sendStatus(404);
            } else {
                res.send(file.toString());
            }
        });
    });
};
