import addDevMiddlwares from './addDevMiddlewares';
import addProdMiddlwares from './addProdMiddleware';
import devConfig from '../../internals/webpack/webpack.dev.babel.js';
import prodConfig from '../../internals/webpack/webpack.prod.babel';

export default (app) => {
    const isProduction = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test';

    if (isProduction) {
        addProdMiddlwares(app, prodConfig);
    } else {
        addDevMiddlwares(app, devConfig);
    }
};
