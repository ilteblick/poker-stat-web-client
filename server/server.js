import Express from 'express';

import setup from './middlewares/frontendMiddleware.js';

// TODO make real port
const port = process.env.PORT || 3000;
const app = new Express();


setup(app);
app.listen(port, 'localhost');
