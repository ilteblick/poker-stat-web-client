# Vcode Files

Needed settings for vscode

## launch.json

    {
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Chrome",
            "type": "chrome",
            "request": "launch",
            "url": "http://localhost:3000",
            "webRoot": "${workspaceRoot}",
            "sourceMaps": true,
            "trace": true,
            "sourceMapPathOverrides": {
                "webpack:///./*": "${webRoot}/*"
            }
        }
    ]
}

## settings.json

{
    "files.exclude": {
        "node_modules/": true,
        "dist/": true,
        "lib/": true,
        "typings/": true
    },
    "javascript.validate.enable": false,
    "eslint.autoFixOnSave": true,
    "javascript.implicitProjectConfig.checkJs": true,
    "tslint.autoFixOnSave": true,
    "typescript.reportStyleChecksAsWarnings": false,
}

## scripts to start
    npm i
    npm start
    f5 in VScode
