import Redux, { createStore, applyMiddleware } from "redux";
import { routerMiddleware } from "react-router-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { fromJS } from "immutable";
import createSagaMiddleware from "redux-saga";

import { IStore } from "./types/IStore";
import createReducerImported from "./reducers";

let createReducer = createReducerImported;

const sagaMiddleware = createSagaMiddleware();

export default <T>(initialState: object = {}, history: any): IStore<T> => {
    const middlewares: Redux.Middleware[] = [
        routerMiddleware(history),
        sagaMiddleware
    ];

    const composeEnhancers = composeWithDevTools({});

    const store: IStore<T> = createStore(
        createReducer(),
        fromJS(initialState),
        composeEnhancers(applyMiddleware(...middlewares)),
    );

    store.runSaga = sagaMiddleware.run;
    store.injectedReducers = {};
    store.injectedSagas = {};

    if (module.hot) {
        module.hot.accept("./reducers", () => {
            import("./reducers").then((result) => {
                createReducer = result.default;
                store.replaceReducer(createReducer(store.injectedReducers));
            });
        });
    }

    return store;
};
