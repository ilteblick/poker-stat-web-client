import { Action } from "../../basicClasses";

export interface ILyfecycle {
    RESOLVED: string;
    REJECTED: string;
    PENDING: string;
}

export function resolve(lifecycle: ILyfecycle, result: Object): Action {
    return {
        type: lifecycle.RESOLVED,
        payload: result,
    };
}

export function reject(lifecycle: ILyfecycle, error: Error): Action {
    return {
        type: lifecycle.REJECTED,
        error,
    };
}

export function pending(lifecycle: ILyfecycle): Action {
    return {
        type: lifecycle.PENDING,
    };
}
