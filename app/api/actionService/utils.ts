import { ILyfecycle } from "./actions";
import { IRequest } from "../sagas";

/**
 *  Generates redux api action type according to adress and method
 */
export function apiType(controller: string, method: string, stateDomain: string) {
    let type = `${controller.toUpperCase()}_${method.toUpperCase()}`;
    if (stateDomain && stateDomain.length > 0) {
        type = `${type}_${stateDomain.toUpperCase()}`;
    }

    return type;
}

export function requestLifecycle(controller: string, method: string, stateDomain?: string) {
    const type = apiType(controller, method, stateDomain);
    return requestLifecycleFromType(type);
}

export function requestLifecycleFromPayload({ controller, method }: IRequest) {
    return requestLifecycle(controller, method);
}

function requestLifecycleFromType(type: string): ILyfecycle {
    return {
        PENDING: `${type}_PENDING`,
        RESOLVED: `${type}_RESOLVED`,
        REJECTED: `${type}_REJECTED`,
    };
}

export function apiAction(server: string, controller: string, method: string, payload: Object,
    request_type: string, stateDomain: string, showSpinner = true) {
    const type = apiType(controller, method, stateDomain);
    payload = Object.assign({}, payload, { server, controller, method, request_type });
    const meta = { api: true };
    return { type, meta, payload, showSpinner };
}
