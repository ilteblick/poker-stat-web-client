import { REQUEST_TYPE, REQUEST_METHOD } from "../request/constants";
import { requestLifecycle, apiAction } from "./utils";
import { SERVER_ADDRESSES } from "../../config";

/**
 *
 *
 * @export
 * @class ApiService
 * This class creates standart api actions.
 *
 * Every action will have
 *
 * {
 *
 *      type - CONTROLLER_METHOD,
 *      meta - {api: true} (if we do not have meta? api would not catch action),
 *      payload: {
 *          req_method - http method of api request (in socket _req_method postfix),
 *          params - map of paramaters. In http map keys would be url args. In socket _param_.
 *          query - object which will be translated in query string
 *      }
 *
 * }
 *
 */
export class ApiService {
    /**
     * Creates an instance of ApiService.
     *
     * @param {any} request_type - transport to use
     *
     * @memberOf ApiService
     */
    public server_address: string;
    public request_type: string;

    constructor(server_address: string, request_type: string) {
        this.server_address = server_address;
        this.request_type = request_type;
    }

    /**
     * Creates get api action. Api will return resourse by id
     *
     * @param {any} controller - service name to call. In socket all '/' will be raplced on '_'
     * @param {any} id - id of resource to get (goes to params of action)
     * @param {any} query - extra paramaters. Will go to query
     *
     *
     * @memberOf ApiService
     */
    get(controller: any, id?: any, query?: any, stateDomain?: string) {
        const paramsMap = new Map();
        paramsMap.set("id", id);

        const payload = {
            req_method: REQUEST_METHOD.GET,
            params: paramsMap,
            query,
            // query: {
            //     start: '25',
            //     end: {
            //         endval: '57',
            //         endval1: '69',
            //     }
            // }

        };
        return apiAction(this.server_address, controller, "get", payload, this.request_type, stateDomain);
    }


    getWithOneConstantRouteSegment(controller: any, id: any, segment: any, query: any, stateDomain: string) {
        const paramsMap = new Map();
        paramsMap.set("id", id);
        paramsMap.set(segment, segment);
        const payload = {
            req_method: REQUEST_METHOD.GET,
            params: paramsMap,
            query,
            // query: {
            //     start: '25',
            //     end: {
            //         endval: '57',
            //         endval1: '69',
            //     }
            // }

        };
        return apiAction(this.server_address, controller, `get_id_${segment}`, payload, this.request_type, stateDomain);
    }

    /**
    * Creates get api action. Api will return resourse by slug
    *
    * @param {any} controller - service name to call. In socket all '/' will be raplced on '_'
    * @param {any} slug - slug of resource to get (goes to params of action)
    * @param {any} query - extra paramaters. Will go to query
    *
    *
    * @memberOf ApiService
    */
    getBySlug(controller: any, slug: any, query: any, stateDomain: string) {
        const paramsMap = new Map();
        paramsMap.set("slug", slug);
        const payload = {
            req_method: REQUEST_METHOD.GET,
            params: paramsMap,
            query,
            // query: {
            //     start: '25',
            //     end: {
            //         endval: '57',
            //         endval1: '69',
            //     }
            // }

        };
        return apiAction(this.server_address, controller, "get_slug", payload, this.request_type, stateDomain);
    }

    getBySlugWithOneConstantRouteSegment(controller: any, slug: any, segment: any, query: any, stateDomain: string) {
        const paramsMap = new Map();
        paramsMap.set("slug", slug);
        paramsMap.set(segment, segment);
        const payload = {
            req_method: REQUEST_METHOD.GET,
            params: paramsMap,
            query,
            // query: {
            //     start: '25',
            //     end: {
            //         endval: '57',
            //         endval1: '69',
            //     }
            // }

        };
        return apiAction(this.server_address, controller, `get_slug_${segment}`, payload, this.request_type, stateDomain);
    }

    /**
     * Creates find api action. Api will return list of resourses
     *
     * @param {any} controller - service name to call. In socket all '/' will be rplaced on '_'
     * @param {any} query - will go to query
     * @returns
     *
     * @memberOf ApiService
     */
    find(controller: any, query?: any, showSpinner = true, stateDomain?: string) {
        const payload = {
            req_method: REQUEST_METHOD.GET,
            query,
        };
        return apiAction(this.server_address, controller, "find", payload, this.request_type, stateDomain, showSpinner);
    }

    /**
     * Creates update api action. Api will return updated resourse
     *
     * @param {any} controller - service name to call. In socket all '/' will be raplced on '_'
     * @param {any} id - id of resource to update (goes to params of action)
     * @param {any} body - new data for resource
     * @param {any} query - will go to query
     * @returns
     *
     * @memberOf ApiService
     */
    put(controller: any, id: any, body: any, query: any, stateDomain: string) {
        const paramsMap = new Map();
        paramsMap.set("id", id);
        const payload = {
            req_method: REQUEST_METHOD.PUT,
            params: paramsMap,
            body,
            query,
        };

        return apiAction(this.server_address, controller, "put", payload, this.request_type, stateDomain);
    }

    /**
     * Creates create api action. Api will return created resourse
     *
     * @param {any} controller - service name to call. In socket all '/' will be raplced on '_'
     * @param {any} body - new resourse
     * @param {any} query - will go to query
     * @returns
     *
     * @memberOf ApiService
     */
    post(controller: any, body?: any, query?: any, headers?: any, stateDomain?: string) {
        const payload = {
            req_method: REQUEST_METHOD.POST,
            body,
            query,
            headers,
        };
        return apiAction(this.server_address, controller, "post", payload, this.request_type, stateDomain);
    }

    /**
     *
     *
     * @param {any} controller
     * @param {any} id - id of resource to update (goes to params of action)
     * @param {any} query - will go to query
     * @returns
     *
     * @memberOf ApiService
     */
    delete(controller: any, id: any, query: any, stateDomain: string) {
        const paramsMap = new Map();
        paramsMap.set("id", id);
        const payload = {
            req_method: REQUEST_METHOD.DELETE,
            params: paramsMap,
            query,

        };
        return apiAction(this.server_address, controller, "delete", payload, this.request_type, stateDomain);
    }

    /**
     *
     *
     * @param {any} controller
     * @returns
     *
     * @memberOf ApiService
     */
    getLifecycle(controller: any, stateDomain?: string) {
        return requestLifecycle(controller, "get", stateDomain);
    }

    getWithOneConstantRouteSegmentLifecycle(controller: any, segment: any, stateDomain: string) {
        return requestLifecycle(controller, `get_id_${segment}`, stateDomain);
    }

    /**
    *
    *
    * @param {any} controller
    * @returns
    *
    * @memberOf ApiService
    */
    getBySlugLifecycle(controller: any, stateDomain: string) {
        return requestLifecycle(controller, "get_slug", stateDomain);
    }

    getBySlugWithOneConstantRouteSegmentLifecycle(controller: any, segment: any, stateDomain: string) {
        return requestLifecycle(controller, `get_slug_${segment}`, stateDomain);
    }
    /**
     *
     *
     * @param {any} controller
     * @returns
     *
     * @memberOf ApiService
     */
    findLifecycle(controller: any, stateDomain?: string) {
        return requestLifecycle(controller, "find", stateDomain);
    }

    /**
     *
     *
     * @param {any} controller
     * @returns
     *
     * @memberOf ApiService
     */
    postLifecycle(controller: any, stateDomain?: string) {
        return requestLifecycle(controller, "post", stateDomain);
    }

    /**
     *
     *
     * @param {any} controller
     * @returns
     *
     * @memberOf ApiService
     */
    putLifecycle(controller: any, stateDomain: string) {
        return requestLifecycle(controller, "put", stateDomain);
    }

    /**
     *
     *
     * @param {any} controller
     * @returns
     *
     * @memberOf ApiService
     */
    deleteLifecycle(controller: any, stateDomain: string) {
        return requestLifecycle(controller, "delete", stateDomain);
    }


}

/**
 * Basic service to create call api actions
 *
 */
export const apiService = new ApiService(SERVER_ADDRESSES.DEFAULT, REQUEST_TYPE.HTTP);
