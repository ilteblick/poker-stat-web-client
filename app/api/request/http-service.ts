import "whatwg-fetch";
import * as qs from "qs";

import { REQUEST_METHOD } from "./constants";
import { convertObjectToError } from "./error";

function makePathFromParams(map: Map<any, any>, separator: string) {
    if (!(map instanceof Map)) {
        throw new Error("Invalid parameter object. Must be Map");
    }

    let mapString = "";

    for (const [key, value] of map) {
        let strToAdd = "";

        if (value instanceof Map) {
            if (typeof key === "object") {
                throw new Error(`Invalid parameter key. Must be plain type ${key}`);
            }
            strToAdd += separator + key;
            strToAdd += makePathFromParams(value, separator);
        } else {
            if (typeof value === "object") {
                throw new Error(`Invalid parameter value. Must be plain type ${key}`);
            }
            strToAdd += separator + value;
        }

        mapString += strToAdd;
    }

    return mapString;
}

/**
 * Parses the JSON returned by a network request
 */
function parseJSON(response: any) {
    return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 */
function checkStatus(response: Response): Response | Promise<any> {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    return parseJSON(response).then((errorData: any) => {
        const error = convertObjectToError(errorData);
        throw error;
    });
}

/**
 * Requests a URL, returning a promise
 */
function makeRequest(server: string, url: string, options: RequestInit) {
    options.credentials = "include";
    options.mode = "cors";

    const urlToSend = `${server}/${url}`;

    return fetch(urlToSend, options)
        .then(checkStatus)
        .then(parseJSON);
}

export default function request({ server, controller, req_method, params, query, body, headers }: any): any {
    let urlString = controller;

    if (params !== undefined && params) {
        urlString += makePathFromParams(params, "/");
    }

    if (query !== undefined) {
        const queryStr = qs.stringify(query);
        urlString += `?${queryStr}`;
    }

    // query.lang = lang;
    // query.locale = locale;

    // const queryStr = qs.stringify(query);
    // urlString += `?${queryStr}`;

    const options: RequestInit = {
        method: req_method || REQUEST_METHOD.GET,
    };

    if (headers === undefined) {
        if (body !== undefined && body) {
            options.body = JSON.stringify(body);
        }
        options.headers = {
            Accept: "application/json",
            "content-type": "application/json",
        };
    }

    if (headers === "multipart/form-data") {
        const formData = new FormData();
        Object.keys(body).forEach((key) => {
            if (body[key] instanceof FileList) {
                formData.append(`file:${key}`, body[key][0]);
            } else {
                formData.append(`data:${key}`, body[key]);
            }
        });
        options.body = formData;
    }

    // options.headers = {
    //     "Accept": "*/*",
    //     "Accept-Encoding": "gzip, deflate, br",
    //     "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    //     "Connection": "keep-alive",
    // };

    return makeRequest(server, urlString, options);
}
