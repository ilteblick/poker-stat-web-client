import { REQUEST_TYPE } from "./constants";
import httpService from "./http-service";
import { IRequest } from "../sagas";
// import socketService from './socket-service';

export function chooseRequest({ request_type }: IRequest) {
    switch (request_type) {
        // case REQUEST_TYPE.WEB_SOCKETS:
        //     return socketService;
        case REQUEST_TYPE.HTTP:
            return httpService;
        default:
            return "";
    }
}
