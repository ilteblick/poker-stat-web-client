const HTTP = "HTTP";
const WEB_SOCKETS = "WEB_SOCKETS";
const DEFAULT = WEB_SOCKETS;


export const REQUEST_TYPE = {
    HTTP,
    WEB_SOCKETS,
    DEFAULT,
};

export const REQUEST_METHOD = {
    GET: "GET",
    PUT: "PUT",
    POST: "POST",
    DELETE: "DELETE",
};
