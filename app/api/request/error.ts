function deserializeError(obj: any) {
    if (!isSerializedError(obj)) { return obj; }
    return Object.assign(new Error(), { stack: undefined }, obj);
}

function isSerializedError(obj: any) {
    return obj &&
        typeof obj === "object" &&
        typeof obj.name === "string" &&
        typeof obj.message === "string";
}

export function convertObjectToError(obj: any) {
    if (obj instanceof Error) { return obj; }
    if (obj instanceof Array) { return obj.map(deserializeError); }
    if (typeof obj !== "object") { return obj; }

    let err = deserializeError(obj);
    if (err !== obj) { return err; }
    err = {};
    for (const k in obj) {
        if ({}.hasOwnProperty.call(obj, k)) {
            err[k] = deserializeError(obj[k]);
        }
    }
    return err;
}
