import { IStore } from "../configureStore";
import configureSaga from "./sagas";


export function configureApi<T>(store: IStore<T>) {
    // const saga = configureSaga(api);
    const saga = configureSaga();

    store.runSaga(saga);
}
