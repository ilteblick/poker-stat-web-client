import { call, put, takeEvery } from "redux-saga/effects";

import { requestLifecycleFromPayload } from "./actionService/utils";
import { resolve, reject, pending } from "./actionService/actions";
import { chooseRequest } from "./request";
import { Action } from "../basicClasses";
import { showApiCallSpinner, hideApiCallSpinner } from "../containers/App/actions";

export interface IRequest {
    server: string;
    controller: string;
    req_method: string;
    params: any;
    query: any;
    body: any;
    headers: any;
    lang: string;
    locale: string;
    method: string;
    request_type: string;
}
export default function () {
    function* requestAPI(action: Action) {
        const payload: IRequest = action.payload;

        const lifecycle = requestLifecycleFromPayload(payload);

        yield put(pending(lifecycle));


        if (action.showSpinner) {
            yield put(showApiCallSpinner());
        }
        try {
            const request = chooseRequest(payload);
            const result = yield call(request, payload);
            yield put(resolve(lifecycle, result));
        } catch (error) {
            console.error("Api request failed", error);
            yield put(reject(lifecycle, error));
        }
        if (action.showSpinner) {
            yield put(hideApiCallSpinner());
        }
    }

    function* saga() {
        const isApiCall = ({ meta }: any) => meta && meta.api;
        return yield takeEvery(isApiCall, requestAPI);
    }

    return saga;
}
