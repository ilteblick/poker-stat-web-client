import Redux from "redux";
import { Task, SagaIterator } from "redux-saga";

export interface IStore<T> extends Redux.Store<T> {
    runSaga?: (saga: (...args: any[]) => SagaIterator, ...args: any[]) => Task;
    injectedReducers?: Redux.ReducersMapObject;
    // TODO add real type
    injectedSagas?: any;
    replaceReducer: any;
}
