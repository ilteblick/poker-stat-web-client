import { Dispatch } from "react-redux";

export interface BaseDispatchProps {
    dispatch: Dispatch<any>;
}
