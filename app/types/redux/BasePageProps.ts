import { LocationDescriptorObject } from "history";
// import { ILoadableProps } from "../../components/LoadableComponent/types";
// import { AccessTokenPayload } from "../../auth/types";

export interface BasePageOwnProps {
    location: LocationDescriptorObject;
}

// export interface BasePageStateProps extends ILoadableProps {
// }

export interface BasePageStateProps {
}

// export interface BaseProtectedPageOwnProps extends BasePageOwnProps {
//     currentUser: AccessTokenPayload;
// }
