export interface Action<T extends string> {
    type: T;
}

export interface ActionWithPayload<T extends string, TPayload> extends Action<T> {
    payload: TPayload;
}

export interface ActionWithPayloadAndError<T extends string, TPayload, TError> extends ActionWithPayload<T, TPayload> {
    error: TError;
}

export interface ActionWithPayloadErrorAndMeta<T extends string, TPayload, TError, TMeta>
    extends ActionWithPayloadAndError<T, TPayload, TError> {
    meta: TMeta;
}

export function createAction<T extends string>(type: T): Action<T>;
export function createAction<T extends string, TPayload>(type: T, payload: TPayload): ActionWithPayload<T, TPayload>;
export function createAction<T extends string, TPayload, TError>(type: T, payload: TPayload,
    error: TError): ActionWithPayloadAndError<T, TPayload, TError>;
export function createAction<T extends string, TPayload, TError, TMeta>(type: T, payload: TPayload,
    error: TError, meta: TMeta): ActionWithPayloadErrorAndMeta<T, TPayload, TError, TMeta>;
export function createAction<T extends string, TPayload, TError, TMeta>(type: T, payload?: TPayload,
    error?: TError, meta?: TMeta) {
    if (meta) {
        return { type, payload, error, meta };
    }

    if (error) {
        return { type, payload, error };
    }

    if (payload) {
        return { type, payload };
    }

    return { type };
}


