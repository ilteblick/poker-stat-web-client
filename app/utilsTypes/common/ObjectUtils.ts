export class ObjectUtils {
    /**
        * Merges two objects.
        *
        * @param obj1 First object to be merged
        * @param obj2 Second object to be merged
        * @returns Merged object
        */
    public static merge(obj1: any, obj2: any): any {
        const result: any = {};
        for (let i in obj1) {
            if ((i in obj2) && (typeof obj1[i] === "object") && (i !== null)) {
                result[i] = ObjectUtils.merge(obj1[i], obj2[i]);
            } else {
                result[i] = obj1[i];
            }
        }
        for (let i in obj2) {
            result[i] = obj2[i];
        }
        return result;
    }

    public static isEmpty(obj: any): boolean {
        if (obj) {
            for (const prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    return false;
                }
            }
        }
        return true;
    }
}

