import { Record } from "immutable";

export interface StaticallyTypedRecord<T> {
    new(v?: Partial<T>): this;
    get<K extends keyof T>(key: K): T[K];
    set<K extends keyof T, V extends T[K]>(key: K, value: V): this;
    withMutations(cb: (r: StaticallyTypedRecord<T>) => StaticallyTypedRecord<T>): this;
    setIn<K1 extends keyof T, V extends T[K1]>(keys: [K1], val: V): this;
    setIn<K1 extends keyof T, K2 extends keyof T[K1], V extends T[K1][K2]>(keys: [K1, K2], val: V): this;
    setIn<K1 extends keyof T, K2 extends keyof T[K1], K3 extends keyof T[K1][K2], V extends T[K1][K2][K3]>(
        keys: [K1, K2, K3],
        val: V
    ): this;
    toJS(): this;
}


export const RecordFactory = <T>(seed: T): StaticallyTypedRecord<T> & Readonly<T> => {
    return (Record(seed) as any) as StaticallyTypedRecord<T> & Readonly<T>;
};

// export const RecordFactory = <T>(seed: T): StaticallyTypedRecord<T> => {
//     return (Record(seed) as any) as StaticallyTypedRecord<T>;
// };
