import * as React from "react";
import * as ReactDOM from "react-dom";

import "antd/dist/antd.min.css";

// import "antd/lib/button/style/css";

import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import createHistory from "history/createBrowserHistory";

import AppImport from "./containers/App";
import rootSaga from "./containers/App/sagas";

import configureStore from "./configureStore";
import { configureApi } from "./api";

import "./global-styles";

const MOUNT_NODE = document.getElementById("app");

const history = createHistory();

const initialState = {};
const store = configureStore(initialState, history);

configureApi(store);


store.runSaga(rootSaga);
let App = AppImport;
const render = () => {
    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <App />
            </ConnectedRouter>
        </Provider>,
        MOUNT_NODE
    );
};

if (module.hot) {
    module.hot.accept(["./containers/App"], () => {
        import("./containers/App").then((result) => {
            App = result.default;
            ReactDOM.unmountComponentAtNode(MOUNT_NODE);
            render();
        });

    });
}

render();
