import styled from "styled-components";
import {StyledThemeProps} from "../../utils/styleThemes";

export const StyledContainer = styled.div`
    display:flex;
    flex-direction:column;
    min-width: 500px;
    flex: 1;
`;

export const StyledTitle = styled<StyledThemeProps, "span">("span")`
    color: ${props => props.theme.color};
    font-size:50px;
    font-weight: bold;
    align-self: center;
`;

export const StyledLabel = styled.span`
    color: #acacac;
    font-size:20px;
`;

export const CenteredContainer = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    flex: 1;
`;
