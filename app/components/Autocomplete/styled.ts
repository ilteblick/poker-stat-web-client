import styled from "styled-components";

export const StyledDiv = styled.div`
    position: relative;
    padding: ${props => props.sizeType === "big" ? "0 20px" : "0"};
`;
