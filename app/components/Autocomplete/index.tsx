import * as React from "react";

import Input from "../FilterInput";
import Suggestions from "../Suggestions";

import { StyledDiv } from "./styled";
import { pokerSitePostfix } from "../../containers/StatPage/utils";

const Search = require("../../../static/Search.png");

type SizeType = "big" | "small";

interface AutocompleteProps {
    label?: string;
    value: string;
    onChange: Function;
    suggests: any;
    onSelect: Function;
    onFind: Function;
    pokersite_id: string;
    sizeType?: SizeType;
}

interface AutocompleteState {
    needToShowSuggests: boolean;
    focused: boolean;
}


export default class Autocomplete extends React.PureComponent<AutocompleteProps, AutocompleteState> {

    static defaultProps: Partial<AutocompleteProps> = {
        sizeType: "big"
    }

    constructor(props: AutocompleteProps) {
        super(props);

        this.state = {
            needToShowSuggests: false,
            focused: false
        };
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleOutsideClick);
    }

    componentWillReceiveProps(nextProps: AutocompleteProps) {
        if (nextProps.suggests && nextProps.suggests.length > 0) {
            this.setState({ needToShowSuggests: true });
        }
    }

    onSelectSuggest = (x: any) => {
        const { onSelect } = this.props;
        onSelect(x);
        this.setState({ needToShowSuggests: false });
    }

    handleOutsideClick = (e: MouseEvent) => {
        if (!this.absoluteRef.contains(e.target)) {
            this.setState({ needToShowSuggests: false });
        }
    }

    getAbsoluteRef = (ref: any) => {
        this.absoluteRef = ref;
    }

    render() {
        const { label, value, onChange, suggests, onFind, pokersite_id, sizeType, className } = this.props;
        const { needToShowSuggests } = this.state;

        const postfix = pokersite_id ? ` (${pokerSitePostfix[pokersite_id]})` : "";

        return (
            <StyledDiv ref={this.getAbsoluteRef}>
                <Input
                     className={className}
                    sizeType={sizeType}
                    label={label}
                    value={`${value || ""}${postfix}`}
                    onChange={onChange}
                    postfix={sizeType === "big" ? Search : undefined}
                    suffix={sizeType === "small" ? Search : undefined}
                    onPostClick={onFind}
                    onSuffixClick={onFind}
                />
                {needToShowSuggests ? <Suggestions suggestions={suggests} onClick={this.onSelectSuggest} sizeType={sizeType} /> : null}
            </StyledDiv>
        );
    }
}
