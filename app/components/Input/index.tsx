import * as React from "react";

import { Group, InputComponent, Bar, Label, Highligter, ErrorLabel, ImgDiv } from "./styled";

const isDecimalRgx = /^((\d+\.?\d*)|(\.\d+))$/;
const isIntegerRgx = /^(\d+)$/;

export interface InputProps {
    label: string;
    value: string;
    onChange: Function;
    finalValidate?: Function;
    isPass?: boolean;
    suffix?: any;
    customSuffix?: any;
}

export default class Input extends React.PureComponent<InputProps, {}> {
    constructor(props: InputProps) {
        super(props);

        this.state = {
            finalError: false,
            runtimeError: false,
            typingError: false,
            isBlured: false,
        };
    }

    isDecimal(value, oldValue) {
        if (value === "") {
            return "";
        }

        if (isDecimalRgx.test(value)) {
            // if (value[0] === '.') {
            //     return '0'.concat(value);
            // }
            return value;
        }

        if (value === ".") {
            return ".";
        }

        return oldValue;
    }

    isNumber(value, oldValue) {
        if (value === "") {
            return "";
        }

        if (isIntegerRgx.test(value)) {
            return value;
        }
        return oldValue;
    }

    isDate = (value, oldValue) => {
        const chunked = value.split("/");
        if ((chunked.length <= 3 && (chunked.every((item) => isIntegerRgx.test(item) || item === ""))) || value === "") {
            return value;
        }

        return oldValue;
    }

    componentWillReceiveProps(nextProps) {
        const { finalValidate, runtimeValidate } = this.props;
        const { isBlured } = this.state;

        let runtimeError = false;
        let finalError = false;

        const { value: oldPropsValue } = this.props;
        const { value: newPropsValue } = nextProps;
        const valuesAreEqual = (!oldPropsValue && !newPropsValue) || oldPropsValue === newPropsValue;

        if (runtimeValidate && !valuesAreEqual) {
            runtimeError = runtimeValidate(nextProps.value);
        }

        if (isBlured && finalValidate && !valuesAreEqual) {
            finalError = finalValidate(nextProps.value);
        }

        if (!valuesAreEqual) {
            this.setState({
                runtimeError,
                finalError,
            });
        }
    }

    handleOnBlur = () => {
        const { isBlured } = this.state;
        const { finalValidate, value } = this.props;

        if (!isBlured) {
            if (finalValidate) {
                this.setState({
                    finalError: finalValidate(value),
                    isBlured: true,
                });
            }
        }
    }

    handleOnChange = (event) => {
        const { onChange, type } = this.props;
        event.persist();

        this.setState({ typingError: { ok: true } });

        if (type === "decimal") {
            if (onChange) {
                const newVal = this.isDecimal(event.target.value, this.props.value);
                if (newVal !== this.props.value) {
                    onChange(newVal);
                } else {
                    clearTimeout(this.timerError);
                    this.setState({ typingError: { ok: false, error: "wrong symbol" } });
                    this.timerError = setTimeout(() => this.setState({ typingError: { ok: true } }), 1000);
                }
            }
        }

        if (type === "int") {
            if (onChange) {
                const newVal = this.isNumber(event.target.value, this.props.value);

                if (newVal !== this.props.value) {
                    onChange(newVal);
                } else {
                    clearTimeout(this.timerError);
                    this.setState({ typingError: { ok: false, error: "wrong symbol" } });
                    this.timerError = setTimeout(() => this.setState({ typingError: { ok: true } }), 1000);
                }
            }
        }

        if (type === "date") {
            const newVal = this.isDate(event.target.value, this.props.value);

            if (newVal !== this.props.value) {
                onChange(newVal);
            } else {
                clearTimeout(this.timerError);
                this.setState({ typingError: { ok: false, error: "wrong symbol" } });
                this.timerError = setTimeout(() => this.setState({ typingError: { ok: true } }), 1000);
            }
        }

        if (type === "text") {
            if (onChange) {
                onChange(event.target.value);
            }
        }
    }

    render() {
        const { value, label, getRef, disabled, onClick, outerError, onKeyDown, onKeyUp, size, inputType, isPass, suffix, customSuffix: CustomSuffix } = this.props;
        const { runtimeError, finalError, typingError } = this.state;

        const valueToDisplay = value === undefined ? "" : value;



        let errorMsgToDisplay = null;
        let isError = false;

        if (typingError && !typingError.ok) {
            isError = true;
            errorMsgToDisplay = typingError.error;
        }

        if (runtimeError && !runtimeError.ok) {
            isError = true;
            errorMsgToDisplay = runtimeError.error;
        }

        if (finalError && !finalError.ok) {
            isError = true;
            errorMsgToDisplay = finalError.error;
        }

        if (outerError && !outerError.ok) {
            isError = true;
            errorMsgToDisplay = outerError.error;
        }

        const pre = suffix
            ? <ImgDiv><img src={suffix} /></ImgDiv>
            : CustomSuffix
                ? <ImgDiv><CustomSuffix /></ImgDiv>
                : null;

        return (
            <Group size={size}>
                {pre}
                <InputComponent
                    value={valueToDisplay}
                    onChange={this.handleOnChange}
                    onClick={onClick}
                    required
                    ref={getRef}
                    blocked={disabled}
                    onBlur={this.handleOnBlur}
                    error={isError}
                    onKeyDown={onKeyDown}
                    onKeyUp={onKeyUp}
                    size={size}
                    type={isPass ? "password" : inputType} />
                <Bar error={isError} />
                <Highligter />
                <Label error={isError} size={size}>{label}</Label>
                {isError ? <ErrorLabel>{errorMsgToDisplay}</ErrorLabel> : null}
            </Group>
        );
    }
}

Input.defaultProps = {
    type: "text",
    size: "default",
};
