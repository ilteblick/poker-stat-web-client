import styled, { keyframes, css } from "styled-components";

const inputHighlighter = keyframes`
    from { background:#5264AE; }
    to { width:0; background:transparent; }
`;

export const Highligter = styled.span`
    position:absolute;
    height:60%;
    width:100px;
    top:25%;
    left:0;
    pointer-events:none;
    opacity:0.5;
`;

export const Label = styled.label`
    color:${(props) => props.error ? props.theme.inputErrorColor : props.theme.InputPlaceholderColor};
    font-size: ${(props) => props.size === "small" ? "14px" : "18px"};
    font-weight:normal;
    position:absolute;
    pointer-events:none;
    left:50px;
    top:50%;
    line-height: 18px;
    transition:0.2s ease all;
    -moz-transition:0.2s ease all;
    -webkit-transition:0.2s ease all;
`;

export const Bar = styled.span`
    position:relative;
    display:block;
    width:100%;

    &:after{
        content:'';
        height:2px;
        width:0;
        bottom:1px;
        position:absolute;
        background:${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputSuccessColor};
        transition:0.2s ease all;
        -moz-transition:0.2s ease all;
        -webkit-transition:0.2s ease all;
        right:50%;
    };

    &:before{
        content:'';
        height:2px;
        width:0;
        bottom:1px;
        position:absolute;
        background:${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputSuccessColor};
        transition:0.2s ease all;
        -moz-transition:0.2s ease all;
        -webkit-transition:0.2s ease all;
        left:50%;
    };
`;

export const InputComponent = styled.input`
    font-size:${(props) => props.size === "small" ? "14px" : "18px"};
    color: ${props => props.theme.inputColor};
    background: transparent;
    padding:5px;
    display:block;
    width:100%;
    border:none;
    border-bottom:1px solid ${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputBorderColor};
    box-sizing: border-box;
    ${(props) => props.blocked ? "pointer-events:none;" : ""};
    padding-left: 50px;

    &:focus{
      outline:none;
    }

`;


const activeBar = css`
    ${InputComponent}{
        &:focus ~ ${Bar}{
            &:before{
                width:50%;
            }
            &:after{
                width:50%;
            }
        };

    }

    ${InputComponent}{
        &:focus ~ ${Highligter}{
            animation: ${inputHighlighter} 0.3s ease;
        }
    }

    ${InputComponent}{
        &:focus ~ ${Label}, &:valid ~ ${Label}{
            top:10px;
            left: 5px;
            font-size:12px;
            color:${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputSuccessColor};
        }
    }

`;
export const Group = styled.div`
    padding-bottom: 10px;
    position:relative;
    padding-top:${(props) => props.size === "small" ? "18px" : "24px"};
    width:100%;

    ${activeBar};
`;

export const ErrorLabel = styled.div`
    color: ${props => props.theme.inputErrorColor};
    font-size: 12px;
    line-height: 14px;
    padding-top: 5px;
    padding-left: 5px;
    position:absolute;
`;

export const ImgDiv = styled.div`
    position: absolute;
    left: 10px;
    height: 38px;
    display: flex;
    align-items: center;
`;

