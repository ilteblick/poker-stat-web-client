import styled from "styled-components";
import { StyledThemeProps } from "../../../utils/styleThemes";

export const StyledIcon = styled<StyledThemeProps, "svg">("svg")`
    fill-rule: evenodd;
    fill: ${props => props.theme.colorMain};
`;
