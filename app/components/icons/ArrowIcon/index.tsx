import * as React from "react";
import { IconProps } from "./types";

import { StyledIcon } from "./styled";

export default ({className}: IconProps) => (
    <StyledIcon width="10" height="5" viewBox="0 0 10 5" className={className}>
        <path
            d="M1044.81,174.165a0.633,0.633,0,0,0-.44-0.165h-8.74a0.633,0.633,0,0,0-.44.165,0.5,0.5,0,0,0,0,.781l4.37,3.888a0.669,0.669,0,0,0,.88,0l4.37-3.888A0.5,0.5,0,0,0,1044.81,174.165Z"
            transform="translate(-1035 -174)"
        />
    </StyledIcon>
);
