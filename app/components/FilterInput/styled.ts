import styled, {keyframes, css} from "styled-components";

const inputHighlighter = keyframes`
    from { background:#5264AE; }
    to { width:0; background:transparent; }
`;

export const Highligter = styled.span`
    position:absolute;
    height:60%;
    width:100px;
    top:25%;
    left:0;
    pointer-events:none;
    opacity:0.5;
`;

export const Label = styled.label`
    color:${(props) => props.error ? props.theme.inputErrorColor : props.theme.InputPlaceholderColor};
    font-size:${(props) => props.sizeType === "big" ? props.size === "small" ? "14px" : "18px" : "12px"};
    font-weight:normal;
    position:absolute;
    pointer-events:none;
    left:${(props) => props.left};
    top:50%;
    line-height: 1em;
    transition:0.2s ease all;
    -moz-transition:0.2s ease all;
    -webkit-transition:0.2s ease all;
`;

export const Bar = styled.span`
    position:relative;
    display:block;
    width:100%;

    &:after{
        content:'';
        height:2px;
        width:0;
        bottom:1px;
        position:absolute;
        background:${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputSuccessColor};
        transition:0.2s ease all;
        -moz-transition:0.2s ease all;
        -webkit-transition:0.2s ease all;
        right:50%;
    };

    &:before{
        content:'';
        height:2px;
        width:0;
        bottom:1px;
        position:absolute;
        background:${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputSuccessColor};
        transition:0.2s ease all;
        -moz-transition:0.2s ease all;
        -webkit-transition:0.2s ease all;
        left:50%;
    };
`;

export const InputComponent = styled.input`
    ${props => props.sizeType === "big" ? `height: auto;` : `height: 24px;`}
    font-size:${(props) => props.sizeType === "big" ? props.size === "small" ? "14px" : "18px" : "12px"};
    color: ${props => props.theme.inputColor};
    text-overflow: ellipsis;
    background: transparent;
    ${props => props.sizeType === "big" ? `padding:5px;` : `padding: 0 0 2px 0;`}
    display:block;
    width:100%;
    border:none;
    border-bottom:1px solid ${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputBorderColor};
    box-sizing: border-box;
    ${(props) => props.blocked ? "pointer-events:none;" : ""};
    padding-left:${(props) => props.left};
    padding-right:${(props) => props.right};

    &:focus{
      outline:none;
    }

`;


const activeBar = css`
    ${InputComponent}{
        &:focus ~ ${Bar}{
            &:before{
                width:50%;
            }
            &:after{
                width:50%;
            }
        };

    }

    ${InputComponent}{
        &:focus ~ ${Highligter}{
            animation: ${inputHighlighter} 0.3s ease;
        }
    }

    ${InputComponent}{
        &:focus ~ ${Label}, &:valid ~ ${Label}{
            top:10px;
            left: 5px;
            ${props => props.sizeType === "big" ? `font-size: 12px;` : `font-size: 10px;`}
            color:${(props) => props.error ? props.theme.inputErrorColor : props.theme.inputSuccessColor};
        }
    }

`;
export const Group = styled.div`
    position:relative;
    width:100%;
    ${(props) => props.sizeType === "big"
        ? props.size === "small"
            ? "padding-top: 18px; padding-bottom: 10px;"
            : "padding-top: 24px; padding-bottom: 10px;"
        : "padding-top: 0; padding-bottom: 0;"}
    ${activeBar};
`;

export const ErrorLabel = styled.div`
    color: ${props => props.theme.inputErrorColor};
    font-size: 12px;
    line-height: 14px;
    padding-top: 5px;
    padding-left: 5px;
    position:absolute;
`;

export const ImgDiv = styled.div`
    position: absolute;
    ${props => props.sizeType === "big" ? `height: 38px;` : `height: 24px;`}
    display: flex;
    align-items: center;
    > img {
        ${props => props.sizeType === "big" ? `max-height: 16px;` : `max-height: 12px;`}
    }
`;

export const ImgDivPost = styled.div`
    position: absolute;
    ${props => props.sizeType === "big" ? `height: 38px;` : `height: 24px;`}
    display: flex;
    align-items: center;
    right: 8px;
    cursor: pointer;
    > img {
        ${props => props.sizeType === "big" ? `max-height: 16px;` : `max-height: 12px;`}
    }
`;

