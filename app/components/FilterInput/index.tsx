import * as React from "react";

import { Group, InputComponent, Bar, Label, Highligter, ErrorLabel, ImgDiv, ImgDivPost } from "./styled";

const isDecimalRgx = /^((\d+\.?\d*)|(\.\d+))$/;
const isIntegerRgx = /^(\d+)$/;

type SizeType = "big" | "small";

export interface InputProps {
    label: string;
    value: string;
    onChange: Function;
    finalValidate?: Function;
    isPass?: boolean;
    suffix?: any;
    customSuffix?: any;
    postfix?: any;
    customPostfix?: any;
    getRef?: Function;
    disabled?: boolean;
    onPostClick?: Function;
    onSuffixClick?: Function;
    sizeType?: SizeType;
}

export default class FilterInput extends React.PureComponent<InputProps, {}> {
    static defaultProps: Partial<InputProps> = {
        type: "text",
        size: "default",
        sizeType: "big",
    }

    constructor(props: InputProps) {
        super(props);

        this.state = {
            finalError: false,
            runtimeError: false,
            typingError: false,
            isBlured: false,
        };
    }

    isDecimal(value, oldValue) {
        if (value === "") {
            return "";
        }

        if (isDecimalRgx.test(value)) {
            // if (value[0] === '.') {
            //     return '0'.concat(value);
            // }
            return value;
        }

        if (value === ".") {
            return ".";
        }

        return oldValue;
    }

    isNumber(value, oldValue) {
        if (value === "") {
            return "";
        }

        if (isIntegerRgx.test(value)) {
            return value;
        }
        return oldValue;
    }

    isDate = (value, oldValue) => {
        const chunked = value.split("/");
        if ((chunked.length <= 3 && (chunked.every((item) => isIntegerRgx.test(item) || item === ""))) || value === "") {
            return value;
        }

        return oldValue;
    }

    componentWillReceiveProps(nextProps) {
        const { finalValidate, runtimeValidate } = this.props;
        const { isBlured } = this.state;

        let runtimeError = false;
        let finalError = false;

        const { value: oldPropsValue } = this.props;
        const { value: newPropsValue } = nextProps;
        const valuesAreEqual = (!oldPropsValue && !newPropsValue) || oldPropsValue === newPropsValue;

        if (runtimeValidate && !valuesAreEqual) {
            runtimeError = runtimeValidate(nextProps.value);
        }

        if (isBlured && finalValidate && !valuesAreEqual) {
            finalError = finalValidate(nextProps.value);
        }

        if (!valuesAreEqual) {
            this.setState({
                runtimeError,
                finalError,
            });
        }
    }

    handleOnBlur = () => {
        const { isBlured } = this.state;
        const { finalValidate, value } = this.props;

        if (!isBlured) {
            if (finalValidate) {
                this.setState({
                    finalError: finalValidate(value),
                    isBlured: true,
                });
            }
        }
    }

    handleOnChange = (event) => {
        const { onChange, type } = this.props;
        event.persist();

        this.setState({ typingError: { ok: true } });

        if (type === "decimal") {
            if (onChange) {
                const newVal = this.isDecimal(event.target.value, this.props.value);
                if (newVal !== this.props.value) {
                    onChange(newVal);
                } else {
                    clearTimeout(this.timerError);
                    this.setState({ typingError: { ok: false, error: "wrong symbol" } });
                    this.timerError = setTimeout(() => this.setState({ typingError: { ok: true } }), 1000);
                }
            }
        }

        if (type === "int") {
            if (onChange) {
                const newVal = this.isNumber(event.target.value, this.props.value);

                if (newVal !== this.props.value) {
                    onChange(newVal);
                } else {
                    clearTimeout(this.timerError);
                    this.setState({ typingError: { ok: false, error: "wrong symbol" } });
                    this.timerError = setTimeout(() => this.setState({ typingError: { ok: true } }), 1000);
                }
            }
        }

        if (type === "date") {
            const newVal = this.isDate(event.target.value, this.props.value);

            if (newVal !== this.props.value) {
                onChange(newVal);
            } else {
                clearTimeout(this.timerError);
                this.setState({ typingError: { ok: false, error: "wrong symbol" } });
                this.timerError = setTimeout(() => this.setState({ typingError: { ok: true } }), 1000);
            }
        }

        if (type === "text") {
            if (onChange) {
                onChange(event.target.value);
            }
        }
    }

    render() {
        const {
            value,
            label,
            getRef,
            disabled,
            onClick,
            outerError,
            onKeyDown,
            onKeyUp,
            size,
            inputType,
            isPass,
            suffix,
            postfix,
            onPostClick,
            onSuffixClick,
            sizeType,
            customSuffix,
            customPostfix,
            className
        } = this.props;
        const { runtimeError, finalError, typingError } = this.state;

        const valueToDisplay = value === undefined ? "" : value;


        let errorMsgToDisplay = null;
        let isError = false;

        if (typingError && !typingError.ok) {
            isError = true;
            errorMsgToDisplay = typingError.error;
        }

        if (runtimeError && !runtimeError.ok) {
            isError = true;
            errorMsgToDisplay = runtimeError.error;
        }

        if (finalError && !finalError.ok) {
            isError = true;
            errorMsgToDisplay = finalError.error;
        }

        if (outerError && !outerError.ok) {
            isError = true;
            errorMsgToDisplay = outerError.error;
        }

        const pre = suffix
            ? <ImgDiv onClick={onSuffixClick} sizeType={sizeType}><img src={suffix}/></ImgDiv>
            : customSuffix
                ? <ImgDiv onClick={onSuffixClick} sizeType={sizeType}>{customSuffix}</ImgDiv>
                : null;
        const post = postfix
            ? <ImgDivPost onClick={onPostClick} sizeType={sizeType}><img src={postfix}/></ImgDivPost>
            : customPostfix
                ? <ImgDivPost onClick={onPostClick} sizeType={sizeType}>{customPostfix}</ImgDivPost>
                : null;

        return (
            <Group size={size} sizeType={sizeType}>
                {pre}
                {post}
                <InputComponent
                    className={className}
                    value={valueToDisplay}
                    onChange={this.handleOnChange}
                    onClick={onClick}
                    required
                    ref={getRef}
                    blocked={disabled}
                    onBlur={this.handleOnBlur}
                    error={isError}
                    onKeyDown={onKeyDown}
                    onKeyUp={onKeyUp}
                    size={size}
                    type={isPass ? "password" : inputType}
                    left={pre ? "25px" : "5px"}
                    right={post ? "20px" : "0px"}
                    sizeType={sizeType}
                />
                <Bar error={isError}/>
                <Highligter/>
                <Label left={pre ? "25px" : "5px"} error={isError} size={size} sizeType={sizeType}>{label}</Label>
                {isError ? <ErrorLabel>{errorMsgToDisplay}</ErrorLabel> : null}
            </Group>
        );
    }
}
