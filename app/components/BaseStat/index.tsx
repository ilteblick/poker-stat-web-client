import * as React from "react";

interface BaseStatProps { label: any; value: any; }

export default class BaseStat extends React.PureComponent<BaseStatProps, {}> {
    render() {
        const { label, value } = this.props;
        return (
            <div>{label}:{value}</div>
        );
    }
}
