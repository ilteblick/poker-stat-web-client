import styled from "styled-components";

export const StyledDiv = styled.div`
    position: absolute;
    top: ${props => props.sizeType === "big" ? "68px" : "24px"};
    ${props => props.sizeType === "big" ? "width: 100%;" : "min-width: 200px;"}
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 64px;
    z-index: 100;
    max-height: ${props => props.sizeType === "big" ? "200px" : "130px"};
    overflow: auto;
`;

export const SuggestContainer = styled.div`
    padding: ${props => props.sizeType === "big" ? "10px 10px 10px 15px" : "2px 5px 2px 10px"};
    background: ${props => props.theme.suggestionsBg};
    :nth-child(2n) {
        background: ${props => props.theme.suggestionsBg_even};
    }
    color: ${props => props.theme.suggestionsColor};
    span {
        color: ${props => props.theme.suggestionsColor};
    }
    ${(props) => props.disabled ? "" : `cursor: pointer; &:hover{
        background: ${props.theme.suggestionsBg_hover};

        span {
            color: ${props.theme.suggestionsColor_hover}!important;
        }
    }`}
`;

export const Label = styled.span`
    font-size: ${props => props.sizeType === "big" ? "18px" : "12px"};
    line-height: 1em;
    color: black;
`;
