import * as React from "react";

import { StyledDiv, Label, SuggestContainer } from "./styled";

type SizeType = "big" | "small";

interface SuggestionsProps {
    suggestions: Array<any>;
    onClick: Function;
    sizeType?: SizeType;
}

export default class Suggestions extends React.Component<SuggestionsProps, {}> {
    static defaultProps: Partial<SuggestionsProps> = {
        sizeType: "big",
    }
    shouldComponentUpdate(nextProps: SuggestionsProps) {
        return this.props.suggestions !== nextProps.suggestions;
    }

    render() {
        const { suggestions, onClick, sizeType } = this.props;
        return (
            <StyledDiv sizeType={sizeType}>
                {suggestions.map((x: any, index: number) => (
                    <SuggestContainer key={index} onClick={() => onClick(x)} disabled={x.disabled} sizeType={sizeType}>
                        <Label sizeType={sizeType}>{x.label} {x.postfix}</Label>
                    </SuggestContainer>
                ))}
            </StyledDiv>
        );
    }
}
