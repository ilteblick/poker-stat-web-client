import {StyledThemeProps} from "../../utils/styleThemes";

export type Type = "portable" | "full";

export interface ZoomAppProps {
    type?: Type;
}

export interface ZoomAppState {
    value: number;
}

export interface StyledContainerProps extends StyledThemeProps {

}
