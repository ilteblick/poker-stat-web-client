import * as React from "react";
import { ZoomAppProps, ZoomAppState } from "./types";
import PlusIcon from "./components/PlusIcon";
import MinusIcon from "./components/MinusIcon";

import { StyledContainer, StyledValue, StyledIcon, StyledPanel } from "./styled";

class ZoomApp extends React.PureComponent<ZoomAppProps, {}> {

    static defaultProps: Partial<ZoomAppProps> = {
        type: "full",
    }

    state: ZoomAppState = {
        value: 100,
    };

    constructor(props: ZoomAppProps) {
        super(props);
        this.state = {
            value: props.type === "full" ? 100 : 125,
        };
    }

    componentDidMount() {
        this.setValue();
    }

    setValue = (value?: number) => {
        const prevValue: number = +localStorage.getItem(this.props.type);
        if (!prevValue || value !== prevValue) {
            value = value || prevValue || this.state.value;
            localStorage.setItem(this.props.type, (value).toString());
            this.setState(() => ({value}));
            document.body.style.zoom = `${value}%`;
        }
    }

    onPlusCLick = () => {
        this.setValue(this.state.value + 25);
    }

    onMinusCLick = () => {
        this.setValue(this.state.value <= 50 ? 25 : this.state.value - 25);
    }

    render() {
        const {
            state: {
                value,
            },
            props: {},
        }: this = this;

        return (
            <StyledContainer>
                <StyledValue>
                    {value}%
                </StyledValue>
                <StyledPanel>
                    <StyledIcon onClick={this.onPlusCLick}><PlusIcon/></StyledIcon>
                    <StyledIcon onClick={this.onMinusCLick}><MinusIcon/></StyledIcon>
                </StyledPanel>
            </StyledContainer>
        );
    }
}

export default ZoomApp;
