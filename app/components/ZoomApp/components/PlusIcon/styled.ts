import styled from "styled-components";

export const StyledIcon = styled.svg`
    fill-rule: evenodd;
    fill: ${props => props.theme.color};
`;
