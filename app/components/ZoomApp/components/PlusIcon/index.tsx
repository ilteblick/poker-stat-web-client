import * as React from "react";
import {Props} from "./types";

import {StyledIcon} from "./styled";

export default ({className}: Props) => (
    <StyledIcon width="8" height="8" viewBox="0 0 8 8" className={className}>
        <path
            d="M85,8H83l0,2.93L80,11v2h3v3h2V13h3V11H85V8Z"
            transform="translate(-80 -8)"
        />
    </StyledIcon>
);
