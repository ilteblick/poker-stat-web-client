import * as React from "react";
import {Props} from "./types";

import {StyledIcon} from "./styled";

export default ({className}: Props) => (
    <StyledIcon width="7.969" height="2" viewBox="0 0 7.969 2" className={className}>
        <path
            d="M87.984,24H80.015v2h7.969V24Z"
            transform="translate(-80 -24)"
        />
    </StyledIcon>
);
