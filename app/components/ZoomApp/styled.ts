import styled  from "styled-components";
import {StyledContainerProps} from "./types";

export const StyledContainer = styled.div`
    display: flex;
    align-items: center;
    font-weight: bold;
    color: ${(props: StyledContainerProps) => props.theme.color};
`;

export const StyledPanel = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

export const StyledValue = styled.span`
    margin-right: 4px;
`;

export const StyledIcon = styled.div`
    cursor: pointer;
    user-select: none;
    height: 12px;
    width: 16px;
    display: flex;
    align-items: center;
    line-height: 0;
    justify-content: center;
`;
