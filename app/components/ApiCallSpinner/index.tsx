import * as React from "react";
import { Spin, Modal } from "antd";


interface ApiCallSpinnerState {
    needToShowSpinner: boolean;
}

export default class ApiCallSpinner extends React.PureComponent<{}, ApiCallSpinnerState> {
    constructor(props: Object) {
        super(props);

        this.state = {
            needToShowSpinner: false,
        };
    }

    componentWillMount() {
        setTimeout(() => this.setState({ needToShowSpinner: true }), 300);
    }

    render() {
        return this.state.needToShowSpinner ? <Modal
            title="Loading data..."
            visible={true}
            footer={[]}>
            <div style={{ textAlign: "center" }}><Spin /></div>
        </Modal> : null;
    }
}
