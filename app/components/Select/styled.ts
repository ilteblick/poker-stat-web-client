import styled from "styled-components";
import ArrowIcon from "../icons/ArrowIcon";
import {StyledArrowProps} from "./types";
import { ComponentType } from "react";

export const StyledDiv = styled.div`
    position: relative;
    padding: 0 20px;
`;

export const StyledArrowIcon = styled<Partial<StyledArrowProps>>(ArrowIcon as ComponentType)`
    transform: rotate(${props => props.top ? "180deg" : "0"});
`;
