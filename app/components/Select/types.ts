import {StyledThemeProps} from "../../utils/styleThemes";

export interface StyledArrowProps extends StyledThemeProps {
    top?: boolean;
}
