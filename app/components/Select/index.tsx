import * as React from "react";

import Input from "../FilterInput";
import Suggestions from "../Suggestions";

import { StyledDiv, StyledArrowIcon } from "./styled";

const Down = require("../../../static/down.png");
const Up = require("../../../static/up.png");

interface SelectProps {
    label?: string;
    onChange: Function;
    options: any;
    defaultValue: Object;
}

interface SelectState {
    needToShowSuggests: boolean;
    focused: boolean;
    value: string;
}

export default class Select extends React.PureComponent<SelectProps, SelectState> {
    constructor(props: SelectProps) {
        super(props);

        this.state = {
            needToShowSuggests: false,
            focused: false,
            value: props.defaultValue ? props.options.find((x: any) => x === props.defaultValue).label : "",
        };
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleOutsideClick);
    }

    onSelectSuggest = (x: any) => {
        const { onChange } = this.props;
        if (x.disabled) {
            return;
        }
        this.setState({ value: x.label });
        onChange(x);
        this.setState({ needToShowSuggests: false });
    }

    handleOutsideClick = (e: MouseEvent) => {
        if (!this.absoluteRef.contains(e.target)) {
            this.setState({ needToShowSuggests: false });
        }

    }

    onClick = () => {
        const { needToShowSuggests } = this.state;

        if (needToShowSuggests) {
            this.inputRef.blur();
            this.setState({ needToShowSuggests: false });
        } else {
            this.inputRef.blur();
            this.setState({ needToShowSuggests: true });
        }
    }

    absoluteRef: HTMLDivElement;
    getAbsoluteRef = (ref: HTMLDivElement) => {
        this.absoluteRef = ref;
    }

    inputRef: HTMLInputElement;
    getInputRef = (ref: HTMLInputElement) => {
        this.inputRef = ref;
    }

    render() {
        const { label, onChange, options, value } = this.props;
        const { needToShowSuggests } = this.state;

        const postfix = needToShowSuggests ? <StyledArrowIcon top /> : <StyledArrowIcon />

        return (
            <StyledDiv ref={this.getAbsoluteRef}>
                <div onClick={this.onClick}>
                    <Input
                        label={label}
                        value={value}
                        onChange={onChange}
                        customPostfix={postfix}
                        getRef={this.getInputRef}
                        disabled
                    />
                </div>
                {needToShowSuggests ? <Suggestions suggestions={options} onClick={this.onSelectSuggest} /> : null}
            </StyledDiv>
        );
    }
}
