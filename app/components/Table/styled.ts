import styled from "styled-components";
import { StyledTdProps } from "./types";
import { StyledThemeProps } from "../../utils/styleThemes";
import { StyledContentProps, StyledRateProps } from "./types";

export const StyledContent = styled<StyledContentProps, "div">("div")`
    display: flex;
    padding: 0 24px 24px;
    border-radius: 4px;
    align-items: center;
    box-shadow: 0 0 64px rgba(0,0,0,0.1);
    flex-direction: ${props => props.flex || "row"};
    background: ${props => props.theme.contentBg};
    color: ${props => props.theme.color};
`;

export const StyledTable = styled.table`
    width: 100%;
`;

export const StyledThead = styled<StyledThemeProps, "thead">("thead")`
    background: ${props => props.theme.tableTheadBg};
    color: ${props => props.theme.tableTheadColor};
`;

export const StyledTheadTr = styled<StyledThemeProps, "tr">("tr")`
    background: ${props => props.theme.tableTheadBg};
    color: ${props => props.theme.tableTheadColor};
`;


export const StyledTh = styled.th`
    font-size: 14px;
    color: white;
    text-align: center;
    padding: 6px;
`;

export const StyledTr = styled<StyledThemeProps, "tr">("tr")`
    color: ${props => props.theme.color};
`;

export const StyledTd = styled.td`
    font-size: ${(props: StyledTdProps) => props.postflop ? "12px" : "14px"};
    font-weight: ${(props: StyledTdProps) => props.postflop ? "bold" : "normal"};
    text-align: center;
    padding: 8px;
`;

export const StyledTbody = styled<StyledThemeProps, "tbody">("tbody")`
    > ${StyledTr} {
        &:nth-child(2n) {
            background: ${props => props.theme.tableTrBg_even};
        }
        &:nth-child(2n+1) {
            background: ${props => props.theme.tableTrBg_odd};
        }
    }
`;

export const StyledRate = styled<StyledRateProps, "span">("span")`
    color: ${props => props.positive ? props.theme.positiveColor : props.theme.negativeColor};
`;

export const StyledA = styled<StyledThemeProps, "a">("a")`
    color: ${props => props.theme.color};
    cursor: pointer;
    &:hover{
        color: ${props => props.theme.hrefColor_hover};
    }
`;
