import styled from "styled-components";
import { ComponentType } from "react";
import ArrowIcon from "../../icons/ArrowIcon";
import { StyledThemeProps } from "../../../utils/styleThemes";
import {StyledAroowProps} from "./types";
import { StyledTh } from "../styled";

export const Th = styled(StyledTh)`
    cursor: pointer;
    position: relative;
`;

export const StyledTr = styled<StyledThemeProps, "tr">("tr")`
`;

export const StyledArrow = styled<Partial<StyledAroowProps>>(ArrowIcon as ComponentType)`
    fill: ${props => props.type === "negative"
        ? props.theme.negativeColor
        : props.type === "positive"
            ? props.theme.positiveColor
            : props.type === "inactive"
                ? props.theme.inactiveColor
                : props.theme.color};
    transform: rotate(${props => props.top ? "180deg" : "0"});
    margin-left: 5px;
`;
