import { StyledThemeProps } from "../../../utils/styleThemes";

type ArrowType = "positive" | "negative" | "inactive";

export interface StyledAroowProps extends StyledThemeProps {
    type?: ArrowType;
    top?: boolean;
}
