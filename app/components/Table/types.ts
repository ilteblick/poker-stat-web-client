import { StyledThemeProps } from "../../utils/styleThemes";

export interface StyledTdProps {
    postflop?: boolean;
}

export interface StyledContentProps extends StyledThemeProps {
    flex?: string;
}

export interface StyledRateProps extends StyledThemeProps {
    positive?: boolean;
}
