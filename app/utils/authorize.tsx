import * as React from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import { createStructuredSelector } from "reselect";
import hoistNonReactStatics = require("hoist-non-react-statics");
import { selectUser } from "../containers/App/selectors";
import { IUser } from "../containers/App/interfaces";
import { showPage } from "../containers/App/actions";

/* eslint-disable no-undef */

/**
 * Dynamically injects a saga, passes component's props as saga arguments
 *
 * @param {string} key A key of the saga
 * @param {function} saga A root saga that will be injected
 * @param {string} [mode] By default (constants.RESTART_ON_REMOUNT) the saga will be started on component mount and
 * cancelled with `task.cancel()` on component un-mount for improved performance. Another two options:
 *   - constants.DAEMON—starts the saga on component mount and never cancels it or starts again,
 *   - constants.ONCE_TILL_UNMOUNT—behaves like 'RESTART_ON_REMOUNT' but never runs it again.
 * @param {array} [args] Arguments passed to the saga once called
 * By default your saga will receive
 *   - component props
 *   - action
 * If defined, the saga will receive those args instead of the component props
 *
 */

interface WrappedProps {
    user: IUser;
    redirect: Function;
    onShowPage: Function;
}

export default ({ pageKey }: { pageKey: string }) => (WrappedComponent: any) => {
    function wrapComponent() {
        class UserAuthWrapperComponent extends React.Component<WrappedProps, {}> {
            static WrappedComponent = WrappedComponent;

            static displayName = `withAuth(${(WrappedComponent.displayName || WrappedComponent.name || "Component")})`;

            componentWillMount() {
                if (!this.props.user) {
                    this.props.redirect("/");
                } else {
                    if (pageKey) {
                        this.props.onShowPage(pageKey);
                    }
                }
            }

            componentWillReceiveProps(nextProps: WrappedProps) {
                if (!nextProps.user) {
                    nextProps.redirect("/");
                } else {
                    if (pageKey) {
                        nextProps.onShowPage(pageKey);
                    }
                }
            }

            render() {
                return <WrappedComponent {...this.props} />;
            }


        }

        const mapStateToProps = createStructuredSelector({
            user: selectUser(),
        });
        function mapDispatchToProps(dispatch: Function) {
            return {
                redirect: (url: string) => dispatch(push(url)),
                onShowPage: (key: string) => dispatch(showPage(key)),
                dispatch,
            };
        }

        return connect<{}, {}, WrappedProps>(mapStateToProps, mapDispatchToProps)(UserAuthWrapperComponent);
    }


    return hoistNonReactStatics(wrapComponent(), WrappedComponent);
};
