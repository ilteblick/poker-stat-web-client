import * as React from "react";
import * as PropTypes from "prop-types";

import hoistNonReactStatics = require("hoist-non-react-statics");

import getInjectors from "./reducerInjectors";

/* eslint-disable no-undef */

/**
 * Dynamically injects a reducer
 *
 * @param {string} key A key of the reducer
 * @param {function} reducer A reducer that will be injected
 *
 */
export default ({ key, reducer }: { key: string, reducer: any }) => (WrappedComponent: any) => {
    class ReducerInjector extends React.Component {
        static WrappedComponent = WrappedComponent;
        static contextTypes = {
            store: PropTypes.object.isRequired,
        };
        static displayName = `withReducer(${(WrappedComponent.displayName || WrappedComponent.name || "Component")})`;

        injectors = getInjectors(this.context.store);

        componentWillMount() {
            const { injectReducer } = this.injectors;

            injectReducer(key, reducer);
        }



        render() {
            return <WrappedComponent {...this.props} />;
        }
    }

    return hoistNonReactStatics(ReducerInjector, WrappedComponent);
};
