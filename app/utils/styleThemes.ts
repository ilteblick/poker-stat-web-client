const LogoLight = require("../../static/Logo.png");
const LogoDark = require("../../static/logo_portable_dark.png");
const appBgLight = require("../../static/bg.png");
const appBgDark = require("../../static/bg_dark.jpg");
const portableBgDark = require("../../static/background_portable_statistic.png");
const adLight = require("../../static/H4H_Portable_available.jpg");
const adDark = require("../../static/ad_dark.jpg");

interface ThemeImagesProps {
    logo: string;
    appBg: string;
    portableBg: string;
    ad: string;
}

export type ThemeType = "light" | "dark";

export interface ThemeProps {
    type: string;
    images: ThemeImagesProps;
    color: string;
    colorMain: string;
    colorError: string;
    bg: string;
    contentBg: string;
    contentBgColor: string;
    footerColor: string;
    menuBg: string;
    menuColor: string;
    menuSeparatorBg: string;
    menuColor_hover: string;
    menuColor_active: string;
    menuColor_drop: string;
    inputColor: string;
    InputPlaceholderColor: string;
    inputBorderColor: string;
    inputIconColor: string;
    inputSuccessColor: string;
    inputErrorColor: string;
    suggestionsBg: string;
    suggestionsBg_even: string;
    suggestionsBg_hover: string;
    suggestionsColor: string;
    suggestionsColor_hover: string;
    headerIconColor: string;
    headerIconColor_hover: string;
    headerIconColor_active: string;
    headerIconSeparatorBg: string;
    portableStatsBg: string;
    portableStatsColor: string;
    portableStatsValueColor: string;
    portableStatsValueColor_asc: string;
    portableStatsValueColor_desc: string;
    portableStatsValueColor_addition: string;
    portableStatsSeparator: string;
    hrefColor: string;
    hrefColor_hover: string;
    buttonPrimary: string;
    buttonPrimary_hover: string;
    buttonPrimaryColor: string;
    modalBg: string;
    modalColor: string;
    modalCloseColor: string;
    modalSpin: string;
    modalBorder: string;
    modalHeaderColor: string;
    tableTheadBg: string;
    tableTheadColor: string;
    tableTrBg_even: string;
    tableTrBg_odd: string;
    positiveColor: string;
    negativeColor: string;
    inactiveColor: string;
    statBlock_positive: string;
    statBlock_negative: string;
    statBlock_type1: string;
    statBlock_type2: string;
    statBlock_type3: string;
    chartLineColor: string;
    chartColor: string;
}

export interface StyledThemeProps {
    theme?: ThemeProps;
}

export const themesLight: ThemeProps = {
    type: "light",
    images: {
        logo: LogoLight,
        appBg: appBgLight,
        portableBg: portableBgDark,
        ad: adLight,
    },
    color: "#000",
    colorError: "#dd2c00",
    colorMain: "#1890ff",
    bg: "#fff",
    contentBg: "#fff",
    contentBgColor: "#cee7ff",
    footerColor: "#000",
    menuBg: "#e2e2e2",
    menuColor: "#000",
    menuSeparatorBg: "#f0f0f0",
    menuColor_hover: "#1890ff",
    menuColor_active: "#1890ff",
    menuColor_drop: "#808080",
    inputColor: "#000",
    InputPlaceholderColor: "#acacac",
    inputBorderColor: "#f4f4f4",
    inputIconColor: "#fff",
    inputSuccessColor: "#1890ff",
    inputErrorColor: "#dd2c00",
    suggestionsBg: "#fff",
    suggestionsBg_even: "#fff",
    suggestionsBg_hover: "#1890ff",
    suggestionsColor: "#000",
    suggestionsColor_hover: "#fff",
    headerIconColor: "#000",
    headerIconColor_hover: "#000",
    headerIconColor_active: "#52acff",
    headerIconSeparatorBg: "#f0f0f0",
    portableStatsBg: "#484849",
    portableStatsColor: "#484849",
    portableStatsValueColor: "#ff9b38",
    portableStatsValueColor_asc: "#67ce67",
    portableStatsValueColor_desc: "#dd2c00",
    portableStatsValueColor_addition: "#a5a5a5",
    portableStatsSeparator: "#6c6c6c",
    buttonPrimary: "#1890ff",
    buttonPrimary_hover: "#1781e1",
    buttonPrimaryColor: "#fff",
    hrefColor: "#1890ff",
    hrefColor_hover: "#3aa2ff",
    modalBg: "#fff",
    modalColor: "#000",
    modalCloseColor: "rgba(0,0,0,.45)",
    modalSpin: "#1890ff",
    modalBorder: "#e8e8e8",
    modalHeaderColor: "rgba(0,0,0,.65)",
    tableTheadBg: "#034786",
    tableTheadColor: "#fff",
    tableTrBg_even: "#e1f1ff",
    tableTrBg_odd: "#fff",
    positiveColor: "#089308",
    negativeColor: "#e50000",
    inactiveColor: "#999999",
    statBlock_positive: "#005d02",
    statBlock_negative: "#940300",
    statBlock_type1: "#034786",
    statBlock_type2: "#464646",
    statBlock_type3: "#7f7f7f",
    chartLineColor: "rgb(221, 221, 221)",
    chartColor: "#1890ff",
};

export const themesDark: ThemeProps = {
    type: "dark",
    images: {
        logo: LogoDark,
        appBg: appBgDark,
        portableBg: portableBgDark,
        ad: adDark,
    },
    color: "#fff",
    colorError: "#f74e4e",
    colorMain: "#ff9b38",
    bg: "#373634",
    contentBg: "#48484a",
    contentBgColor: "#555557",
    footerColor: "#acacac",
    menuBg: "#5c5c5e",
    menuColor: "#ffffff",
    menuSeparatorBg: "#6f6f6f",
    menuColor_hover: "#fb9938",
    menuColor_active: "#fb9938",
    menuColor_drop: "#808080",
    inputColor: "#fff",
    InputPlaceholderColor: "#acacac",
    inputBorderColor: "#808080",
    inputIconColor: "#d5d5d5",
    inputSuccessColor: "#fff",
    inputErrorColor: "#f74e4e",
    suggestionsBg: "#474748",
    suggestionsBg_even: "#7e7e7e",
    suggestionsBg_hover: "#acacac",
    suggestionsColor: "#fff",
    suggestionsColor_hover: "#000",
    headerIconColor: "#fff",
    headerIconColor_hover: "#fff",
    headerIconColor_active: "#fff",
    headerIconSeparatorBg: "#6f6f6f",
    portableStatsBg: "#484849",
    portableStatsColor: "#484849",
    portableStatsValueColor: "#ff9b38",
    portableStatsValueColor_asc: "#67ce67",
    portableStatsValueColor_desc: "#f74e4e",
    portableStatsValueColor_addition: "#a5a5a5",
    portableStatsSeparator: "#6c6c6c",
    buttonPrimary: "#ff9b38",
    buttonPrimary_hover: "#e18832",
    buttonPrimaryColor: "#fff",
    hrefColor: "#ff9b38",
    hrefColor_hover: "#ffbb7a",
    modalBg: "#373634",
    modalColor: "#fff",
    modalCloseColor: "rgba(255,255,255,.45)",
    modalSpin: "#ff9b38",
    modalBorder: "#444444",
    modalHeaderColor: "rgba(255,255,255,.65)",
    tableTheadBg: "#2a2a2a",
    tableTheadColor: "#fff",
    tableTrBg_even: "#505052",
    tableTrBg_odd: "#484849",
    positiveColor: "#67ce67",
    negativeColor: "#f74e4e",
    inactiveColor: "#858587",
    statBlock_positive: "#435d74",
    statBlock_negative: "#914b4b",
    statBlock_type1: "#3a3839",
    statBlock_type2: "#606060",
    statBlock_type3: "#808080",
    chartLineColor: "#555557",
    chartColor: "#1890ff",
};

export const GlobalLightStyle = `
    .ant-btn-primary {
        background-color: ${themesLight.buttonPrimary};
        border-color: ${themesLight.buttonPrimary};
        &:hover, &:active, &:focus {
            background-color: ${themesLight.buttonPrimary_hover};
            border-color: ${themesLight.buttonPrimary_hover};
        }
    }
    .ant-modal-content {
        background-color: ${themesLight.modalBg};
    }
    .ant-modal-title {
        color: ${themesLight.modalColor};
    }
    .ant-modal-close {
        color: ${themesLight.modalCloseColor};
    }
    .ant-modal-header {
        background: ${themesLight.modalBg};
        color: ${themesLight.modalHeaderColor};
        border-bottom: 1px solid ${themesLight.modalBorder};
    }
    .ant-modal-footer {
        border-top: 1px solid ${themesLight.modalBorder};
    }
    .ant-spin-dot-spin > i{
        background-color: ${themesLight.modalSpin};
    }
`;

export const GlobalDarkStyle = `
    a {
        color: ${themesDark.hrefColor};
        &:hover, &:focus, &:active {
            color: ${themesDark.buttonPrimary_hover};
        }
    }
    .ant-btn:focus, .ant-btn:hover {
        color: #ff9b38;
        border-color: #ff9b38;
    }
    .ant-btn-primary {
        color: ${themesDark.color};
        background-color: ${themesDark.buttonPrimary};
        border-color: ${themesDark.buttonPrimary};
        &:hover, &:active, &:focus {
            color: ${themesDark.color};
            background-color: ${themesDark.buttonPrimary_hover};
            border-color: ${themesDark.buttonPrimary_hover};
        }
    }
    .ant-modal-content {
        background-color: ${themesDark.modalBg};
    }
    .ant-modal-title {
        color: ${themesDark.modalColor};
    }
    .ant-modal-close {
        color: ${themesDark.modalCloseColor};
    }
    .ant-modal-header {
        background: ${themesDark.modalBg};
        color: ${themesDark.modalHeaderColor};
        border-bottom: 1px solid ${themesDark.modalBorder};
    }
    .ant-modal-footer {
        border-top: 1px solid ${themesDark.modalBorder};
    }
    .ant-spin-dot-spin > i{
        background-color: ${themesDark.modalSpin};
    }
    .ant-click-animating-node {
        border: 0 solid ${themesDark.colorMain};
    }
    .ant-input:focus, .ant-input:hover {
        border-color: ${themesDark.colorMain};
    }
    .ant-pagination-item-active, .ant-pagination-item {
        border-color: ${themesDark.colorMain};
        &:hover, &:focus {
            border-color: ${themesDark.colorMain};
        }
    }
    .ant-pagination-item-active, .ant-pagination-item {
        a {
            color: ${themesDark.hrefColor};
        }
        &:hover, &:focus, &:active {
            a {
                color: ${themesDark.buttonPrimary_hover};
            }
        }
    }
    .ant-form-item-label label {
        color: ${themesDark.color};
    }
    .ant-form-item-required:before{
        color: ${themesDark.colorError};
    }
    .has-error .ant-form-explain, .has-error .ant-form-split {
        color: ${themesDark.colorError};
    }
    .has-error .ant-input, .has-error .ant-input:hover {
        border-color: ${themesDark.colorError};
    }
    .ant-steps-item-process .ant-steps-item-icon {
        background: ${themesDark.colorMain};
    }
    .ant-steps-item-process .ant-steps-item-icon {
        border-color: ${themesDark.colorMain};
    }
    .ant-steps-item-process>.ant-steps-item-content>.ant-steps-item-title {
        color: ${themesDark.color};
    }
`;
