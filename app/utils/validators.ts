const emailRegExp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

export function emailFieldValidator(email: string) {
    if (!emailRegExp.test(email)) {
        return {
            ok: false,
            error: "Please enter correct e-mail",
        };
    }
    return {
        ok: true,
    };
}

export function requiredValidator(value: string) {
    if (value === undefined || value === null || value === "") {
        return {
            ok: false,
            error: "This field is required",
        };
    }
    return {
        ok: true,
    };
}

export function registerPageEmailFieldValidator(email: string) {
    if (!emailRegExp.test(email)) {
        return {
            ok: false,
            error: "The input is not valid E-mail!",
        };
    }
    return {
        ok: true,
    };
}

export function registerPageRequiredValidator(pass: string) {
    if (pass === undefined || pass === null || pass === "" || pass.length < 3) {
        return {
            ok: false,
            error: "Please input your password!",
        };
    } 
    return {
        ok: true,
    };
}

export function registerPageComparePasswordValidator(pass: string, confirm: string) {
    if (confirm !== pass) {
        return {
            ok: false,
            error: "Two passwords that you enter is inconsistent!",
        };
    }
    return {
        ok: true,
    };
}

export function registerPageInviteCodeValidator(inviteCode: string) {
    if (inviteCode === undefined || inviteCode === null || inviteCode === "") {
        return {
            ok: false,
            error: "Please input your invition code!",
        };
    }
    return {
        ok: true,
    };
}

