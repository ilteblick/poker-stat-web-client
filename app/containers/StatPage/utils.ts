import { IPlayer } from "./interfaces";

export const pokerSitePostfix = {
    2: "PS",
    12: "888",
    1: "CHI"
};

export function prepareLoadedPlayers(players: Array<IPlayer>) {
    return players.map((x: IPlayer) => ({
        id: x.id,
        playername: `${x.playername}`,
        pokersite_id: x.pokersite_id,
        postfix: `(${pokerSitePostfix[x.pokersite_id]})`,
    }));
}
