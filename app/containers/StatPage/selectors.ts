import { createSelector } from "reselect";

const selectStat = (state: any) => state.get("stat");

const selelctNeedToShowStats = () => createSelector(
    selectStat,
    (homeState) => homeState.get("needToShowStats")
);

const selelctBaseStats = () => createSelector(
    selectStat,
    (homeState) => homeState.get("baseStats")
);

const selelctBaseStatePortable = () => createSelector(
    selectStat,
    (homeState) => homeState.get("baseStatePortable")
);

const selelctGraph = () => createSelector(
    selectStat,
    (homeState) => homeState.get("graph")
);

const selelctCBetStats = () => createSelector(
    selectStat,
    (homeState) => homeState.get("cbetStats")
);

const selectNicknameToFind = () => createSelector(
    selectStat,
    (homeState) => homeState.get("nicknameToFind")
);

const selectPokersiteId = () => createSelector(
    selectStat,
    (homeState) => homeState.get("pokersite_id")
);

const selectPokerGameTypeToFind = () => createSelector(
    selectStat,
    (homeState) => homeState.get("pokerGameTypeToFind")
);

const selectLimitToFind = () => createSelector(
    selectStat,
    (homeState) => homeState.get("limitToFind")
);

const selectTableSizeToFind = () => createSelector(
    selectStat,
    (homeState) => homeState.get("tableSizeToFind")
);

const selectDateToFind = () => createSelector(
    selectStat,
    (homeState) => homeState.get("dateToFind")
);

const selectMonthResults = () => createSelector(
    selectStat,
    (homeState) => homeState.get("monthResults")
);

const selectAutocomletePlayers = () => createSelector(
    selectStat,
    (homeState) => homeState.get("autocompletePlayers")
);

const selectPostflopResults = () => createSelector(
    selectStat,
    (homeState) => homeState.get("postflopResults")
);

const selectLastSearchedNickname = () => createSelector(
    selectStat,
    (homeState) => homeState.get("lastSearchedNickname")
);

const selectNeedToShowChooseRoomPage = () => createSelector(
    selectStat,
    (homeState) => homeState.get("needToShowChooseRoomPage")
);

const selectPlayersToChooseRoom = () => createSelector(
    selectStat,
    (homeState) => homeState.get("playersToChooseRoom")
);

const selectAgrStats = () => createSelector(
    selectStat,
    (homeState) => homeState.get("agrStats")
);

const selectGraphSpinner = () => createSelector(
    selectStat,
    (homeState) => homeState.get("graphSpinner")
);

export {
    selectStat,
    selelctNeedToShowStats,
    selelctBaseStats,
    selelctBaseStatePortable,
    selelctGraph,
    selelctCBetStats,
    selectNicknameToFind,
    selectPokerGameTypeToFind,
    selectLimitToFind,
    selectTableSizeToFind,
    selectDateToFind,
    selectMonthResults,
    selectAutocomletePlayers,
    selectPostflopResults,
    selectPokersiteId,
    selectLastSearchedNickname,
    selectNeedToShowChooseRoomPage,
    selectPlayersToChooseRoom,
    selectAgrStats,
    selectGraphSpinner
};
