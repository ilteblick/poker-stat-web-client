export interface IPlayer {
    id: number;
    playername: string;
    pokersite_id: number;
}
