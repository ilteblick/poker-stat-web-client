import styled from "styled-components";
import {StyledThemeProps} from "../../utils/styleThemes";

export const StyledContainer = styled<StyledThemeProps, "div">("div")`
    max-width: 1100px;
    flex: 1 1 0%;
    width: 100%;
    @media (max-width: 1100px){
        align-self: flex-start;
    }
`;

export const StyledContent = styled<StyledThemeProps, "div">("div")`
    display: flex;
    margin: 48px 64px;
    box-shadow: rgba(0, 0, 0, 0.1) 0 0 64px;
    justify-content: center;
    border-radius: 4px;
    padding: 20px;
    background: ${props => props.theme.contentBg};
    color: ${props => props.theme.color};
`;
