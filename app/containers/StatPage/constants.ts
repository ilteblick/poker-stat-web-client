export interface IPokerGameTypeSelectItem {
    key: string;
    label: string;
    value: string;
}
export const PokerGameTypeSelect: IPokerGameTypeSelectItem[] = [
    {
        key: "all_game_types",
        label: "NL Holdem",
        value: undefined,
    },
    {
        key: "nl_holdem",
        label: "NL Holdem(without cap)",
        value: "nl_holdem",
    },
    // {
    //     key: "nl_omaha",
    //     label: "NL Omaha",
    //     value: "nl_omaha",
    // }
];

export interface ILimitSelectItem {
    key: string;
    label: string;
    value: string;
    neededSubscriptionType: number;
}
export const LimitSelect: ILimitSelectItem[] = [
    {
        key: "all_limits",
        label: "All limits",
        value: undefined,
        neededSubscriptionType: 0,
    },
    {
        key: "10",
        label: "10",
        value: "10",
        neededSubscriptionType: 0,
    },
    {
        key: "16",
        label: "16",
        value: "16",
        neededSubscriptionType: 0,
    },
    {
        key: "20",
        label: "20 (888)",
        value: "20",
        neededSubscriptionType: 0,
    },
    {
        key: "25",
        label: "25",
        value: "25",
        neededSubscriptionType: 1,
    },
    {
        key: "30",
        label: "30 (888)",
        value: "30",
        neededSubscriptionType: 1,
    },
    {
        key: "50",
        label: "50",
        value: "50",
        neededSubscriptionType: 1,
    },
    {
        key: "100",
        label: "100",
        value: "100",
        neededSubscriptionType: 2,
    },
    {
        key: "200",
        label: "200",
        value: "200",
        neededSubscriptionType: 2,
    },
    {
        key: "400",
        label: "400",
        value: "400",
        neededSubscriptionType: 2,
    },
    {
        key: "600",
        label: "600",
        value: "600",
        neededSubscriptionType: 2,
    },
    {
        key: "1000",
        label: "1K",
        value: "1000",
        neededSubscriptionType: 2,
    },
    {
        key: "2000",
        label: "2K",
        value: "2000",
        neededSubscriptionType: 2,
    },
    {
        key: "3000",
        label: "3K (888)",
        value: "3000",
        neededSubscriptionType: 2,
    },
    {
        key: "5000",
        label: "5K",
        value: "5000",
        neededSubscriptionType: 2,
    },
    {
        key: "10000",
        label: "10K",
        value: "10000",
        neededSubscriptionType: 2,
    },
    {
        key: "20000",
        label: "20K",
        value: "20000",
        neededSubscriptionType: 2,
    }
];

export interface ITableSizeSelectItem {
    key: string;
    label: string;
    value: string;
}
export const TableSizeSelect: ITableSizeSelectItem[] = [
    {
        key: "all_tables",
        label: "All tables",
        value: undefined,
    },
    {
        key: "2",
        label: "2max",
        value: "2",
    },
    {
        key: "6",
        label: "6max",
        value: "6",
    },
    ,
    {
        key: "9",
        label: "9max",
        value: "9",
    }
];

export interface IDateSelectItem {
    key: string;
    label: string;
    value: string;
}

export const PokerSiteSelect: any[] = [
    {
        key: "pokerstars",
        label: "PS",
        value: 2,
    },
    {
        key: "888",
        label: "888",
        value: 12,
    },
    {
        key: "chicko",
        label: "CHI",
        value: 1,
    },
];

export const DateSelect: IDateSelectItem[] = [
    {
        key: "all_dates",
        label: "All dates",
        value: undefined,
    },
    {
        key: "this_month",
        label: "This month",
        value: "this_month",
    },
    {
        key: "last_month",
        label: "Last month",
        value: "last_month",
    },
    {
        key: "last_three_months",
        label: "Last 3 months",
        value: "last_three_months",
    },
    {
        key: "last_half_year",
        label: "Last 6 months",
        value: "last_half_year",
    },
    {
        key: "last_twelve_months",
        label: "Last 12 months",
        value: "last_twelve_months",
    },
    {
        key: "last_year",
        label: "Last year",
        value: "last_year",
    },
    {
        key: "this_year",
        label: "This year",
        value: "this_year",
    },
];

export const FIND_STATS = "App/StatPage/FIND_STATS";
export const FIND_STATS_LOADED_ACTION = "App/StatPage/FIND_STATS_LOADED_ACTION";

export const CHANGE_NICKNAME_TO_FIND = "App/StatPage/CHANGE_NICKNAME_TO_FIND";
export const CHANGE_POKERGAMETYPE_TO_FIND = "App/StatPage/CHANGE_POKERGAMETYPE_TO_FIND";
export const CHANGE_LIMIT_TO_FIND = "App/StatPage/CHANGE_LIMIT_TO_FIND";
export const CHANGE_TABLE_SIZE_TO_FIND = "App/StatPage/CHANGE_TABLE_SIZE_TO_FIND";
export const CHANGE_DATE_TO_FIND = "App/StatPage/CHANGE_DATE_TO_FIND";
export const SET_LOADED_AUTOCOMPLETE_PLAYERS = "App/StatPage/SET_LOADED_AUTOCOMPLETE_PLAYERS";

export const GET_PLAYERS_TO_CHOOSE = "App/StatPage/GET_PLAYERS_TO_CHOOSE";
export const SET_LOADED_PLAYERS_TO_CHOOSE = "App/StatPage/SET_LOADED_PLAYERS_TO_CHOOSE";

export const SET_LOADED_AGR_FACTOR_STATS = "App/StatPage/SET_LOADED_AGR_FACTOR_STATS";
export const SET_LOADED_BASE_STATS = "App/StatPage/SET_LOADED_BASE_STATS";
export const SET_LOADED_GRAPH_STATS = "App/StatPage/SET_LOADED_GRAPH_STATS";

export const SHOW_LOADING_GRAPH_SPINNER = "App/StatPage/SHOW_LOADING_GRAPH_SPINNER";
export const SHOW_GRAPH_ERROR = "App/StatPage/SHOW_GRAPH_ERROR";

export const DROP_FILTERS = "App/StatPage/DROP_FILTERS";
