import { fromJS } from "immutable";

import {
    FIND_STATS_LOADED_ACTION,
    CHANGE_NICKNAME_TO_FIND,
    CHANGE_POKERGAMETYPE_TO_FIND,
    CHANGE_LIMIT_TO_FIND,
    CHANGE_TABLE_SIZE_TO_FIND,
    CHANGE_DATE_TO_FIND,
    PokerGameTypeSelect,
    LimitSelect,
    TableSizeSelect,
    DateSelect,
    SET_LOADED_AUTOCOMPLETE_PLAYERS,
    SET_LOADED_PLAYERS_TO_CHOOSE,
    SET_LOADED_AGR_FACTOR_STATS,
    SET_LOADED_BASE_STATS,
    SET_LOADED_GRAPH_STATS,
    SHOW_LOADING_GRAPH_SPINNER,
    SHOW_GRAPH_ERROR,
    GET_PLAYERS_TO_CHOOSE,
    DROP_FILTERS,
} from "./constants";
import { StatsType, BasicStatsType } from "./containers/PortableStats/components/StatsItem/types";
import { Action } from "../../basicClasses";

const initialState = fromJS({
    needToShowStats: false,
    graph: undefined,
    graphSpinner: false,
    baseStats: undefined,
    monthResults: undefined,
    agrStats: false,

    baseStatePortable: undefined,
    autocompletePlayers: [],


    nicknameToFind: undefined,
    pokersite_id: undefined,
    lastSearchedNickname: undefined,
    pokerGameTypeToFind: PokerGameTypeSelect[0].value,
    limitToFind: LimitSelect[0].value,
    tableSizeToFind: TableSizeSelect[0].value,
    dateToFind: DateSelect[0].value,


    needToShowChooseRoomPage: false,
    playersToChooseRoom: false,
});

function statReducer(state = initialState, action: Action) {
    switch (action.type) {
        case FIND_STATS_LOADED_ACTION: {
            const { nickname, pokersite_id, result } = action.payload;
            const statistics = result.mainStatsResult;
            const graph = result.graphResult;
            const home = result.home;


            const data = [{
                x: 0,
                y: 0,
            }];

            if (statistics && (statistics.hands === null || statistics.hands === undefined || statistics.hands === 0)) {
                return state.set("needToShowStats", false);
            }

            const color: BasicStatsType = statistics.winnings >= 0 ? "positive" : "negative";

            document.title = statistics.playername.toUpperCase();
            const baseStats: any = [
                {
                    label: "",
                    value: statistics.playername.toUpperCase(),
                    color: "#111111"
                },
                {
                    label: "Hands",
                    labelShortcut: "Hands",
                    value: statistics.hands,
                    color
                },
                {
                    label: "Home limit",
                    labelShortcut: "#HomeIcon",
                    value: home ? `NL${home}` : "NL -",
                    color
                },
                {
                    label: "Winnings",
                    labelShortcut: "Winnings",
                    value: !isNaN(statistics.winnings) ? statistics.winnings : "0.0",
                    color
                },

                {
                    label: "Winrate",
                    labelShortcut: "Winrate",
                    value: !isNaN(statistics.bb100) ? statistics.bb100 : "0.0",
                    color
                },
                {
                    label: "VPIP",
                    labelShortcut: "V",
                    value: !isNaN(statistics.vpip) ? statistics.vpip : "0.0",
                    color: "type1"
                },
                {
                    label: "PFR",
                    labelShortcut: "P",
                    value: !isNaN(statistics.pfr) ? statistics.pfr : "0.0",
                    color: "type1"
                },
                {
                    label: "3bet",
                    labelShortcut: "3bet",
                    value: !isNaN(statistics.threebet) ? statistics.threebet : "0.0",
                    color: "type1"
                },
                {
                    label: "F3bet",
                    labelShortcut: "F3bet",
                    value: !isNaN(statistics.foldedtothreebetpreflop) ? statistics.foldedtothreebetpreflop : "0.0",
                    color: "type1"
                },
                {
                    label: "Ag",
                    value: !isNaN(statistics.aggressive) ? statistics.aggressive : "0.0",
                    color: "type2"
                },
                {
                    label: "WWSF",
                    labelShortcut: "WWSF",
                    value: !isNaN(statistics.wonwhensawflop) ? statistics.wonwhensawflop : "0.0",
                    color: "type2"
                },
                {
                    label: "WTSD",
                    labelShortcut: "WTSD",
                    value: !isNaN(statistics.sawshowdown) ? statistics.sawshowdown : "0.0",
                    color: "type2"
                },
                {
                    label: "W$SD",
                    labelShortcut: "W$SD",
                    value: !isNaN(statistics.wonshowdown) ? statistics.wonshowdown : "0.0",
                    color: "type2"
                },
                {
                    label: "4bet",
                    labelShortcut: "4bet",
                    value: !isNaN(statistics.raisedthreebetpreflop) ? statistics.raisedthreebetpreflop : "0.0",
                    color: "type3",
                    mt: "24px",
                },
                {
                    label: "F4bet",
                    labelShortcut: "F4bet",
                    value: !isNaN(statistics.foldedtofourbetpreflop) ? statistics.foldedtofourbetpreflop : "0.0",
                    color: "type3",
                    mt: "24px",
                },
                {
                    label: "5bet",
                    value: !isNaN(statistics.raisedfourbetpreflop) ? statistics.raisedfourbetpreflop : "0.0",
                    color: "type3"
                },
                {
                    label: "SQZ",
                    value: !isNaN(statistics.didsqueeze) ? statistics.didsqueeze : "0.0",
                    color: "type3"
                },
                {
                    label: "CC",
                    value: !isNaN(statistics.didcoldcall) ? statistics.didcoldcall : "0.0",
                    color: "type3"
                },
                {
                    label: "Call 3bet",
                    value: !isNaN(statistics.calledthreebetpreflop) ? statistics.calledthreebetpreflop : "0.0",
                    color: "type3"
                },
                {
                    label: "SB resteal",
                    value: !isNaN(statistics.smallblindstealreraised) ? statistics.smallblindstealreraised : "0.0",
                    color: "type3"
                },
                {
                    label: "BB resteal",
                    value: !isNaN(statistics.bigblindstealreraised) ? statistics.bigblindstealreraised : "0.0",
                    color: "type3"
                },
                // {
                //     label: "FvsCBf",
                //     value: statistics.foldflopcbet,
                //     color: "#7f7d7e"
                // },
                // {
                //     label: "CBt",
                //     value: statistics.cbetturn,
                //     color: "#7f7d7e"
                // },
                // {
                //     label: "FvsCBt",
                //     value: statistics.foldturnbet,
                //     color: "#7f7d7e"
                // },
                // {
                //     label: "CBr",
                //     value: statistics.cbetriver,
                //     color: "#7f7d7e"
                // },
                // {
                //     label: "FvsCBr",
                //     value: statistics.foldriverbet,
                //     color: "#7f7d7e"
                // },
            ];

            const statsType: StatsType = statistics.winnings >= 0 ? "positive" : "negative";
            const BaseStatePortable = {
                playerName: {
                    label: "",
                    value: statistics.playername.toUpperCase(),
                },
                hands: {
                    label: "Hands",
                    labelShortcut: "Hands",
                    value: statistics.hands,
                },
                hl: {
                    label: "Home limit",
                    labelShortcut: "#HomeIcon",
                    value: home ? `NL${home}` : "NL -",
                },
                winnings: {
                    label: "Winnings",
                    labelShortcut: "Winnings",
                    value: !isNaN(statistics.winnings) ? statistics.winnings : "0.0",
                    statsType,
                },
                winrate: {
                    label: "Winrate",
                    labelShortcut: "Winrate",
                    value: !isNaN(statistics.bb100) ? statistics.bb100 : "0.0",
                    statsType,
                },
                vpip: {
                    label: "VPIP",
                    labelShortcut: "V",
                    value: !isNaN(statistics.vpip) ? statistics.vpip : "0.0",
                },
                pfr: {
                    label: "PFR",
                    labelShortcut: "P",
                    value: !isNaN(statistics.pfr) ? statistics.pfr : "0.0",
                },
                "3bet": {
                    label: "3bet",
                    labelShortcut: "3bet",
                    value: !isNaN(statistics.threebet) ? statistics.threebet : "0.0",
                },
                f3bet: {
                    label: "F3bet",
                    labelShortcut: "F3bet",
                    value: !isNaN(statistics.foldedtothreebetpreflop) ? statistics.foldedtothreebetpreflop : "0.0",
                },
                wwsf: {
                    label: "WWSF",
                    labelShortcut: "WWSF",
                    value: !isNaN(statistics.wonwhensawflop) ? statistics.wonwhensawflop : "0.0",
                },
                wtsd: {
                    label: "WTSD",
                    labelShortcut: "WTSD",
                    value: !isNaN(statistics.sawshowdown) ? statistics.sawshowdown : "0.0",
                },
                w$sd: {
                    label: "W$SD",
                    labelShortcut: "W$SD",
                    value: !isNaN(statistics.wonshowdown) ? statistics.wonshowdown : "0.0",
                },
                "4bet": {
                    label: "4bet",
                    labelShortcut: "4bet",
                    value: !isNaN(statistics.raisedthreebetpreflop) ? statistics.raisedthreebetpreflop : "0.0",
                },
                f4bet: {
                    label: "F4bet",
                    labelShortcut: "F4bet",
                    value: !isNaN(statistics.foldedtofourbetpreflop) ? statistics.foldedtofourbetpreflop : "0.0",
                }
            };

            const postflopResults = {
                cbf: !isNaN(statistics.cbetflop) ? statistics.cbetflop : "0.0",
                cbt: !isNaN(statistics.cbetturn) ? statistics.cbetturn : "0.0",
                cbr: !isNaN(statistics.cbetriver) ? statistics.cbetriver : "0.0",

                fvcbf: !isNaN(statistics.foldflopcbet) ? statistics.foldflopcbet : "0.0",
                fvcbt: !isNaN(statistics.foldturncbet) ? statistics.foldturncbet : "0.0",
                fvcbr: !isNaN(statistics.foldrivercbet) ? statistics.foldrivercbet : "0.0",

                agF: 0,
                agT: 0,
                agR: 0,

                rcbf: !isNaN(statistics.raisedflopcontinuationbet) ? statistics.raisedflopcontinuationbet : "0.0",
                rcbt: !isNaN(statistics.raisedturncontinuationbet) ? statistics.raisedturncontinuationbet : "0.0",
                rcbr: !isNaN(statistics.raisedrivercontinuationbet) ? statistics.raisedrivercontinuationbet : "0.0",
            };

            let sum = 0;

            const winsFromHm = statistics.winnings;
            let winsFromDb = 0;

            let dif = 0;

            if (winsFromDb && graph.length !== 0) {
                dif = (+winsFromHm - winsFromDb) / graph.length;
            }

            if (graph) {
                for (const point of graph) {
                    sum += parseFloat(point.sum);
                    sum += dif;
                    data.push({
                        x: (point.num + 1),
                        y: sum,
                    });
                }
            }

            const graphToView = [{
                id: "graph",
                data,
            }];

            return state
                .set("needToShowStats", true)
                .set("baseStats", baseStats)
                .set("baseStatePortable", BaseStatePortable)
                .set("graph", graphToView)
                .set("monthResults", result.monthResults.sort((a: any, b: any) => +b.month - +a.month))
                .set("postflopResults", postflopResults)
                .set("nicknameToFind", nickname)
                .set("pokersite_id", pokersite_id)
                .set("lastSearchedNickname", nickname)
                .set("needToShowChooseRoomPage", false);
        }
        case CHANGE_NICKNAME_TO_FIND:
            return changeNicknameToFind(state, action);
        case CHANGE_POKERGAMETYPE_TO_FIND:
            return changePokerGameTypeToFind(state, action);
        case CHANGE_LIMIT_TO_FIND:
            return changeLimitToFind(state, action);
        case CHANGE_TABLE_SIZE_TO_FIND:
            return changeTableSizeToFind(state, action);
        case CHANGE_DATE_TO_FIND:
            return changeDateToFind(state, action);
        case SET_LOADED_AUTOCOMPLETE_PLAYERS:
            return setLoadedPlayers(state, action);
        case SET_LOADED_PLAYERS_TO_CHOOSE:
            return state.set("playersToChooseRoom", action.payload)
                .set("needToShowChooseRoomPage", true);
        case SET_LOADED_AGR_FACTOR_STATS:
            return state.set("agrStats", action.payload);
        case SET_LOADED_BASE_STATS:
            return setLoadedBaseStats(state, action);
        case SET_LOADED_GRAPH_STATS:
            return setLoadedGraphStats(state, action);
        case SHOW_LOADING_GRAPH_SPINNER:
            return state.set("graphSpinner", true);
        case SHOW_GRAPH_ERROR:
            return state.set("graphSpinner", false);
        case GET_PLAYERS_TO_CHOOSE:
            return initialState;
        case DROP_FILTERS:
            return state.set("pokerGameTypeToFind", PokerGameTypeSelect[0].value)
            .set("limitToFind", LimitSelect[0].value)
            .set("tableSizeToFind", TableSizeSelect[0].value)
            .set("dateToFind", DateSelect[0].value)
        default:
            return state;
    }
}

function setLoadedGraphStats(state: any, action: Action) {
    const { graphStats } = action.payload;

    // const statPlayername = state.get("nicknameToFind");
    // console.log(statPlayername, player.playername);
    // if (statPlayername === player.playername) {
    //     let sum = 0;
    //     const graph = [];
    //     if (graphStats) {
    //         for (const point of graphStats) {
    //             sum += parseFloat(point.sum);
    //             // sum += dif;
    //             graph.push({
    //                 x: (point.num + 1),
    //                 y: sum,
    //             });
    //         }
    //     }

    //     const graphToView = [{
    //         id: "graph",
    //         graph,
    //     }];

    //     return state.set("graph", graphToView);
    // }

    return state.set("graph", graphStats).set("graphSpinner", false);
}

function setLoadedBaseStats(state: any, action: Action) {
    const { baseStats: statistics, monthResults, home, player } = action.payload;

    const color: BasicStatsType = statistics.winnings >= 0 ? "positive" : "negative";

    document.title = statistics.playername.toUpperCase();
    const baseStats: any = [
        {
            label: "",
            value: statistics.playername.toUpperCase(),
            color: "#111111"
        },
        {
            label: "Hands",
            labelShortcut: "Hands",
            value: statistics.hands,
            color
        },
        {
            label: "Home limit",
            labelShortcut: "#HomeIcon",
            value: home ? `NL${home}` : "NL -",
            color
        },
        {
            label: "Winnings",
            labelShortcut: "Winnings",
            value: !isNaN(statistics.winnings) ? statistics.winnings : "0.0",
            color
        },

        {
            label: "Winrate",
            labelShortcut: "Winrate",
            value: !isNaN(statistics.bb100) ? statistics.bb100 : "0.0",
            color
        },
        {
            label: "VPIP",
            labelShortcut: "V",
            value: !isNaN(statistics.vpip) ? statistics.vpip : "0.0",
            color: "type1"
        },
        {
            label: "PFR",
            labelShortcut: "P",
            value: !isNaN(statistics.pfr) ? statistics.pfr : "0.0",
            color: "type1"
        },
        {
            label: "3bet",
            labelShortcut: "3bet",
            value: !isNaN(statistics.threebet) ? statistics.threebet : "0.0",
            color: "type1"
        },
        {
            label: "F3bet",
            labelShortcut: "F3bet",
            value: !isNaN(statistics.foldedtothreebetpreflop) ? statistics.foldedtothreebetpreflop : "0.0",
            color: "type1"
        },
        {
            label: "Ag",
            value: !isNaN(statistics.aggressive) ? statistics.aggressive : "0.0",
            color: "type2"
        },
        {
            label: "WWSF",
            labelShortcut: "WWSF",
            value: !isNaN(statistics.wonwhensawflop) ? statistics.wonwhensawflop : "0.0",
            color: "type2"
        },
        {
            label: "WTSD",
            labelShortcut: "WTSD",
            value: !isNaN(statistics.sawshowdown) ? statistics.sawshowdown : "0.0",
            color: "type2"
        },
        {
            label: "W$SD",
            labelShortcut: "W$SD",
            value: !isNaN(statistics.wonshowdown) ? statistics.wonshowdown : "0.0",
            color: "type2"
        },
        {
            label: "4bet",
            labelShortcut: "4bet",
            value: !isNaN(statistics.raisedthreebetpreflop) ? statistics.raisedthreebetpreflop : "0.0",
            color: "type3",
            mt: "24px",
        },
        {
            label: "F4bet",
            labelShortcut: "F4bet",
            value: !isNaN(statistics.foldedtofourbetpreflop) ? statistics.foldedtofourbetpreflop : "0.0",
            color: "type3",
            mt: "24px",
        },
        {
            label: "5bet",
            value: !isNaN(statistics.raisedfourbetpreflop) ? statistics.raisedfourbetpreflop : "0.0",
            color: "type3"
        },
        {
            label: "SQZ",
            value: !isNaN(statistics.didsqueeze) ? statistics.didsqueeze : "0.0",
            color: "type3"
        },
        {
            label: "CC",
            value: !isNaN(statistics.didcoldcall) ? statistics.didcoldcall : "0.0",
            color: "type3"
        },
        {
            label: "Call 3bet",
            value: !isNaN(statistics.calledthreebetpreflop) ? statistics.calledthreebetpreflop : "0.0",
            color: "type3"
        },
        {
            label: "SB resteal",
            value: !isNaN(statistics.smallblindstealreraised) ? statistics.smallblindstealreraised : "0.0",
            color: "type3"
        },
        {
            label: "BB resteal",
            value: !isNaN(statistics.bigblindstealreraised) ? statistics.bigblindstealreraised : "0.0",
            color: "type3"
        },
    ];

    const statsType: StatsType = statistics.winnings >= 0 ? "positive" : "negative";
    const BaseStatePortable = {
        playerName: {
            label: "",
            value: statistics.playername.toUpperCase(),
        },
        hands: {
            label: "Hands",
            labelShortcut: "Hands",
            value: statistics.hands,
        },
        hl: {
            label: "Home limit",
            labelShortcut: "#HomeIcon",
            value: home ? `NL${home}` : "NL -",
        },
        winnings: {
            label: "Winnings",
            labelShortcut: "Winnings",
            value: !isNaN(statistics.winnings) ? statistics.winnings : "0.0",
            statsType,
        },
        winrate: {
            label: "Winrate",
            labelShortcut: "Winrate",
            value: !isNaN(statistics.bb100) ? statistics.bb100 : "0.0",
            statsType,
        },
        vpip: {
            label: "VPIP",
            labelShortcut: "V",
            value: !isNaN(statistics.vpip) ? statistics.vpip : "0.0",
        },
        pfr: {
            label: "PFR",
            labelShortcut: "P",
            value: !isNaN(statistics.pfr) ? statistics.pfr : "0.0",
        },
        "3bet": {
            label: "3bet",
            labelShortcut: "3bet",
            value: !isNaN(statistics.threebet) ? statistics.threebet : "0.0",
        },
        f3bet: {
            label: "F3bet",
            labelShortcut: "F3bet",
            value: !isNaN(statistics.foldedtothreebetpreflop) ? statistics.foldedtothreebetpreflop : "0.0",
        },
        wwsf: {
            label: "WWSF",
            labelShortcut: "WWSF",
            value: !isNaN(statistics.wonwhensawflop) ? statistics.wonwhensawflop : "0.0",
        },
        wtsd: {
            label: "WTSD",
            labelShortcut: "WTSD",
            value: !isNaN(statistics.sawshowdown) ? statistics.sawshowdown : "0.0",
        },
        w$sd: {
            label: "W$SD",
            labelShortcut: "W$SD",
            value: !isNaN(statistics.wonshowdown) ? statistics.wonshowdown : "0.0",
        },
        "4bet": {
            label: "4bet",
            labelShortcut: "4bet",
            value: !isNaN(statistics.raisedthreebetpreflop) ? statistics.raisedthreebetpreflop : "0.0",
        },
        f4bet: {
            label: "F4bet",
            labelShortcut: "F4bet",
            value: !isNaN(statistics.foldedtofourbetpreflop) ? statistics.foldedtofourbetpreflop : "0.0",
        }
    };

    const postflopResults = {
        cbf: !isNaN(statistics.cbetflop) ? statistics.cbetflop : "-",
        cbt: !isNaN(statistics.cbetturn) ? statistics.cbetturn : "-",
        cbr: !isNaN(statistics.cbetriver) ? statistics.cbetriver : "-",

        fvcbf: !isNaN(statistics.foldflopcbet) ? statistics.foldflopcbet : "-",
        fvcbt: !isNaN(statistics.foldturncbet) ? statistics.foldturncbet : "-",
        fvcbr: !isNaN(statistics.foldrivercbet) ? statistics.foldrivercbet : "-",

        rcbf: !isNaN(statistics.raisedflopcontinuationbet) ? statistics.raisedflopcontinuationbet : "-",
        rcbt: !isNaN(statistics.raisedturncontinuationbet) ? statistics.raisedturncontinuationbet : "-",
        rcbr: !isNaN(statistics.raisedrivercontinuationbet) ? statistics.raisedrivercontinuationbet : "-",
    };

    return state.set("needToShowStats", true)
        .set("baseStats", baseStats)
        .set("baseStatePortable", BaseStatePortable)
        .set("monthResults", monthResults.sort((a: any, b: any) => +b.month - +a.month))
        .set("postflopResults", postflopResults)
        // .set("nicknameToFind", player.playername)
        .set("nicknameToFind", "")
        .set("pokersite_id", player.pokersite_id)
        .set("lastSearchedNickname", player.playername)
        .set("needToShowChooseRoomPage", false);
}

function setLoadedPlayers(state: any, action: Action) {
    const players = action.payload;
    return state.set("autocompletePlayers", players.map((x: any) => ({
        label: x.playername,
        postfix: x.postfix,
        pokersite_id: x.pokersite_id
    })));
}

function changeNicknameToFind(state: any, action: Action) {
    const { nickname, pokersite_id } = action.payload;
    return state.set("nicknameToFind", nickname)
        .set("pokersite_id", pokersite_id)
        .set("autocompletePlayers", []);
}

function changePokerGameTypeToFind(state: any, action: Action) {
    return state.set("pokerGameTypeToFind", action.payload);
}

function changeLimitToFind(state: any, action: Action) {
    return state.set("limitToFind", action.payload);
}

function changeTableSizeToFind(state: any, action: Action) {
    return state.set("tableSizeToFind", action.payload);
}

function changeDateToFind(state: any, action: Action) {
    return state.set("dateToFind", action.payload);
}

export default statReducer;
