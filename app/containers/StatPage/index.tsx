import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";

import {
    getPlayersToChoose,
} from "./actions";

import saga from "./sagas";
import reducer from "./reducer";

import { selectNeedToShowChooseRoomPage } from "./selectors";
import { STAT_PAGE } from "../App/constants";
import {selectUser, selectPortableMode, selectCurrentPage} from "../App/selectors";
import { IUser } from "../App/interfaces";
import authorize from "../../utils/authorize";
import { showPage } from "../App/actions";

import FullScreenStatsPage from "./containers/FullScreenStats";
import FullScreenChooseRoomPage from "./containers/FullScreenChooseRoomPage";
import PortableLayout from "./containers/PortableLayout";
import {checkPortableMode} from "../App/utils";

import {StyledContent, StyledContainer} from "./styled";

interface StatProps {
    location: any;
    user: IUser;
    onGetPlayersToChoose: Function;

    onShowPage: Function;
    needToShowChooseRoom: boolean;
    portableMode: boolean;
    currentPage: string;
}

class StatPage extends React.PureComponent<StatProps, {}> {
    constructor(props: StatProps) {
        super(props);

        this.props.onShowPage(STAT_PAGE);
    }

    componentWillMount() {
        const { location } = this.props;

        let nickname;
        if (window.URLSearchParams) {
            const params = new URLSearchParams(location.search);
            nickname = params.get("nickname");
        } else {
            nickname = new RegExp("[\?&]" + "nickname" + "=([^&#]*)").exec(location.search)[1];
        }
        if (nickname) {
            this.props.onGetPlayersToChoose(nickname);
        }
    }

    render() {
        const { user, needToShowChooseRoom, portableMode, currentPage } = this.props;

        if (!user) {
            return null;
        }
        if (checkPortableMode(currentPage, portableMode)) {
            return <PortableLayout needToShowRoom={needToShowChooseRoom} />;
        }
        const { subscription_type, is_admin } = user;
        if (!is_admin && !Number.isInteger(subscription_type)) {
            return (
                <StyledContainer>
                    <StyledContent>Subscribe. Then you can get access this page.</StyledContent>
                </StyledContainer>
            );
        }
        if (needToShowChooseRoom) {
            return <FullScreenChooseRoomPage />;
        } else {
            return <FullScreenStatsPage />;
        }
    }
}

const mapStateToProps = createStructuredSelector({
    user: selectUser(),
    needToShowChooseRoom: selectNeedToShowChooseRoomPage(),
    portableMode: selectPortableMode(),
    currentPage: selectCurrentPage(),
});

export function mapDispatchToProps(dispatch: Function) {
    return {
        onShowPage: (key: string) => dispatch(showPage(key)),
        onGetPlayersToChoose: (nickname: string) => dispatch(getPlayersToChoose(nickname)),
    };
}


const withConnect = connect<{}, {}, StatProps>(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "stat", reducer });
const withSaga = injectSaga({ key: "stat", saga });

const withAuth = authorize({ pageKey: STAT_PAGE });

export default withRouter(compose(
    withAuth,
    withReducer,
    withSaga,
    withConnect,
)(StatPage) as any);
