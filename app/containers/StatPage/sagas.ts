import { fork, take, put, race, select, cancel } from "redux-saga/effects";
import * as qs from "qs";
import { message } from "antd";
import { FIND_STATS, CHANGE_NICKNAME_TO_FIND, GET_PLAYERS_TO_CHOOSE } from "./constants";
import {
    setLoadedPlayers, findStats, setLoadedPlayersToChoose,
    setLoadedArgFactorStats, setLoadedBaseStats, setLoadedGraphStats, showLoadingGraphSpinner, showGraphError, dropFilters
} from "./actions";
import { apiService } from "../../api/actionService/api-service";
import {
    selectNicknameToFind, selectPokerGameTypeToFind, selectLimitToFind,
    selectTableSizeToFind, selectDateToFind, selectPokersiteId, selectLastSearchedNickname
} from "./selectors";
import { prepareLoadedPlayers } from "./utils";

// const find_stats_url_segment = "stats";

const get_autocomlete_platers_url_segment = "players";
const get_players_to_choose_url_segment = "playersToChoose";
// const get_players_agr_factor_url_segment = "agr-factor";
const check_permissions_url_segment = "check-permissions";

const get_players_agr_factor_url_segment = "statistics/agr-factor";
const get_players_graph_stats_url_segment = "statistics/graph-stats";
const get_players_base_stats_url_segment = "statistics/base-stats";

// const findStatsLyfecycle = apiService.findLifecycle(find_stats_url_segment);
const getPlayersLyfecycle = apiService.findLifecycle(get_autocomlete_platers_url_segment);
const getPlayersToChooseLyfecycle = apiService.findLifecycle(get_players_to_choose_url_segment);

const checkPermissionsLyfecycle = apiService.findLifecycle(check_permissions_url_segment);

const getPlayersAgrFactorLyfecycle = apiService.findLifecycle(get_players_agr_factor_url_segment);
const getPlayersGraphStatsLyfecycle = apiService.findLifecycle(get_players_graph_stats_url_segment);
const getPlayersBaseStatsLyfecycle = apiService.findLifecycle(get_players_base_stats_url_segment);


function* findAgrFactor(query: any) {
    yield put(apiService.find(get_players_agr_factor_url_segment, query, false));

    const { result, error } = yield race({
        result: take(getPlayersAgrFactorLyfecycle.RESOLVED),
        error: take(getPlayersAgrFactorLyfecycle.REJECTED),
    });

    if (error) {
        // yield put(setLoadedArgFactorStats(null));
    }

    if (result) {
        yield put(setLoadedArgFactorStats(result.payload.agrStats));
    }
}

function* findGraphStats(query: any) {
    yield put(apiService.find(get_players_graph_stats_url_segment, query, false));

    yield put(showLoadingGraphSpinner());

    const { result, error } = yield race({
        result: take(getPlayersGraphStatsLyfecycle.RESOLVED),
        error: take(getPlayersGraphStatsLyfecycle.REJECTED),
    });

    if (error) {
        yield put(showGraphError());
        // yield put(setLoadedArgFactorStats(null));
    }

    if (result) {
        const { graphStats, player } = result.payload;
        yield put(setLoadedGraphStats(graphStats, player));
    }
}
function* findBaseStats(query: any) {
    yield put(apiService.find(get_players_base_stats_url_segment, query));

    const { result, error } = yield race({
        result: take(getPlayersBaseStatsLyfecycle.RESOLVED),
        error: take(getPlayersBaseStatsLyfecycle.REJECTED),
    });

    if (error) {
        // yield put(setLoadedArgFactorStats(null));
        message.error(error.error.message);
        yield put(showGraphError());
    }

    if (result) {
        const { stats, monthResults, home, player } = result.payload;
        yield put(setLoadedBaseStats(stats, monthResults, home, player));

        let newUrl = "/statistics";
        const nickname = player.playername;
        if (nickname !== undefined || nickname !== null || nickname !== "") {
            const querystring = qs.stringify({ nickname });
            newUrl = `${newUrl}?${querystring}`;
        }
        window.history.pushState({}, "", newUrl);
    }
}

function* watchFindStat() {
    let graphStatsRequest;
    let argStatsRequest;
    while (true) {
        // const { payload } = yield take(FIND_STATS);
        // let { nickname, pokersite_id } = payload;

        // if (!nickname) {
        //     nickname = yield select(selectNicknameToFind());
        // }

        // if (!pokersite_id) {
        //     pokersite_id = yield select(selectPokersiteId());
        // }

        // const pokerGameType = yield select(selectPokerGameTypeToFind());
        // const limit = yield select(selectLimitToFind());
        // const tableSize = yield select(selectTableSizeToFind());
        // const date = yield select(selectDateToFind());

        // if (nickname === undefined || nickname === null || nickname === "") {
        //     message.error("Enter nickname");
        //     continue;
        // }

        // const query = {
        //     nickname,
        //     pokersite_id,
        //     pokerGameType,
        //     limit,
        //     tableSize,
        //     date,
        // };

        // yield fork(findAgrFactor, query);

        // yield put(apiService.find(find_stats_url_segment, query));

        // const { result, error } = yield race({
        //     result: take(findStatsLyfecycle.RESOLVED),
        //     error: take(findStatsLyfecycle.REJECTED),
        // });

        // if (error) {
        //     message.error(error.error.message);
        //     continue;
        // }

        // const statistics = result.payload.mainStatsResult;
        // if (statistics && !statistics.hands) {
        //     message.warning("There is no data for these filters");
        // }

        // yield put(findStatsLoadedAction(result.payload, nickname, pokersite_id));

        // let newUrl = "/statistics";
        // if (nickname !== undefined || nickname !== null || nickname !== "") {
        //     const querystring = qs.stringify({ nickname });
        //     newUrl = `${newUrl}?${querystring}`;
        // }
        // window.history.pushState({}, "", newUrl);

        const { payload } = yield take(FIND_STATS);

        if (graphStatsRequest) {
            yield cancel(graphStatsRequest);
        }

        if (argStatsRequest) {
            yield cancel(argStatsRequest);
        }


        let { nickname, pokersite_id } = payload;

        if(nickname && pokersite_id){
            yield put(dropFilters());
        }

        if (!nickname) {
            nickname = yield select(selectLastSearchedNickname());
            if (!nickname) {
                nickname = yield select(selectNicknameToFind());
            }
        }

        if (!pokersite_id) {
            pokersite_id = yield select(selectPokersiteId());
        }

        if (nickname === undefined || nickname === null || nickname === "") {
            message.error("Enter nickname");
            continue;
        }

        const limit = yield select(selectLimitToFind());

        yield put(apiService.find(check_permissions_url_segment, { playername: nickname, pokersite_id, limit }, false));

        const { checkPermissionResult, checkPermissionError } = yield race({
            checkPermissionResult: take(checkPermissionsLyfecycle.RESOLVED),
            checkPermissionError: take(checkPermissionsLyfecycle.REJECTED),
        });

        if (checkPermissionError) {
            continue;
        }



        if (checkPermissionResult) {
            const pokerGameType = yield select(selectPokerGameTypeToFind());

            const tableSize = yield select(selectTableSizeToFind());
            const date = yield select(selectDateToFind());

            const query = {
                playername: nickname,
                pokersite_id,
                pokerGameType,
                limit,
                tableSize,
                date,
            };

            yield fork(findBaseStats, query);
            argStatsRequest = yield fork(findAgrFactor, query);
            graphStatsRequest = yield fork(findGraphStats, query);

            continue;
        }



        // const query = {
        //     nickname,
        //     pokersite_id,
        //     pokerGameType,
        //     limit,
        //     tableSize,
        //     date,
        // };

        // yield fork(findAgrFactor, query);

        // yield put(apiService.find(find_stats_url_segment, query));

        // const { result, error } = yield race({
        //     result: take(findStatsLyfecycle.RESOLVED),
        //     error: take(findStatsLyfecycle.REJECTED),
        // });

        // if (error) {
        //     message.error(error.error.message);
        //     continue;
        // }

        // const statistics = result.payload.mainStatsResult;
        // if (statistics && !statistics.hands) {
        //     message.warning("There is no data for these filters");
        // }

        // yield put(findStatsLoadedAction(result.payload, nickname, pokersite_id));

        // let newUrl = "/statistics";
        // if (nickname !== undefined || nickname !== null || nickname !== "") {
        //     const querystring = qs.stringify({ nickname });
        //     newUrl = `${newUrl}?${querystring}`;
        // }
        // window.history.pushState({}, "", newUrl);
    }
}

function* watchChangeNickname() {
    while (true) {
        const { payload } = yield take(CHANGE_NICKNAME_TO_FIND);
        const { nickname, ignoreSuggests } = payload;

        if (!ignoreSuggests) {
            yield put(apiService.find(get_autocomlete_platers_url_segment, { nickname }, false));

            const { result, error } = yield race({
                result: take(getPlayersLyfecycle.RESOLVED),
                error: take(getPlayersLyfecycle.REJECTED),
            });

            if (result && result.payload.players) {
                const players = prepareLoadedPlayers(result.payload.players);
                yield put(setLoadedPlayers(players));
            }

            if (error) {
                yield put(setLoadedPlayers([]));
            }
        }
    }
}

function* watchGetPlayersToChoose() {
    while (true) {
        const { payload: nickname } = yield take(GET_PLAYERS_TO_CHOOSE);

        const querystring = qs.stringify({ nickname });
        const newUrl = `/statistics?${querystring}`;
        window.history.pushState({}, "", newUrl);

        yield put(apiService.find(get_players_to_choose_url_segment, { nickname }));

        const { result, error } = yield race({
            result: take(getPlayersToChooseLyfecycle.RESOLVED),
            error: take(getPlayersToChooseLyfecycle.REJECTED),
        });

        if (error) {
            message.error(error.error.message);
            continue;
        }

        if (result) {
            const { players } = result.payload;
            if (players.length === 1) {
                yield put(findStats(players[0].playername, players[0].pokersite_id));
            } else {
                yield put(setLoadedPlayersToChoose(players));
            }
        }
    }
}

export default function* statSaga() {
    yield fork(watchFindStat);
    yield fork(watchChangeNickname);
    yield fork(watchGetPlayersToChoose);
}
