import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { List } from "antd";

import { selelctBaseStats, selectPokersiteId } from "../../selectors";

import { StyledNickname, StyledRateBlock } from "./styled";
import { pokerSitePostfix } from "../../utils";

export interface BaseStatsProps { baseStats?: any; pokersite_id: string; }

class BaseStats extends React.PureComponent<BaseStatsProps, {}> {

    render() {
        const { baseStats, pokersite_id } = this.props;

        const statsToShow = baseStats.slice();
        delete statsToShow[0];

        return (
            <div style={{ width: "100%", display: "flex", flexWrap: "wrap", flexDirection: "column" }}>
                <div style={{ width: "100%", display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                    <StyledNickname>
                        {baseStats[0].value} ({pokerSitePostfix[pokersite_id]})
                    </StyledNickname>
                </div>
                <div style={{ width: "100%", display: "flex", flexWrap: "wrap" }}>
                    {statsToShow.map((x: any, index: number) => {
                        return (
                            <div
                                key={`rate-${index}`}
                                style={{
                                    width: "50%", padding: 6, marginTop: x.mt,
                                }}>
                                <StyledRateBlock type={x.color}>
                                    <span style={{ color: "white", fontWeight: "bold", fontSize: 14 }}>{x.label}</span>
                                    <span style={{ color: "white", fontWeight: "bold", fontSize: 14 }}>{x.value}</span>
                                </StyledRateBlock>
                            </div>
                        );
                    })}
                </div>
            </div >
        );

        return (
            baseStats ?
                <div style={{ width: "100%" }}><List
                    size="small"
                    bordered
                    dataSource={baseStats}
                    renderItem={(item: any) => (
                        <List.Item>
                            <List.Item.Meta
                                title={item.label}
                                description={item.value}
                            />
                        </List.Item>
                    )} /></div>
                : null
        );
    }
}

const mapStateToProps = createStructuredSelector({
    baseStats: selelctBaseStats(),
    pokersite_id: selectPokersiteId(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
    withConnect,
)(BaseStats);
