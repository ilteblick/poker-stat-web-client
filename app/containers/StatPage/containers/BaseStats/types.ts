import {StyledThemeProps} from "../../../../utils/styleThemes";

export interface StyledRateBlockProps extends StyledThemeProps {
    type?: string;
}
