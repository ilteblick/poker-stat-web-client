import styled from "styled-components";
import { StyledThemeProps } from "../../../../utils/styleThemes";
import { StyledRateBlockProps } from "./types";

export const StyledNickname = styled<StyledThemeProps, "span">("span")`
    font-size: 24px;
    color: ${props => props.theme.color};
    font-weight: bold;
    padding: 6px;
`;

export const StyledRateBlock = styled<StyledRateBlockProps, "div">("div")`
    width: 100%;
    display: flex;
    justify-content: space-between;
    padding: 10px;
    background: ${props => props.theme[`statBlock_${props.type}`]};
    border-radius: 4px;
`;
