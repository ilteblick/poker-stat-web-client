import styled from "styled-components";

export const StyledSpan = styled.span`
    font-size: 18px;
    color: black;
    cursor: pointer;

    margin-right: 48px;

    &:hover{
        text-decoration: underline;
    }
`;

export const Img = styled.img`
    @media (max-width: 750px){
        display: none;
    }
`;

export const StyledContainer = styled.section`
    padding: 16px 10px;
    flex: 1 0 auto;
    height: 0;
    overflow: auto;
    display: flex;
    flex-direction: column;
    ${props => props.srcImage ? `background: url(${props.srcImage}) right no-repeat` : "" }
`;

export const StyledTitle = styled.div`
    font-size: 17px;
    width: 100%;
    line-height: 1em;
`;

export const StyledContent = styled.div`
    flex: 1 0 auto;
    display: flex;
    flex-direction: column;
`;

export const StyledItems = styled.ul`
    margin-top: 14px;
    list-style: none;
    padding-left: 8px;
`;

export const StyledItem = styled.li`
    position: relative;
    cursor: pointer;
    line-height: 1em;
    margin-bottom: 10px;
    :hover {
        text-decoration: underline;
    }
    :before {
        content: "·";
        font-size: 30px;
        line-height: 14px;
        position: absolute;
        left: -10px;
        display: inline-block;
        height: 14px;
        top: 50%;
        margin-top: -7px;
        color: ${props => props.theme.colorMain}
    }
`;
