import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { selectPlayersToChooseRoom } from "../../selectors";
import { findStats, changeNicknameToFind } from "../../actions";
import { StyledItems, StyledContainer, StyledTitle, StyledContent, StyledItems, StyledItem } from "./styled";
import { pokerSitePostfix } from "../../utils";
import PortableContent from "../PortableContent";

const BackgroundStatistic = require("../../../../../static/background_portable_statistic.png");

interface FullScreenChooseRoomProps {
    playersToChooseRoom?: any;
    onFindStats?: Function;
    onChangeNicknameToFind?: Function;
}

class PortableChooseRoomPage extends React.PureComponent<FullScreenChooseRoomProps, {}> {
    onSelectPlayerToShow = (x: any) => {
        this.props.onChangeNicknameToFind(x.playername, x.pokersite_id, true);
        this.props.onFindStats(x.playername, x.pokersite_id);
    }

    render() {
        const { playersToChooseRoom } = this.props;


        const content = playersToChooseRoom && playersToChooseRoom.length > 0
            ? <StyledItems>
                {playersToChooseRoom.map((x: any) => (
                    <StyledItem onClick={() => this.onSelectPlayerToShow(x)}>
                        {x.playername} ({pokerSitePostfix[x.pokersite_id]})
                    </StyledItem>
                ))}
            </StyledItems> : null;

        return (
            <PortableContent>
                <StyledContainer srcImage={BackgroundStatistic}>
                    <StyledTitle>
                        Select poker room
                    </StyledTitle>
                    <StyledContent>
                        {content}
                    </StyledContent>
                </StyledContainer>
            </PortableContent>
        );
    }
}


const mapStateToProps = createStructuredSelector({
    playersToChooseRoom: selectPlayersToChooseRoom(),
});

export function mapDispatchToProps(dispatch: Function) {
    return {
        onFindStats: (nickname?: string, pokersite_id?: string) => dispatch(findStats(nickname, pokersite_id)),
        onChangeNicknameToFind: (nickname: string, pokersite_id: number, ignoreSuggests: boolean) =>
            dispatch(changeNicknameToFind(nickname, pokersite_id, ignoreSuggests)),
    };
}


const withConnect = connect<{}, {}, FullScreenChooseRoomProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(PortableChooseRoomPage);
