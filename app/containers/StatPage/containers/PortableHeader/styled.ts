import styled from "styled-components";
import {Link} from "react-router-dom";

export const StyledContainer = styled.section`
    width: 100%;
    max-height: 34px;
    background-color: ${props => props.theme.headerBg};
    display: flex;
    padding: 6px 0 5px 10px;
    flex: 1 0 auto;
`;

export const StyledLogoLink = styled(Link)`
    display: inline-block;
`;

export const StyledLogo = styled.img`
    max-height: 23px;
    max-width: 23px;
`;

export const StyledSearch = styled.div`
    max-width: 130px;
    width: 100%;
`;

export const StyledUserBar = styled.div`
    display: flex;
    margin-left: auto;
`;
