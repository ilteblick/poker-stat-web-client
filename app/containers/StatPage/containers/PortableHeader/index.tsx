import * as React from "react";
import Autocomplete from "../../../../components/Autocomplete";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { compose } from "redux";
import {
    selectAutocomletePlayers,
    selectLastSearchedNickname,
    selectNicknameToFind,
    selectPokersiteId,
} from "../../selectors";
import {
    changeNicknameToFind,
    findStats, getPlayersToChoose
} from "../../actions";
import HeaderIcon from "../../../App/containers/Header/components/HeaderIcon";
import ZoomApp from "../../../../components/ZoomApp";
import { selectAutopast, selectPortableMode, selectUser } from "../../../App/selectors";
import { setAutopast, setPortableMode } from "../../../App/actions";
import { IUser } from "../../../App/interfaces";

import { StyledContainer, StyledLogo, StyledLogoLink, StyledSearch, StyledUserBar } from "./styled";

const Logo = require("../../../../../static/logo_portable_dark.png");

interface PortableHeaderProps {
    nicknameToFind?: string;
    lastSearchedNickname?: string;
    pokersite_id?: string;
    autocompletePLayers?: any;
    autopast?: boolean;
    portableMode?: boolean;
    onChangeNicknameToFind?: Function;
    onFindStats?: Function;
    onGetPlayersToChoose?: Function;
    onSetAutopast?: Function;
    onSetPortableMode?: Function;
    user?: IUser;
}

class PortableHeader extends React.Component<PortableHeaderProps, {}> {

    onChangeNicknameToFindType = (nickname: string) => {
        this.props.onChangeNicknameToFind(nickname);
    }

    onSelect = (x: any) => {
        this.props.onChangeNicknameToFind(x.label, x.pokersite_id, true);
        this.props.onFindStats(x.label, x.pokersite_id);
    }

    onFindStatsClick = () => {
        const { nicknameToFind, lastSearchedNickname } = this.props;
        if (nicknameToFind === lastSearchedNickname) {
            this.props.onFindStats();
        } else {
            this.props.onGetPlayersToChoose(nicknameToFind);
        }

    }

    onFind = () => {
        this.onFindStatsClick();
    }

    onSetAutopastClick = () => {
        this.props.onSetAutopast(!this.props.autopast);
    }

    onSetPortableModeClick = () => {
        this.props.onSetPortableMode(false);
    }

    render() {
        const {
            nicknameToFind,
            pokersite_id,
            autocompletePLayers,
            autopast,
            user: { subscription_type, is_admin },
        } = this.props;

        return (
            <StyledContainer>
                <StyledLogoLink to="/">
                    <StyledLogo src={Logo} />
                </StyledLogoLink>
                <StyledUserBar>
                    <HeaderIcon
                        customItem={(
                            <ZoomApp type="portable" />
                        )}
                        size="small"
                    />
                    {!(!is_admin && !Number.isInteger(subscription_type)) && (
                        <HeaderIcon
                            customItem={(
                                <StyledSearch>
                                    <Autocomplete
                                        sizeType="small"
                                        value={nicknameToFind}
                                        // pokersite_id={pokersite_id}
                                        suggests={autocompletePLayers}
                                        onChange={this.onChangeNicknameToFindType}
                                        onSelect={this.onSelect}
                                        onFind={this.onFind}>
                                    </Autocomplete>
                                </StyledSearch>
                            )}
                            size="small"
                        />
                    )}
                    <HeaderIcon onClick={this.onSetAutopastClick} label="Autopast" isSwitch active={autopast} revers size="small" />
                    <HeaderIcon icon="full_mode" onClick={this.onSetPortableModeClick} size="small" sizeIcon="small" />
                </StyledUserBar>
            </StyledContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    nicknameToFind: selectNicknameToFind(),
    pokersite_id: selectPokersiteId(),
    autocompletePLayers: selectAutocomletePlayers(),
    lastSearchedNickname: selectLastSearchedNickname(),
    autopast: selectAutopast(),
    portableMode: selectPortableMode(),
    user: selectUser(),
});

export function mapDispatchToProps(dispatch: Function) {
    return {
        onFindStats: (nickname?: string, pokersite_id?: string) => dispatch(findStats(nickname, pokersite_id)),
        onChangeNicknameToFind: (nickname: string, pokersite_id: number, ignoreSuggests: boolean) =>
            dispatch(changeNicknameToFind(nickname, pokersite_id, ignoreSuggests)),
        onGetPlayersToChoose: (nickname: string) => dispatch(getPlayersToChoose(nickname)),
        onSetAutopast: (state: boolean) => dispatch(setAutopast(state)),
        onSetPortableMode: (state: boolean) => dispatch(setPortableMode(state)),
    };
}


const withConnect = connect<{}, {}, PortableHeaderProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(PortableHeader);
