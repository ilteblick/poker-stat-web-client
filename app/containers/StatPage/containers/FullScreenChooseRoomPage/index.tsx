import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { selectPlayersToChooseRoom } from "../../selectors";
import { findStats, changeNicknameToFind } from "../../actions";
import { StyledSpan, StyledContainer, StyledChooseRoomBg } from "./styled";
import { pokerSitePostfix } from "../../utils";

interface FullScreenChooseRoomProps {
    playersToChooseRoom?: any;
    onFindStats?: Function;
    onChangeNicknameToFind?: Function;
}

class FullScreenChooseRoomPage extends React.PureComponent<FullScreenChooseRoomProps, {}> {
    onSelectPlayerToShow = (x: any) => {
        this.props.onChangeNicknameToFind(x.playername, x.pokersite_id, true);
        this.props.onFindStats(x.playername, x.pokersite_id);
    }

    render() {
        const { playersToChooseRoom } = this.props;


        const content = playersToChooseRoom && playersToChooseRoom.length > 0
            ? <div style={{ display: "flex" }}>{playersToChooseRoom.map((x: any) => (
                <div>
                    <StyledSpan onClick={() => this.onSelectPlayerToShow(x)}>
                        {x.playername} ({pokerSitePostfix[x.pokersite_id]})
                    </StyledSpan>
                </div>
            ))}</div> : null;

        return (
            <div style={{ flex: 1 }}>
                <StyledContainer>
                    <div style={{
                        display: "flex", flexDirection: "column", padding: "48px 64px"
                    }}>

                        <span style={{ fontSize: 24, fontWeight: "bold", marginBottom: 32, lineHeight: "24px" }}>
                            Select room
                    </span>
                        {content}
                    </div>
                    <StyledChooseRoomBg />
                </StyledContainer>
            </div>
        );
    }
}


const mapStateToProps = createStructuredSelector({
    playersToChooseRoom: selectPlayersToChooseRoom(),
});

export function mapDispatchToProps(dispatch: Function) {
    return {
        onFindStats: (nickname?: string, pokersite_id?: string) => dispatch(findStats(nickname, pokersite_id)),
        onChangeNicknameToFind: (nickname: string, pokersite_id: number, ignoreSuggests: boolean) =>
            dispatch(changeNicknameToFind(nickname, pokersite_id, ignoreSuggests)),
    };
}


const withConnect = connect<{}, {}, FullScreenChooseRoomProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(FullScreenChooseRoomPage);
