import styled from "styled-components";
import ChooseRoomeBgIcon from "../../../../components/icons/ChooseRoomeBgIcon";
import {StyledThemeProps} from "../../../../utils/styleThemes";
import { ComponentType } from "react";

export const StyledSpan = styled.span`
    font-size: 18px;
    cursor: pointer;

    margin-right: 48px;

    &:hover{
        text-decoration: underline;
    }
`;

export const Img = styled.img`
    @media (max-width: 750px){
        display: none;
    }
`;

export const StyledContainer = styled<StyledThemeProps, "div">("div")`
    display: flex;
    margin: 48px 64px;
    background: ${props => props.theme.contentBg};
    color: ${props => props.theme.color};
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 64px;
    align-items: flex-start;
    border-radius: 4px;
`;
export const StyledChooseRoomBg = styled<Partial<StyledThemeProps>>(ChooseRoomeBgIcon as ComponentType)`
    fill: ${props => props.theme.contentBgColor};
    margin: 12px;
    width: 200px;
    height: 200px;
`;
