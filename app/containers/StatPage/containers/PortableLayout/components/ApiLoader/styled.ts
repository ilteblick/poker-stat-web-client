import styled from "styled-components";
import { Spin } from "antd";
import { StyledThemeProps } from "../../../../../../utils/styleThemes";
import {ComponentType} from "react";

export const StyledSpin = styled<Partial<StyledThemeProps>>(Spin as ComponentType)`
    color: ${props => props.theme.colorMain};
    i {
        background-color: ${props => props.theme.colorMain};
    }
`;

export const StyledContainer = styled.section`
    position: absolute;
    z-index: 1000;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    justify-content: center;
    align-items: center;
    display: flex;
    background-color: ${(props: StyledThemeProps) => props.theme.modalBg};
`;

export const StyledContent = styled.div`
    padding: 10px;
`;
