import * as React from "react";
import {StyledContainer, StyledContent, StyledSpin} from "./styled";

interface ApiLoaderProps {
    request?: boolean;
}

class ApiLoader extends React.PureComponent<ApiLoaderProps, {}> {
    render() {
        const {request} = this.props;
        if (!request) {
            return null;
        }
        return (
            <StyledContainer>
                <StyledContent>
                    <StyledSpin tip="Loading..."  />
                </StyledContent>
            </StyledContainer>
        );
    }
}

export default ApiLoader;
