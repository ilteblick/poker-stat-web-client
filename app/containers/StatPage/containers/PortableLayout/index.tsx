import * as React from "react";
import { ThemeProvider } from "styled-components";
import { compose } from "redux";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import PortableHeader from "../PortableHeader";
import PortableChooseRoomPage from "../PortableChooseRoomPage";
import PortableStats from "../../containers/PortableStats";
import ApiLoader from "./components/ApiLoader";
import { themesDark } from "../../../../utils/styleThemes";
import { selectApiCallSpinner, selectUser } from "../../../App/selectors";
import { IUser } from "../../../App/interfaces";

import { StyledContainer, StyledNotPermission } from "./styled";

export interface PortableLayoutProps {
    needToShowRoom: boolean;
    apiCallSpinner?: boolean;
    user?: IUser;
}

class PortableLayout extends React.Component<PortableLayoutProps, {}> {

    render() {
        const { subscription_type, is_admin } = this.props.user;

        return (
            <ThemeProvider theme={themesDark}>
                <StyledContainer>
                    <ApiLoader request={this.props.apiCallSpinner} />
                    <PortableHeader />
                    {(!is_admin && !Number.isInteger(subscription_type))
                        ? <StyledNotPermission>Subscribe. Then you can get access this page.</StyledNotPermission>
                        : this.props.needToShowRoom
                            ? <PortableChooseRoomPage />
                            : <PortableStats />
                    }
                </StyledContainer>
            </ThemeProvider>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    apiCallSpinner: selectApiCallSpinner(),
    user: selectUser(),
});

const withConnect = connect<{}, {}, PortableLayoutProps>(mapStateToProps);

export default compose(
    withConnect,
)(PortableLayout);
