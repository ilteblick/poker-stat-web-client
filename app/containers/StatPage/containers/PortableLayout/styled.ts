import styled from "styled-components";
import { StyledThemeProps } from "../../../../utils/styleThemes";

export const StyledContainer = styled<StyledThemeProps, "section">("section")`
    margin: 0 auto auto 0;
    width: 387px;
    height: 182px;
    overflow: hidden;
    background-color: ${props => props.theme.contentBg};
    display: flex;
    flex-direction: column;
    position: relative;
    border-right: 2px solid white;
    border-bottom: 2px solid white;
`;

export const StyledNotPermission = styled<StyledThemeProps, "div">("div")`
  margin: auto;
  color: ${props => props.theme.color};
`;
