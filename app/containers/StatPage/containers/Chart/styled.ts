import styled from "styled-components";
import { StyledThemeProps } from "../../../../utils/styleThemes";

export const StyledContainer = styled<StyledThemeProps, "div">("div")`
    svg text {
        fill: ${props => props.theme.color}!important;
    }
    svg line {
        shape-rendering: crispEdges;
        stroke: ${props => props.theme.chartLineColor};
}
`;
