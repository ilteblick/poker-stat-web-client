import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { Spin } from "antd";
import { createStructuredSelector } from "reselect";
import { ThemeProps } from "../../../../utils/styleThemes";
import { StyledContainer } from "./styled";


const { Line } = require("@nivo/line");

import { selelctGraph, selelctBaseStats, selectGraphSpinner } from "../../selectors";


export interface ChartProps {
    graph?: any;
    theme: ThemeProps;
    baseStats?: any;
    spinner?: boolean;
}

const width = 600;
const height = 200;

class ChartPage extends React.PureComponent<ChartProps, {}> {
    render() {
        const { graph, baseStats, spinner } = this.props;

        if (spinner) {
            return <div style={{ width, height, display: "flex", alignItems: "center", justifyContent: "center" }}><Spin /></div>;
        }

        if (!graph || !baseStats) {
            return null;
        }
        let sum = 0;
        let num = 0;

        let graphSum = 0;
        for (let point of graph) {
            graphSum += point;
        }

        const dif = (+baseStats[3].value - +graphSum) / graph.length;

        const generatedGraph = [{ x: 0, y: 0 }];
        if (graph) {
            for (const point of graph) {
                sum += parseFloat(point);
                sum += dif;
                generatedGraph.push({
                    x: num + 1,
                    y: sum,
                });
                num += 1;
            }
        }

        const graphToView = [{
            id: "graph",
            data: generatedGraph,
        }];

        return (
            graph ? <StyledContainer>
                <Line
                    data={graphToView}
                    margin={{
                        "top": 10,
                        "right": 10,
                        "bottom": 10,
                        "left": 50
                    }}
                    width={width}
                    height={height}
                    minY="auto"
                    axisBottom={{
                        "tickValues": [],
                        "orient": "bottom",
                        "tickSize": 5,
                        "tickPadding": 5,
                        "tickRotation": 0,
                        "legendOffset": 36,
                        "legendPosition": "center"
                    }}
                    axisLeft={{
                        "orient": "left",
                        "tickSize": 5,
                        "tickPadding": 5,
                        "tickRotation": 0,
                        "legendOffset": -40,
                        "legendPosition": "center"
                    }}
                    colors={this.props.theme.colorMain}
                    dotSize={1}
                    isInteractive={false}
                    enableGridX={false}
                />
            </StyledContainer> : null
        );
    }
}

const mapStateToProps = createStructuredSelector({
    graph: selelctGraph(),
    baseStats: selelctBaseStats(),
    spinner: selectGraphSpinner(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
    withConnect,
)(ChartPage);
