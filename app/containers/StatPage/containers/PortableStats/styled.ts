import styled from "styled-components";

export const StyledTopStats = styled.div`
    display: flex;
    flex-wrap: wrap;
    font-size: 11px;
    padding: 6px 6px;
    border-bottom: 1px solid ${props => props.theme.portableStatsSeparator};
`;

export const StyledBottomStats = styled.div`
    display: flex;
    flex-wrap: wrap;
    font-size: 11px;
    padding: 10px 6px;
`;

export const StyledContainer = styled.section`
    flex: 1 0 auto;
    height: 0;
    overflow: auto;
    display: flex;
    flex-direction: column;
    background: ${props => props.theme.portableStatesBg};
    color: ${props => props.theme.portableStatesColor};
`;
