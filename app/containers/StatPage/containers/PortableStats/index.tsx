import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import {
    selelctNeedToShowStats
} from "../../selectors";
import BaseStats from "./BaseStats";
import AdditionStats from "./AdditionStats";
import PortableContent from "../PortableContent";

import {StyledContainer, StyledTopStats, StyledBottomStats} from "./styled";

interface FullScreenStatsProps {
    needToShowStats?: boolean;
}

class FullScreenStatsPage extends React.PureComponent<FullScreenStatsProps, {}> {

    render() {
        const { needToShowStats } = this.props;

        return (
            <PortableContent>
                {needToShowStats
                    ? (
                        <StyledContainer>
                            <StyledTopStats>
                                <BaseStats />
                            </StyledTopStats>
                            <StyledBottomStats>
                                <AdditionStats />
                            </StyledBottomStats>
                        </StyledContainer>
                    ) : null
                }
            </PortableContent>
        );
    }
}


const mapStateToProps = createStructuredSelector({
    needToShowStats: selelctNeedToShowStats(),
});


const withConnect = connect<{}, {}, FullScreenStatsProps>(mapStateToProps);

export default compose(
    withConnect,
)(FullScreenStatsPage);
