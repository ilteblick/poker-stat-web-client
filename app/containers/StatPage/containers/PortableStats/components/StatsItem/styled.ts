import styled from "styled-components";
import {StyledContainerProps, StyledValueProps} from "./types";

export const StyledLabel = styled.span`
    display: flex;
    align-items: center;
`;

export const StyledValue = styled.span`
    color: ${(props: StyledValueProps) => props.statsType === "positive"
        ? props.theme.portableStatsValueColor_asc
        : props.statsType === "negative"
            ? props.theme.portableStatsValueColor_desc
            : props.statsType === "addition"
                ? props.theme.portableStatsValueColor_addition
                : props.theme.portableStatsValueColor
    };
    margin-left: 2px;
`;

export const StyledContainer = styled.span`
    display: flex;
    align-items: flex-end;
    margin: 0 4px;
    justify-content: ${(props: StyledContainerProps) => props.align === "right" ? "flex-end" : "flex-start"};
    line-height: 1em;
    ${(props: StyledContainerProps) => props.type === "big" ? "font-size: 17px;" : ""}
    ${(props: StyledContainerProps) => props.stretch ? "flex: 1;" : ""}
`;
