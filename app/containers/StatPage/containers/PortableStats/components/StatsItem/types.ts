import {StyledThemeProps} from "../../../../../../utils/styleThemes";

export type StatsType = "positive" | "negative" | "addition";
export type BasicStatsType = "positive" | "negative" | "type1" | "type2" | "type3";
export type Type = "big" | "small";
export type AlignType = "left" | "right";

export interface StatsItemProps {
    type?: Type;
    label: string;
    value: any;
    statsType?: StatsType;
    align?: AlignType;
    stretch?: boolean;
}

export interface StyledValueProps extends StyledThemeProps {
    statsType?: StatsType;
}

export interface StyledContainerProps extends StyledThemeProps {
    type: Type;
    align?: AlignType;
    stretch?: boolean;
}
