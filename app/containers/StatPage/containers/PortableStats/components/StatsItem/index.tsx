import * as React from "react";
import {StatsItemProps} from "./types";

import {StyledContainer, StyledLabel, StyledValue} from "./styled";

import HomeIcon from "../HomeIcon";

const icons: any = {
    HomeIcon: <HomeIcon />
};

class StatsItem extends React.PureComponent<StatsItemProps, {}> {

    static defaultProps: Partial<StatsItemProps> = {
        type: "big",
        align: "right",
    }
    render() {
        const {
            label,
            value,
            stretch,
            statsType,
            type,
            align,
        } = this.props;
        return (
            <StyledContainer stretch={stretch} type={type} align={align}>
                <StyledLabel>
                    {label[0] === "#" ? icons[label.replace("#", "")] : label}
                </StyledLabel>
                <StyledValue statsType={statsType}>
                    {value}
                </StyledValue>
            </StyledContainer>
        );
    }
}

export default StatsItem;
