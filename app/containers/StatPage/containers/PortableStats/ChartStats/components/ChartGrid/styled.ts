import styled from "styled-components";

export const StyledIcon = styled.svg`
    fill-rule: evenodd;
    fill-opacity: .3;
    fill: ${props => props.theme.color};
    position: absolute;
    z-index: 0;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
`;
