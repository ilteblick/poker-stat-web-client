import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
const { Line } = require("@nivo/line");
import ChartGrid from "./components/ChartGrid";
import {ThemeProps} from "../../../../../utils/styleThemes";
import { selelctGraph, selelctBaseStats } from "../../../selectors";

import { StyledGrid } from "./styled";


export interface ChartProps {
    graph?: any;
    theme: ThemeProps;
    baseStats?: any;
}

class ChartPage extends React.PureComponent<ChartProps, {}> {
    render() {
        const { graph, baseStats } = this.props;

        if (!graph || !baseStats) {
            return null;
        }
        let sum = 0;
        let num = 0;

        let graphSum = 0;
        for (let point of graph) {
            graphSum += point;
        }

        const dif = (+baseStats[3].value - +graphSum) / graph.length;

        const generatedGraph = [];
        if (graph) {
            for (const point of graph) {
                sum += parseFloat(point);
                sum += dif;
                generatedGraph.push({
                    x: num,
                    y: sum,
                });
                num += 1;
            }
        }

        const graphToView = [{
            id: "graph",
            data: generatedGraph,
        }];

        if (graph) {
            return (
                <StyledGrid width={125} height={60}>
                    <Line
                        data={graphToView}
                        width={125}
                        height={60}
                        minY="auto"
                        axisBottom={{
                            "tickValues": [],
                            "orient": "bottom",
                            "tickSize": 5,
                            "tickPadding": 5,
                            "tickRotation": 0,
                            "legendOffset": 36,
                            "legendPosition": "center"
                        }}
                        axisLeft={{
                            "orient": "left",
                            "tickSize": 5,
                            "tickPadding": 5,
                            "tickRotation": 0,
                            "legendOffset": -40,
                            "legendPosition": "center"
                        }}
                        colors={this.props.theme.chartColor}
                        dotSize={1}
                        isInteractive={false}
                        enableGridX={false}
                        enableGridY={false}
                    />
                    <ChartGrid />
                </StyledGrid>
            );
        }
        return null;
    }
}

const mapStateToProps = createStructuredSelector({
    graph: selelctGraph(),
    baseStats: selelctBaseStats(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
    withConnect,
)(ChartPage);
