import styled from "styled-components";

export const StyledGrid = styled.div`
    position: relative;
    overflow: hidden;
    margin: 0 4px auto auto;
    height: ${props => props.height}px;
    width: ${props => props.width}px;
`;
