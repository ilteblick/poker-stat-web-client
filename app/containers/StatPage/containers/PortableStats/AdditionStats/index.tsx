import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { selectPostflopResults, selectAgrStats } from "../../../selectors";
import { selelctBaseStatePortable } from "../../../selectors";
import StatsItem from "../components/StatsItem";
import ChartStats from "../ChartStats";

import { StyledTable, StyledTr, StyledTd, StyledContainer, StyledContainerLeft, StyledContainerRight, StyledBaseStat } from "./styled";
import { ThemeConsumer } from "styled-components";

export interface MonthTableProps {
    postflopResults?: any;
    baseStatePortable?: any;
}

class MonthTable extends React.PureComponent<MonthTableProps, {}> {
    render() {
        const { postflopResults, baseStatePortable, agrStats } = this.props;

        return (
            <StyledContainer>
                <StyledContainerLeft>
                    <StyledBaseStat>
                        {baseStatePortable["3bet"] && (
                            <StatsItem
                                label={baseStatePortable["3bet"].labelShortcut}
                                value={baseStatePortable["3bet"].value}
                                statsType={baseStatePortable["3bet"].statsType}
                                type="small"
                            />
                        )}
                        {baseStatePortable["f3bet"] && (
                            <StatsItem
                                label={baseStatePortable["f3bet"].labelShortcut}
                                value={baseStatePortable["f3bet"].value}
                                statsType={baseStatePortable["f3bet"].statsType}
                                type="small"
                            />
                        )}
                        {baseStatePortable["4bet"] && (
                            <StatsItem
                                label={baseStatePortable["4bet"].labelShortcut}
                                value={baseStatePortable["4bet"].value}
                                statsType={baseStatePortable["4bet"].statsType}
                                type="small"
                            />
                        )}
                        {baseStatePortable["f4bet"] && (
                            <StatsItem
                                label={baseStatePortable["f4bet"].labelShortcut}
                                value={baseStatePortable["f4bet"].value}
                                statsType={baseStatePortable["f4bet"].statsType}
                                type="small"
                            />
                        )}
                    </StyledBaseStat>
                    <StyledTable>
                        <tbody>
                            <StyledTr>
                                <StyledTd>
                                    <StatsItem
                                        label="CBf"
                                        value={postflopResults.cbf}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="FvCBf"
                                        value={postflopResults.fvcbf}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="RcbF"
                                        value={postflopResults.rcbf}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="AgF"
                                        value={agrStats ? agrStats.flopagrfactor : "-"}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                            </StyledTr>
                            <StyledTr>
                                <StyledTd>
                                    <StatsItem
                                        label="CBt"
                                        value={postflopResults.cbt}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="FvCBt"
                                        value={postflopResults.fvcbt}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="RcbT"
                                        value={postflopResults.rcbt}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="AgT"
                                        value={agrStats ? agrStats.turnagrfactor : "-"}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                            </StyledTr>
                            <StyledTr>
                                <StyledTd>
                                    <StatsItem
                                        label="Cbr"
                                        value={postflopResults.cbr}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="FvCBr"
                                        value={postflopResults.fvcbr}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="RcbR"
                                        value={postflopResults.rcbr}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                                <StyledTd>
                                    <StatsItem
                                        label="AgR"
                                        value={agrStats ? agrStats.riveragrfactor : "-"}
                                        statsType="addition"
                                        type="small"
                                        align="left"
                                    />
                                </StyledTd>
                            </StyledTr>
                        </tbody>
                    </StyledTable>
                </StyledContainerLeft>
                <StyledContainerRight>
                    <ThemeConsumer>{theme => <ChartStats theme={theme}/>}</ThemeConsumer>
                </StyledContainerRight>
            </StyledContainer>
        );
    }

}

const mapStateToProps = createStructuredSelector({
    postflopResults: selectPostflopResults(),
    baseStatePortable: selelctBaseStatePortable(),
    agrStats: selectAgrStats(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
    withConnect,
)(MonthTable);
