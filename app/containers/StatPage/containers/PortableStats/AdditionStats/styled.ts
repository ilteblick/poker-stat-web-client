import styled from "styled-components";

export const StyledTable = styled.table`
    width: 100%;
    margin-top: 10px;
`;

export const StyledTr = styled.tr`
`;

export const StyledTd = styled.td`
    height: 14px;
`;

export const StyledBaseStat = styled.section`
    display: flex;
    width: 100%;
`;

export const StyledContainerLeft = styled.section`
    display: flex;
    flex-direction: column;
    width: 60%;
`;

export const StyledContainerRight = styled.section`
    display: flex;
    flex-direction: column;
    width: 40%;
`;

export const StyledContainer = styled.section`
    display: flex;
    width: 100%;
`;
