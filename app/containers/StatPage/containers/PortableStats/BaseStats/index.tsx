import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import {BaseStatsProps} from "./types";
import { selelctBaseStatePortable } from "../../../selectors";
import StatsItem from "../components/StatsItem";

import { StyledNickname, StyledContainer, StyledNewLine } from "./styled";

class BaseStats extends React.PureComponent<BaseStatsProps, {}> {

    render() {
        const baseStats = this.props.baseStatePortable;

        if (!baseStats) {
            return null;
        }
        return (
            <StyledContainer>
                <StyledNickname>
                    {baseStats.playerName && baseStats.playerName.value}
                </StyledNickname>
                {baseStats["vpip"] && (
                    <StatsItem
                        label={baseStats["vpip"].labelShortcut}
                        value={baseStats["vpip"].value}
                        statsType={baseStats["vpip"].statsType}
                    />
                )}
                {baseStats["pfr"] && (
                    <StatsItem
                        label={baseStats["pfr"].labelShortcut}
                        value={baseStats["pfr"].value}
                        statsType={baseStats["pfr"].statsType}
                    />
                )}
                {baseStats["wwsf"] && (
                    <StatsItem
                        label={baseStats["wwsf"].labelShortcut}
                        value={baseStats["wwsf"].value}
                        statsType={baseStats["wwsf"].statsType}
                        type="small"
                    />
                )}
                {baseStats["hands"] && (
                    <StatsItem
                        label={baseStats["hands"].labelShortcut}
                        value={baseStats["hands"].value}
                        statsType={baseStats["hands"].statsType}
                        type="small"
                        stretch
                    />
                )}
                <StyledNewLine/>
                {baseStats["winnings"] && (
                    <StatsItem
                        label={baseStats["winnings"].labelShortcut}
                        value={baseStats["winnings"].value}
                        statsType={baseStats["winnings"].statsType}
                        type="small"
                    />
                )}
                {baseStats["winrate"] && (
                    <StatsItem
                        label={baseStats["winrate"].labelShortcut}
                        value={baseStats["winrate"].value}
                        statsType={baseStats["winrate"].statsType}
                        type="small"
                    />
                )}
                {baseStats["wtsd"] && (
                    <StatsItem
                        label={baseStats["wtsd"].labelShortcut}
                        value={baseStats["wtsd"].value}
                        statsType={baseStats["wtsd"].statsType}
                        type="small"
                    />
                )}
                {baseStats["w$sd"] && (
                    <StatsItem
                        label={baseStats["w$sd"].labelShortcut}
                        value={baseStats["w$sd"].value}
                        statsType={baseStats["w$sd"].statsType}
                        type="small"
                    />
                )}
                {baseStats["hl"] && (
                    <StatsItem
                        label={baseStats["hl"].labelShortcut}
                        value={baseStats["hl"].value}
                        statsType={baseStats["hl"].statsType}
                        type="small"
                        stretch
                    />
                )}
            </StyledContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    baseStatePortable: selelctBaseStatePortable(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
    withConnect,
)(BaseStats);
