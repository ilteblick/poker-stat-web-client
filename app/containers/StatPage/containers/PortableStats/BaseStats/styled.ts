import styled from "styled-components";

export const StyledNickname = styled.span`
    font-size: 17px;
    line-height: 1em;
    margin: 0 4px;
`;

export const StyledNewLine = styled.div`
    width: 100%;
    height: 1px;
`;

export const StyledContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    > span {
        height: 16px;
    }
`;
