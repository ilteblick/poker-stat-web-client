import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { ThemeConsumer } from "styled-components";
import * as ReactSwipe from "react-swipe";

import { createStructuredSelector } from "reselect";
import {
  selectNicknameToFind,
  selelctNeedToShowStats,
  selectPokersiteId,
  selectAutocomletePlayers,
  selectLastSearchedNickname,
  selectLimitToFind,
  selectDateToFind,
  selectPokerGameTypeToFind,
  selectTableSizeToFind
} from "../../selectors";
import {
  LimitSelect,
  PokerGameTypeSelect,
  TableSizeSelect,
  DateSelect
} from "../../constants";
import Select from "../../../../components/Select";
import Autocomplete from "../../../../components/Autocomplete";
import { selectUser } from "../../../App/selectors";
import BaseStats from "../BaseStats";
import Chart from "../Chart";
import MonthTable from "../MonthTable";
import { StyledContainer } from "../../styled";
import { StyledContent, StyledContentChart, StyledContentBase } from "./styled";
import { IUser } from "../../../App/interfaces";
import {
  findStats,
  changeNicknameToFind,
  changePokerGameTypeToFind,
  changeLimitToFind,
  changeTableSizeToFind,
  changeDateToFind,
  getPlayersToChoose
} from "../../actions";

const { isMobile } = require("react-device-detect");

const banner1 = require("./3.jpg");
const banner2 = require("./4.png");

interface FullScreenStatsProps {
  user?: IUser;
  needToShowStats?: boolean;
  nicknameToFind?: string;
  pokersite_id?: string;
  autocompletePLayers?: any;
  lastSearchedNickname?: string;

  limitToFind: string;
  tableSizeToFind: string;
  dateToFind: string;
  pokerGameTypeToFind: string;

  onChangeNicknameToFind?: Function;
  onChangePokerGameTypeToFind?: Function;
  onChangeLimitToFind?: Function;
  onChangeTableSizeToFind?: Function;
  onChangeDateToFind?: Function;

  onFindStats?: Function;
  onGetPlayersToChoose?: Function;
}

class Caruousel extends React.PureComponent {
  render() {
    let href = "http://kingshands.com/";
    let image = banner1;
    // if(Math.random() >= 0.5){
    //     href = "http://kingshands.com/";
    //     image=banner1;
    // }else{
    //     href = "https://t.me/Fyodor_n";
    //     image=banner2;
    // }

    return (
      <a href={href} target="_blank">
        <div
          style={{
            margin: 12,
            marginBottom: 0,
            marginTop: 24,
            background: "black"
          }}
        >
          <img
            src={image}
            style={{ width: "100%", height: 70, objectFit: "contain" }}
          />
        </div>
      </a>
    );

    // return (
    //     <ReactSwipe
    //             className="carousel"
    //             swipeOptions={{ auto: 10000, speed: 500 }}>
    //             <div key={1}>
    //                 <a href="http://kingshands.com/" target="_blank">
    //                     <div style={{margin: 12, marginBottom: 0, marginTop: 24, background: 'black'}}>
    //                     <img src={banner1} style={{width: "100%", height: 70, objectFit: 'contain'}}/>
    //                     </div>
    //                 </a>
    //             </div>
    //             <div key={2}>
    //                 <a href="https://t.me/Fyodor_n" target="_blank">
    //                     <div style={{margin: 12, marginBottom: 0, marginTop: 24, background: 'black'}}>
    //                     <img src={banner2} style={{width: "100%", height: 70, objectFit: 'contain'}}/>
    //                     </div>
    //                 </a>
    //             </div>
    //         </ReactSwipe>
    // )
  }
}

class FullScreenStatsPage extends React.PureComponent<
  FullScreenStatsProps,
  {}
> {
  componentDidMount() {
    window.addEventListener("keydown", this.enterClick);
    // this.readFromClipboard();
  }

  componentWillUnmount() {
    window.removeEventListener("keydown", this.enterClick);
  }

  readFromClipboard = () => {
    const {} = this.props;
    navigator.permissions.query({ name: "clipboard-read" }).then(result => {
      if (result.state === "granted" || result.state === "prompt") {
        setTimeout(
          function read() {
            navigator.clipboard
              .readText()
              .then(text => {
                // console.log(text);
                // alert(document.getElementsByClassName("autocomplete")[0]);
                // document.getElementsByClassName("autocomplete")[0].value = text;
                if (text === "") {
                  setTimeout(read.bind(this), 100);
                } else {
                  this.onChangeNicknameToFindType(text, true);
                  this.onFindStatsClick();

                  navigator.clipboard
                    .writeText("")
                    .then(() => {
                      setTimeout(read.bind(this), 100);
                    })
                    .catch(err => {
                      console.log("ddddSomething went wrong", err);
                      setTimeout(read.bind(this), 100);
                    });
                }
              })
              .catch(err => {
                console.log("Something went wrong", err);
                setTimeout(read.bind(this), 100);
              });
          }.bind(this),
          100
        );
      }
    });
  };

  enterClick = (e: KeyboardEvent) => {
    if (e.keyCode === 13) {
      this.onFindStatsClick();
    }
  };

  onFindStatsClick = () => {
    const { nicknameToFind, lastSearchedNickname } = this.props;
    if (nicknameToFind) {
      this.props.onGetPlayersToChoose(nicknameToFind);
    }
    // if (nicknameToFind === lastSearchedNickname) {
    //     this.props.onFindStats();
    // } else {
    //     this.props.onGetPlayersToChoose(nicknameToFind);
    // }
  };

  onChangeNicknameToFindType = (nickname: string, ignoreSuggests = false) => {
    this.props.onChangeNicknameToFind(nickname, undefined, ignoreSuggests);
  };

  onChangePokerGameTypeToFindType = (x: any) => {
    this.props.onChangePokerGameTypeToFind(x.value);
    this.props.onFindStats();
  };

  onChangeLimitToFindType = (x: any) => {
    this.props.onChangeLimitToFind(x.value);
    this.props.onFindStats();
  };

  onChangeTableSizeToFindType = (x: any) => {
    this.props.onChangeTableSizeToFind(x.value);
    this.props.onFindStats();
  };

  onChangeDateToFindType = (x: any) => {
    this.props.onChangeDateToFind(x.value);
    this.props.onFindStats();
  };

  onSelect = (x: any) => {
    this.props.onChangeNicknameToFind(x.label, x.pokersite_id, true);
    this.props.onFindStats(x.label, x.pokersite_id);
  };

  onFind = () => {
    this.onFindStatsClick();
  };

  render() {
    const {
      user,
      nicknameToFind,
      pokersite_id,
      autocompletePLayers,
      needToShowStats,
      limitToFind,
      tableSizeToFind,
      dateToFind,
      pokerGameTypeToFind
    } = this.props;

    const { subscription_type, is_admin } = user;
    const filteredLimitsToFind = LimitSelect.filter(
      x => is_admin || subscription_type >= x.neededSubscriptionType
    );

    const pokerGameTypeSelectContent = (
      <Select
        defaultValue={PokerGameTypeSelect[0]}
        onChange={this.onChangePokerGameTypeToFindType}
        options={PokerGameTypeSelect}
        value={pokerGameTypeToFind || PokerGameTypeSelect[0].label}
      />
    );

    const limitSelectContent = (
      <Select
        defaultValue={LimitSelect[0]}
        onChange={this.onChangeLimitToFindType}
        options={filteredLimitsToFind}
        value={limitToFind || LimitSelect[0].label}
      />
    );

    const tableSizeSelectContent = (
      <Select
        defaultValue={TableSizeSelect[0]}
        onChange={this.onChangeTableSizeToFindType}
        options={TableSizeSelect}
        value={tableSizeToFind || TableSizeSelect[0].label}
      />
    );

    const dateSelectContent = (
      <Select
        defaultValue={DateSelect[0]}
        onChange={this.onChangeDateToFindType}
        options={DateSelect}
        value={dateToFind || DateSelect[0].label}
      />
    );

    return (
      <StyledContainer style={{ maxWidth: 1100, minWidth: 1100, flex: 1 }}>
        {/* <Caruousel /> */}
        <div
          style={{
            width: "100%",
            display: "flex",
            padding: "12px",
            paddingTop: 24
          }}
        >
          <StyledContent>
            <Autocomplete
              className={"autocomplete"}
              value={nicknameToFind}
              // pokersite_id={pokersite_id}
              onChange={this.onChangeNicknameToFindType}
              onSelect={this.onSelect}
              suggests={autocompletePLayers}
              onFind={this.onFind}
            ></Autocomplete>
            {pokerGameTypeSelectContent}
            {limitSelectContent}
            {tableSizeSelectContent}
            {dateSelectContent}
          </StyledContent>
        </div>

        {needToShowStats ? (
          <div style={{ width: "100%", display: "flex" }}>
            <div style={{ width: "400px", padding: 12, minWidth: "400px" }}>
              <StyledContentBase>
                <BaseStats />
              </StyledContentBase>
            </div>
            <div style={{ flex: 1, padding: 12 }}>
              <StyledContentChart>
                {!isMobile ? (
                  <ThemeConsumer>
                    {theme => <Chart theme={theme} />}
                  </ThemeConsumer>
                ) : null}
                <MonthTable />
              </StyledContentChart>
            </div>
          </div>
        ) : null}
      </StyledContainer>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  nicknameToFind: selectNicknameToFind(),
  pokersite_id: selectPokersiteId(),
  needToShowStats: selelctNeedToShowStats(),
  user: selectUser(),
  autocompletePLayers: selectAutocomletePlayers(),
  lastSearchedNickname: selectLastSearchedNickname(),
  limitToFind: selectLimitToFind(),
  tableSizeToFind: selectTableSizeToFind(),
  dateToFind: selectDateToFind(),
  pokerGameTypeToFind: selectPokerGameTypeToFind()
});

export function mapDispatchToProps(dispatch: Function) {
  return {
    onFindStats: (nickname?: string, pokersite_id?: string) =>
      dispatch(findStats(nickname, pokersite_id)),
    onChangeNicknameToFind: (
      nickname: string,
      pokersite_id: number,
      ignoreSuggests: boolean
    ) => dispatch(changeNicknameToFind(nickname, pokersite_id, ignoreSuggests)),
    onChangePokerGameTypeToFind: (pokerGameType: string) =>
      dispatch(changePokerGameTypeToFind(pokerGameType)),
    onChangeLimitToFind: (limit: string) => dispatch(changeLimitToFind(limit)),
    onChangeTableSizeToFind: (tableSize: string) =>
      dispatch(changeTableSizeToFind(tableSize)),
    onChangeDateToFind: (date: string) => dispatch(changeDateToFind(date)),
    // onShowPage: (key: string) => dispatch(showPage(key)),
    onGetPlayersToChoose: (nickname: string) =>
      dispatch(getPlayersToChoose(nickname))
  };
}

const withConnect = connect<{}, {}, FullScreenStatsProps>(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(withConnect)(FullScreenStatsPage);
