import styled from "styled-components";
import {StyledThemeProps} from "../../../../utils/styleThemes";

export const StyledContent = styled<StyledThemeProps, "div">("div")`
    display: flex;
    box-shadow: rgba(0, 0, 0, 0.1) 0 0 64px;
    border-radius: 4px;
    padding: 0 24px 24px;
    align-items: center;
    background: ${props => props.theme.contentBg};
    color: ${props => props.theme.color};
`;

export const StyledContentChart = styled<StyledThemeProps, "div">("div")`
    display: flex;
    box-shadow: rgba(0, 0, 0, 0.1) 0 0 64px;
    padding: 24px;
    align-items: center;
    background: ${props => props.theme.contentBg};
    color: ${props => props.theme.color};
    width: 100%;
    flex-direction: column;
`;

export const StyledContentBase = styled<StyledThemeProps, "div">("div")`
    width: 100%;
    display: flex;
    box-shadow: rgba(0, 0, 0, 0.1) 0 0 64px;
    border-radius: 4px;
    padding: 24px;
    background: ${props => props.theme.contentBg};
    color: ${props => props.theme.color};
`;
