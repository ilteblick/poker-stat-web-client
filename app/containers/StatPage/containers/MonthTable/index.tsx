import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { selectMonthResults, selectPostflopResults, selectAgrStats } from "../../selectors";

import { StyledTable, StyledTh, StyledThead, StyledTr, StyledTd, StyledTbody, StyledTheadTr, StyledRate } from "../../../../components/Table/styled";

export interface MonthTableProps { monthResults?: any; postflopResults?: any; agrStats?: any; }

class MonthTable extends React.PureComponent<MonthTableProps, {}> {
    render() {
        const { postflopResults, agrStats } = this.props;
        return (
            <div style={{ width: "100%" }}>
                <div style={{ width: "100%", marginBottom: 24 }}>
                    <div style={{ display: "flex", alignItems: "center", width: "100%", margin: "18px 0" }}>
                        <span style={{ fontSize: 14 }}>POSTFLOP</span>
                        <div style={{ height: 1, flex: 1, background: "#ebebeb", marginLeft: 12 }} />
                    </div>
                    <StyledTable>
                        <thead>
                            <StyledTheadTr>
                                <StyledTh>CBf</StyledTh>
                                <StyledTh>CBt</StyledTh>
                                <StyledTh>CBr</StyledTh>
                                <StyledTh>
                                    <div style={{ width: "100%", color: "white", justifyContent: "center" }}>
                                        <div style={{ width: 1, height: 20, background: "white" }} />
                                    </div>
                                </StyledTh>
                                <StyledTh>FvCBf</StyledTh>
                                <StyledTh>FvCBt</StyledTh>
                                <StyledTh>FvCBr</StyledTh>
                                <StyledTh>
                                    <div style={{ width: "100%", color: "white", justifyContent: "center" }}>
                                        <div style={{ width: 1, height: 20, background: "white" }} />
                                    </div>
                                </StyledTh>
                                <StyledTh>AgF%</StyledTh>
                                <StyledTh>AgT%</StyledTh>
                                <StyledTh>AgR%</StyledTh>
                                <StyledTh>
                                    <div style={{ width: "100%", color: "white", justifyContent: "center" }}>
                                        <div style={{ width: 1, height: 20, background: "white" }} />
                                    </div>
                                </StyledTh>
                                <StyledTh>RcbF</StyledTh>
                                <StyledTh>RcbT</StyledTh>
                                <StyledTh>RcbR</StyledTh>
                            </StyledTheadTr>
                        </thead>
                        <tbody>
                            <tr>
                                <StyledTd>{Math.round(+postflopResults.cbf) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+postflopResults.cbt) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+postflopResults.cbr) || "-"}</StyledTd>
                                <td />
                                <StyledTd>{Math.round(+postflopResults.fvcbf) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+postflopResults.fvcbt) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+postflopResults.fvcbr) || "-"}</StyledTd>
                                <td />
                                <StyledTd>{agrStats ? Math.round(+agrStats.flopagrfactor) : "-"}</StyledTd>
                                <StyledTd>{agrStats ? Math.round(+agrStats.turnagrfactor) : "-"}</StyledTd>
                                <StyledTd>{agrStats ? Math.round(+agrStats.riveragrfactor) : "-"}</StyledTd>
                                <td />
                                <StyledTd>{Math.round(+postflopResults.rcbf) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+postflopResults.rcbt) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+postflopResults.rcbr) || "-"}</StyledTd>
                            </tr>
                        </tbody>
                    </StyledTable>
                </div>
                {agrStats ? <div style={{ width: "100%", marginBottom: 24 }}>
                    <StyledTable>
                        <StyledThead>
                            <tr>
                                <StyledTh>BetF</StyledTh>
                                <StyledTh>BetT</StyledTh>
                                <StyledTh>BetR</StyledTh>
                                <StyledTh>
                                    <div style={{ width: "100%", color: "white", justifyContent: "center" }}>
                                        <div style={{ width: 1, height: 20, background: "white" }} />
                                    </div>
                                </StyledTh>
                                <StyledTh>FvBetF</StyledTh>
                                <StyledTh>FvBetT</StyledTh>
                                <StyledTh>FvBetR</StyledTh>
                                <StyledTh>
                                    <div style={{ width: "100%", color: "white", justifyContent: "center" }}>
                                        <div style={{ width: 1, height: 20, background: "white" }} />
                                    </div>
                                </StyledTh>
                                <StyledTh>CallF</StyledTh>
                                <StyledTh>CallT</StyledTh>
                                <StyledTh>CallR</StyledTh>
                                <StyledTh>
                                    <div style={{ width: "100%", color: "white", justifyContent: "center" }}>
                                        <div style={{ width: 1, height: 20, background: "white" }} />
                                    </div>
                                </StyledTh>
                                <StyledTh>RbF</StyledTh>
                                <StyledTh>RbT</StyledTh>
                                <StyledTh>RbR</StyledTh>
                            </tr>
                        </StyledThead>
                        <tbody>
                            <tr>
                                <StyledTd>{Math.round(+agrStats.flopbet) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.turnbet) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.riverbet) || "-"}</StyledTd>
                                <td />
                                <StyledTd>{Math.round(+agrStats.flopfold) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.turnfold) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.riverfold) || "-"}</StyledTd>
                                <td />
                                <StyledTd>{Math.round(+agrStats.flopcall) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.turncall) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.rivercall) || "-"}</StyledTd>
                                <td />
                                <StyledTd>{Math.round(+agrStats.flopraise) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.turnraise) || "-"}</StyledTd>
                                <StyledTd>{Math.round(+agrStats.riverraise) || "-"}</StyledTd>
                            </tr>
                        </tbody>
                    </StyledTable>
                </div> : null}
                <StyledTable>
                    <StyledThead>
                        <StyledTr>
                            <StyledTh>Month</StyledTh>
                            <StyledTh>Hands</StyledTh>
                            <StyledTh>Winnings</StyledTh>
                            <StyledTh>bb/100</StyledTh>
                        </StyledTr>
                    </StyledThead>
                    <StyledTbody>
                        {this.props.monthResults.map((x: any, index: number) => <StyledTr key={`tr-${index}`}>
                            <StyledTd>{x.month}</StyledTd>
                            <StyledTd>{x.hands}</StyledTd>
                            <StyledTd>
                                <StyledRate positive={x.winnings > 0}>{x.winnings}</StyledRate>
                            </StyledTd>
                            <StyledTd>
                                <StyledRate positive={x.winnings > 0}>{x.bb100}</StyledRate>
                            </StyledTd>
                        </StyledTr>)}
                    </StyledTbody>
                </StyledTable>
            </div>
        );
    }

}

const mapStateToProps = createStructuredSelector({
    monthResults: selectMonthResults(),
    postflopResults: selectPostflopResults(),
    agrStats: selectAgrStats(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
    withConnect,
)(MonthTable);
