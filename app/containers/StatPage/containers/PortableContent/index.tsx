import * as React from "react";

import { StyledContainer } from "./styled";

interface PortableContentProps {
    children?: any;
}

class PortableContent extends React.Component<PortableContentProps, {}> {

    render() {
        const { children } = this.props;

        return (
            <StyledContainer>
                {children}
            </StyledContainer>
        );
    }
}

export default PortableContent;
