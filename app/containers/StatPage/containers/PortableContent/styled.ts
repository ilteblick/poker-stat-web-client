import styled from "styled-components";

export const StyledContainer = styled.section`
    flex: 1 0 auto;
    display: flex;
    flex-direction: column;
    color: ${props => props.theme.color};
`;

