import {
    FIND_STATS,
    FIND_STATS_LOADED_ACTION,
    CHANGE_NICKNAME_TO_FIND,
    CHANGE_POKERGAMETYPE_TO_FIND,
    CHANGE_LIMIT_TO_FIND,
    CHANGE_DATE_TO_FIND,
    CHANGE_TABLE_SIZE_TO_FIND,
    SET_LOADED_AUTOCOMPLETE_PLAYERS,
    GET_PLAYERS_TO_CHOOSE,
    SET_LOADED_PLAYERS_TO_CHOOSE,
    SET_LOADED_AGR_FACTOR_STATS,
    SET_LOADED_BASE_STATS,
    SET_LOADED_GRAPH_STATS,
    SHOW_LOADING_GRAPH_SPINNER,
    SHOW_GRAPH_ERROR,
    DROP_FILTERS
} from "./constants";
import { Action } from "../../basicClasses";


export function findStats(nickname?: string, pokersite_id?: string): Action {
    return {
        type: FIND_STATS,
        payload: {
            nickname,
            pokersite_id,
        }
    };
}

export function findStatsLoadedAction(result: any, nickname: string, pokersite_id: number): Action {
    return {
        type: FIND_STATS_LOADED_ACTION,
        payload: {
            result,
            nickname,
            pokersite_id
        },
    };
}

export function changeNicknameToFind(nickname: string, pokersite_id: number, ignoreSuggests: boolean): Action {
    return {
        type: CHANGE_NICKNAME_TO_FIND,
        payload: {
            nickname,
            ignoreSuggests,
            pokersite_id,
        },
    };
}

export function changePokerGameTypeToFind(pokerGameType: string): Action {
    return {
        type: CHANGE_POKERGAMETYPE_TO_FIND,
        payload: pokerGameType,
    };
}

export function changeLimitToFind(limit: string): Action {
    return {
        type: CHANGE_LIMIT_TO_FIND,
        payload: limit,
    };
}

export function changeTableSizeToFind(tableSize: string): Action {
    return {
        type: CHANGE_TABLE_SIZE_TO_FIND,
        payload: tableSize,
    };
}

export function changeDateToFind(date: string): Action {
    return {
        type: CHANGE_DATE_TO_FIND,
        payload: date,
    };
}

export function setLoadedPlayers(players: any): Action {
    return {
        type: SET_LOADED_AUTOCOMPLETE_PLAYERS,
        payload: players,
    };
}

export function getPlayersToChoose(nickname: string): Action {
    return {
        type: GET_PLAYERS_TO_CHOOSE,
        payload: nickname,
    };
}

export function setLoadedPlayersToChoose(players: any): Action {
    return {
        type: SET_LOADED_PLAYERS_TO_CHOOSE,
        payload: players
    };
}

export function setLoadedArgFactorStats(agrFactor: any) {
    return {
        type: SET_LOADED_AGR_FACTOR_STATS,
        payload: agrFactor,
    };
}

export function setLoadedBaseStats(baseStats: any, monthResults: any, home: any, player: any) {
    return {
        type: SET_LOADED_BASE_STATS,
        payload: {
            baseStats,
            monthResults,
            home,
            player
        },
    };
}

export function setLoadedGraphStats(graphStats: any, player: any) {
    return {
        type: SET_LOADED_GRAPH_STATS,
        payload: { graphStats, player },
    };
}

export function showLoadingGraphSpinner() {
    return {
        type: SHOW_LOADING_GRAPH_SPINNER
    };
}

export function showGraphError() {
    return {
        type: SHOW_GRAPH_ERROR
    };
}

export function dropFilters() {
    return {
        type: DROP_FILTERS
    };
}
