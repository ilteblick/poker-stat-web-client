import styled from "styled-components";
import SupportIcon from "./icons/SupportIcon";
import {StyledThemeProps} from "../../utils/styleThemes";
import { ComponentType } from "react";

export const Img = styled.img`
    @media (max-width: 1250px){
        display: none;
    }
`;


export const StyledContainer = styled.div`
    display: flex;
    align-items: flex-start;
    flex:1;

    @media (max-width: 900px){
        align-self: flex-start;
    }
`;

export const StyledContent = styled<StyledThemeProps, "div">("div")`
    display: flex;
    margin: 48px 64px;
    background: ${props => props.theme.contentBg};
    color: ${props => props.theme.color};
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 64px;
    align-items: flex-start;
    border-radius: 4px;
`;

export const StyledSupportIcon = styled<Partial<StyledThemeProps>>(SupportIcon as ComponentType)`
    margin: 12px;
    opacity: 0.1;
`;
