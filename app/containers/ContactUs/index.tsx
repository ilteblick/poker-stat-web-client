import * as React from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import authorize from "../../utils/authorize";

import {
    StyledSkypeContainer, StyledSkypeImg, StyledSkypeLabel,
} from "../PricePage/styled";
import { CONTACT_US } from "../App/constants";

const SkypeImg = require("../../../static/Skype_1.png");

import {
    StyledContainer,
    StyledSupportIcon,
    StyledContent,
} from "./styled";

class ContactUs extends React.PureComponent {
    render() {
        return (<StyledContainer>
            <StyledContent>
                <div style={{
                    display: "flex", flexDirection: "column", padding: "48px 64px",
                }}>
                    <span style={{ fontSize: 24, fontWeight: "bold", marginBottom: 32, lineHeight: "24px" }}>
                        English support
                    </span>
                    <span style={{ fontSize: 18, marginBottom: 18 }}>
                        live:h4h_supp_eng
                        </span>
                    <StyledSkypeContainer
                        target="_blank"
                        href="skype:live:h4h_supp_eng?chat">
                        <StyledSkypeImg src={SkypeImg} />
                        <StyledSkypeLabel>Contact support</StyledSkypeLabel>
                    </StyledSkypeContainer>
                    <span style={{ fontSize: 18, marginTop: 8 }}>18:00 - 22:00 (GMT)</span>
                </div>
                <div style={{
                    display: "flex", flexDirection: "column", padding: "48px 64px",
                }}>
                    <span style={{ fontSize: 24, fontWeight: "bold", marginBottom: 32, lineHeight: "24px" }}>
                        Русская служба поддержки
                            </span>
                    <span style={{ fontSize: 18, marginBottom: 18 }}>live:h4h_supp_rus</span>
                    <StyledSkypeContainer
                        target="_blank"
                        href="skype:live:h4h_supp_rus?chat">
                        <StyledSkypeImg src={SkypeImg} />
                        <StyledSkypeLabel>Contact support</StyledSkypeLabel>
                    </StyledSkypeContainer>
                    <span style={{ fontSize: 18, marginTop: 8 }}>08:00 - 20:00 (GMT)</span>
                    <span style={{ fontSize: 18, }}>10:00 - 22:00 (MCK)</span>
                </div>

                <StyledSupportIcon />
            </StyledContent>
        </StyledContainer>
        );
    }
}

const withAuth = authorize({ pageKey: CONTACT_US });

export default withRouter(compose(
    withAuth,
)(ContactUs) as any);
