import * as React from "react";

import {StyledIcon} from "./styled";

export default () => (
    <StyledIcon width="25" height="25" viewBox="0 0 25 25">
        <path
            d="M550.5,341A12.5,12.5,0,1,0,563,353.5,12.5,12.5,0,0,0,550.5,341Zm4.32,13.163-6.25,3.906a0.782,0.782,0,0,1-1.2-.663v-7.812a0.782,0.782,0,0,1,1.2-.663l6.25,3.906A0.782,0.782,0,0,1,554.82,354.163Z"
            transform="translate(-538 -341)"
        />
    </StyledIcon>
);
