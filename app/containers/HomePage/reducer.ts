import { fromJS } from "immutable";

import { Action } from "../../basicClasses";

const initialState = fromJS({
    name: "home",
});

function homeReducer(state = initialState, action: Action) {
    switch (action.type) {
        default:
            return state;
    }
}

export default homeReducer;
