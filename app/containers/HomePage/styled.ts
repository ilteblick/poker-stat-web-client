import styled from "styled-components";
import { StyledThemeProps } from "../../utils/styleThemes";
import { StyledInfo2Props } from "./types";
import { Link, LinkProps } from "react-router-dom";



export const StyledSignInConatiner = styled.div`
    padding: 12px 0;
    display:flex;
    align-items:center;
    justify-content: space-between;
`;

interface StyledCreateNewProps extends StyledThemeProps, LinkProps {
    activepage?: string;
}

export const StyledCreateNew = styled<StyledCreateNewProps>(Link)`
    color: ${props => props.theme.colorMain};
    font-size: 20px;
    cursor: pointer;
`;

export const StyledInfo1 = styled<StyledThemeProps, "span">("span")`
    color: ${props => props.theme.colorMain};
    font-size: 18px;
    display: inline-block;
    font-weight: bold;
    margin: 20px 128px;
    text-align: center;
`;

export const StyledInfo2 = styled<StyledInfo2Props, "span">("span")`
    color: ${props => props.primary ? props.theme.colorMain : props.theme.color};
    margin-bottom: 12px;
    display: list-item;
`;

export const StyledImg = styled.img`
    background: #1890ff;
    height: 60px;
    border-bottom-right-radius: 4px;
    border-top-right-radius: 4px;
    border-left: 1px solid rgba(0,0,0,0.1);
    object-fit: scale-down;
    cursor: pointer;

    &:hover {
        background: #40a9ff;
    }
`;

export const StyledPlayVideo = styled<StyledThemeProps, "a">("a")`
    font-size: 18px;
    text-decoration: underline;
    font-weight: bold;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    margin: auto;
    max-width: 800px;
    color: ${props => props.theme.colorMain};
    > svg {
      margin-right: 12px;
    }
    :hover {
        color: ${props => props.theme.color};
        text-decoration: underline;
    }
`;

export const StyledAd = styled<StyledThemeProps, "img">("img").attrs({
    src: props => props.theme.images.ad,
})`
    object-fit: contain;
    flex: 1;
`;

export const StyledAdContainer = styled<StyledThemeProps, "div">("div")`
     padding: 15px 0 25px;
     margin-bottom: 65px;
     text-align: center;
     background-color: ${props => props.theme.contentBg};
`;
