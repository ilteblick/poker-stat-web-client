import * as React from "react";
import { Button } from "antd";
import styled from "styled-components";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { Tag } from "antd";

import reducer from "./reducer";
import saga from "./sagas";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";

import { selectName } from "./selectors";
import { openSignInModal, openRegisterModal, showPage } from "../App/actions";
import { selectUser } from "../App/selectors";
import { IUser } from "../App/interfaces";
import { STAT_PAGE, HOME_PAGE } from "../App/constants";
import { push } from "react-router-redux";

import { StyledTitle, StyledLabel, StyledContainer, CenteredContainer } from "../../components/FormsItems";

import {
    StyledSignInConatiner,
    StyledCreateNew,
    StyledPlayVideo,
    StyledInfo1,
    StyledInfo2,
    StyledAd,
    StyledAdContainer,
} from "./styled";
import Input from "../../components/Input";

import { signIn } from "../App/containers/SignInModal/actions";
import { SignInModel } from "../App/containers/SignInModal/containers/SignInForm/interfaces";

import PlayIcon from "./components/PlayIcon";
const Icon = require("../../../static/Statistics_icon.png");
const Email = require("../../../static/E-mail.png");
const Pass = require("../../../static/Password.png");

import { emailFieldValidator, requiredValidator } from "../../utils/validators";

const Btn = styled(Button)`
    font-size: 20px;
    height: auto;
    padding: 0;
    display: flex;
    align-items: center;

    img {
        padding: 12px 25px;
        border-left: 1px solid rgba(0,0,0,0.1);
    }

    span {
        padding: 0 24px;
    }
`;

export interface HomeProps {
    name: string;
    user: IUser;
    onSingIn?: Function;
    onOpenSignInModal?: Function;
    onOpenRegisterModal?: Function;
    onShowPage?: Function;
    redirect?: Function;
}

interface HomeState {
    login: string;
    password: string;
}



class HomePage extends React.PureComponent<HomeProps, HomeState> {
    constructor(props: HomeProps) {
        super(props);

        this.props.onShowPage(HOME_PAGE);

        this.state = {
            login: "",
            password: "",
        };
    }

    onOpenSignInModalClick = () => {
        this.props.onOpenSignInModal();
    }

    onOpenRegisterModalClick = () => {
        this.props.onOpenRegisterModal();
    }

    onShowStatPageClick = () => {
        this.props.onShowPage(STAT_PAGE);
        this.props.redirect("/statistics");
    }

    onChangeLogin = (login: string) => {
        this.setState({
            login,
        });
    }

    onChangePass = (password: string) => {
        this.setState({
            password,
        });
    }

    onSingInClick = () => {
        const { login, password } = this.state;

        if (emailFieldValidator(login).ok && requiredValidator(password).ok) {
            const model = {
                email: login,
                password,
            };

            this.props.onSingIn(model);
        }
    }

    render() {
        const { user }: { user: IUser } = this.props;
        const { login, password } = this.state;
        return (
            <CenteredContainer>
                {user ?
                    <div style={{
                        display: "flex", flexDirection: "column", alignItems: "center", flex: "0 1 auto",
                        overflow: "auto"
                    }}>

                        <StyledInfo1>
                        Dear customers. After registration you will get a trial period up to 01.01.2021. In case you didn't get a trial period during 24 hours refresh the page (press F5) or relogin.
                        </StyledInfo1>
                        <StyledAdContainer>
                            <StyledAd />
                            <StyledPlayVideo
                                target="_blank"
                                href="https://www.youtube.com/watch?v=m9AN6FbbhE4&feature=youtu.be"
                            >
                                <PlayIcon />Watch the video “How to use portable”
                            </StyledPlayVideo>
                        </StyledAdContainer>

                        <div><Btn
                            style={{ marginBottom: "50px" }}
                            key="go-to-stat-page-btn"
                            size="large"
                            onClick={this.onShowStatPageClick}
                            type="primary">
                            Go to statistics
                        <img src={Icon} />
                        </Btn></div>
                        <div style={{
                            flex: 0.4, margin: 48, padding: "48px 128px", boxShadow: "rgba(0, 0, 0, 0.1) 0px 0px 64px",
                            color: "black", fontWeight: "bold", fontSize: 18
                        }}>
                            <StyledInfo2>{"Chico and 888 have been added (2017 & 2018)."}</StyledInfo2>

                            <StyledInfo2>Please use Google Chrome or Mozilla. We are working hard to become better everyday.
                            Currently hands are updated every 3-7 days. Cap graphics will be fixed soon.</StyledInfo2>
                            <StyledInfo2>We have 10 minutes delay for several login attempts from different PC (browsers). Soon it will be up to 15 minutes or more for different IP address. Sorry for the inconvenience.</StyledInfo2>
                        </div>


                    </div> :
                    <StyledContainer>
                        <div style={{ alignSelf: "center", flex: 0.4 }}>
                            <Tag
                                color="#757575"
                                closable
                                style={{ margin: 12 }}>
                                Dear Customers! Please use Google Chrome or Mozilla. We are working hard to become better everyday.
                    </Tag>
                        </div>
                        <StyledTitle>Sign in</StyledTitle>
                        <StyledLabel>Enter your details below</StyledLabel>
                        <Input
                            label="E-mail"
                            value={login}
                            onChange={this.onChangeLogin}
                            finalValidate={emailFieldValidator}
                            suffix={Email} />
                        <Input
                            label="Password"
                            value={password}
                            onChange={this.onChangePass}
                            isPass
                            finalValidate={requiredValidator}
                            suffix={Pass} />

                        <StyledSignInConatiner>
                            {<StyledCreateNew to="/register">Create new account</StyledCreateNew>}
                            <Button
                                size="large"
                                onClick={this.onSingInClick}
                                type="primary"
                                style={{
                                    width: "240px", height: "60px",
                                    fontSize: "20px", borderRadius: "6px",
                                }}>
                                Log in
                            </Button>
                        </StyledSignInConatiner>
                    </StyledContainer>
                }</CenteredContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    user: selectUser(),
    name: selectName(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onOpenSignInModal: () => dispatch(openSignInModal()),
        onOpenRegisterModal: () => dispatch(openRegisterModal()),
        onShowPage: (key: string) => dispatch(showPage(key)),
        onSingIn: (model: SignInModel) => dispatch(signIn(model)),
        redirect: (url: string) => dispatch(push(url)),
    };
}
const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "home", reducer });
const withSaga = injectSaga({ key: "home", saga });

export default withRouter(compose(
    withReducer,
    withSaga,
    withConnect,
)(HomePage) as any);
