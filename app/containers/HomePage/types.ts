import { StyledThemeProps } from "../../utils/styleThemes";

export interface StyledInfo2Props extends StyledThemeProps {
    primary?: boolean;
}

