import { createSelector } from "reselect";

const selectHome = () => (state: any) => {
    return state.get("home");
};

const selectName = () => createSelector(
    selectHome(),
    (homeState) => homeState.get("name")
);

export {
    selectHome,
    selectName,
};
