import { put, race, take, fork } from "redux-saga/effects";
import { LOAD_USER_INVITES } from "./constants";
import { apiService } from "../../api/actionService/api-service";
import { message } from "antd";
import { setLoadedUserInvites, setLoadedUser } from "./actions";
import {
    SUBSCRIBE_USER, MAKE_ADMIN, GIVE_INVITE_CODES_TO_USER,
    MAKE_SIMPLE_USER, MAKE_UNLIMIT, MAKE_LIMIT
} from "../AdminPage/containers/UserListItem/constants";
import { prepareLoadedUsers, prepareLoadedUser } from "../AdminPage/utils";

const adminPrefix = "admin";
const load_invites_url_segment = `${adminPrefix}/user-info`;
const subscribe_url_segment = `${adminPrefix}/subscribe`;
const make_admin_url_segment = `${adminPrefix}/make_admin`;
const make_simple_user_url_segment = `${adminPrefix}/make_simple_user`;
const give_invite_code_url_segment = `${adminPrefix}/create_invite_code`;
const make_unlimit_url_segment = `${adminPrefix}/make_unlimit`;
const make_limit_url_segment = `${adminPrefix}/make_limit`;

const subscribeLyfecycle = apiService.postLifecycle(subscribe_url_segment);
const makeAdminLyfecycle = apiService.postLifecycle(make_admin_url_segment);
const makeSimpleUserLyfecycle = apiService.postLifecycle(make_simple_user_url_segment);
const giveInviteCodeLyfecycle = apiService.postLifecycle(give_invite_code_url_segment);
const makeLimitLyfecycle = apiService.postLifecycle(make_limit_url_segment);
const makeUnlimitLyfecycle = apiService.postLifecycle(make_unlimit_url_segment);

const getLoadedInvitesLyfecycle = apiService.findLifecycle(load_invites_url_segment);

function* watchLoadUserInvites() {
    while (true) {
        const { payload } = yield take(LOAD_USER_INVITES);

        yield put(apiService.find(load_invites_url_segment, payload));

        const { loadedInvitesSuccessFromApi, usersFindErrorFromApi } = yield race({
            loadedInvitesSuccessFromApi: take(getLoadedInvitesLyfecycle.RESOLVED),
            usersFindErrorFromApi: take(getLoadedInvitesLyfecycle.REJECTED),
        });

        if (usersFindErrorFromApi) {
            message.error(usersFindErrorFromApi.error.message);
        }

        if (loadedInvitesSuccessFromApi) {
            loadedInvitesSuccessFromApi.payload.invites = prepareLoadedUsers(loadedInvitesSuccessFromApi.payload.invites);
            yield put(setLoadedUserInvites(loadedInvitesSuccessFromApi.payload));
        }
    }
}

function* watchSubscribeUser() {
    while (true) {
        const { payload } = yield take(SUBSCRIBE_USER);

        const { id, type, days } = payload;

        const body = {
            id,
            type,
            days
        };

        yield put(apiService.post(subscribe_url_segment, body));

        const { subscribeUserSuccessFromApi, subscribeUserErrorFromApi } = yield race({
            subscribeUserSuccessFromApi: take(subscribeLyfecycle.RESOLVED),
            subscribeUserErrorFromApi: take(subscribeLyfecycle.REJECTED),
        });

        if (subscribeUserErrorFromApi) {
            // console.log(subscribeUserErrorFromApi);
            message.error(subscribeUserErrorFromApi.error.message);
        }

        if (subscribeUserSuccessFromApi) {
            subscribeUserSuccessFromApi.payload = prepareLoadedUser(subscribeUserSuccessFromApi.payload);
            yield put(setLoadedUser(subscribeUserSuccessFromApi.payload));
        }
    }
}

function* watchMakeAdmin() {
    while (true) {
        const { payload } = yield take(MAKE_ADMIN);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_admin_url_segment, body));

        const { makeAdminFromApi, makeAdminErrorFromApi } = yield race({
            makeAdminFromApi: take(makeAdminLyfecycle.RESOLVED),
            makeAdminErrorFromApi: take(makeAdminLyfecycle.REJECTED),
        });

        if (makeAdminErrorFromApi) {
            message.error(makeAdminErrorFromApi.error.message);
        }

        if (makeAdminFromApi) {
            makeAdminFromApi.payload = prepareLoadedUser(makeAdminFromApi.payload);
            yield put(setLoadedUser(makeAdminFromApi.payload));
        }
    }
}

function* watchGiveInviteCodes() {
    while (true) {
        const { payload } = yield take(GIVE_INVITE_CODES_TO_USER);
        const { id, count } = payload;

        if (count > 0) {
            const body = {
                id,
                count
            };

            yield put(apiService.post(give_invite_code_url_segment, body));

            const { giveInviteCodeFromApi, giveInviteCodeErrorFromApi } = yield race({
                giveInviteCodeFromApi: take(giveInviteCodeLyfecycle.RESOLVED),
                giveInviteCodeErrorFromApi: take(giveInviteCodeLyfecycle.REJECTED),
            });

            if (giveInviteCodeErrorFromApi) {
                message.error(giveInviteCodeErrorFromApi.error.message);
            }

            if (giveInviteCodeFromApi) {
                giveInviteCodeFromApi.payload = prepareLoadedUser(giveInviteCodeFromApi.payload);
                yield put(setLoadedUser(giveInviteCodeFromApi.payload));
            }
        }
    }
}

function* watchMakeSimpleUser() {
    while (true) {
        const { payload } = yield take(MAKE_SIMPLE_USER);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_simple_user_url_segment, body));

        const { makeSimpleUserFromApi, makeSimpleUserErrorFromApi } = yield race({
            makeSimpleUserFromApi: take(makeSimpleUserLyfecycle.RESOLVED),
            makeSimpleUserErrorFromApi: take(makeSimpleUserLyfecycle.REJECTED),
        });

        if (makeSimpleUserErrorFromApi) {
            message.error(makeSimpleUserErrorFromApi.error.message);
        }

        if (makeSimpleUserFromApi) {
            makeSimpleUserFromApi.payload = prepareLoadedUser(makeSimpleUserFromApi.payload);
            yield put(setLoadedUser(makeSimpleUserFromApi.payload));
        }
    }
}

function* watchMakeUnlimit() {
    while (true) {
        const { payload } = yield take(MAKE_UNLIMIT);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_unlimit_url_segment, body));

        const { makeAdminFromApi, makeAdminErrorFromApi } = yield race({
            makeAdminFromApi: take(makeUnlimitLyfecycle.RESOLVED),
            makeAdminErrorFromApi: take(makeUnlimitLyfecycle.REJECTED),
        });

        if (makeAdminErrorFromApi) {
            message.error(makeAdminErrorFromApi.error.message);
        }

        if (makeAdminFromApi) {
            makeAdminFromApi.payload = prepareLoadedUser(makeAdminFromApi.payload);
            yield put(setLoadedUser(makeAdminFromApi.payload));
        }
    }
}

function* watchMakeLimit() {
    while (true) {
        const { payload } = yield take(MAKE_LIMIT);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_limit_url_segment, body));

        const { makeAdminFromApi, makeAdminErrorFromApi } = yield race({
            makeAdminFromApi: take(makeLimitLyfecycle.RESOLVED),
            makeAdminErrorFromApi: take(makeLimitLyfecycle.REJECTED),
        });

        if (makeAdminErrorFromApi) {
            message.error(makeAdminErrorFromApi.error.message);
        }

        if (makeAdminFromApi) {
            makeAdminFromApi.payload = prepareLoadedUser(makeAdminFromApi.payload);
            yield put(setLoadedUser(makeAdminFromApi.payload));
        }
    }
}

export default function* adminUserInfoSaga(): IterableIterator<any> {
    yield fork(watchLoadUserInvites);

    yield fork(watchSubscribeUser);
    yield fork(watchMakeAdmin);
    yield fork(watchMakeSimpleUser);
    yield fork(watchGiveInviteCodes);
    yield fork(watchMakeUnlimit);
    yield fork(watchMakeLimit);
}
