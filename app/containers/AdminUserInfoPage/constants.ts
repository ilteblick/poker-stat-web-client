export const LOAD_USER_INVITES = "App/AdminUserInfoPage/LOAD_USER_INVITES";
export const SET_LOADED_USER_INVITES = "App/AdminUserInfoPage/SET_LOADED_USER_INVITES";

export const SET_LOADED_USER = "App/AdminUserInfoPage/SET_LOADED_USER";
