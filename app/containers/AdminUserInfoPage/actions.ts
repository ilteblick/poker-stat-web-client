import { Action } from "../../basicClasses";
import { LOAD_USER_INVITES, SET_LOADED_USER_INVITES, SET_LOADED_USER } from "./constants";

interface LoadedUserInvites {
    email: string;
    invites: Array<any>;
}

export function loadUserInvites(email: string, isShowActive: boolean, isShowAdmins: boolean,
    typeToSearch: boolean | number): Action {
    return {
        type: LOAD_USER_INVITES,
        payload: {
            email,
            isShowActive,
            isShowAdmins,
            typeToSearch
        },
    };
}

export function setLoadedUserInvites(result: LoadedUserInvites): Action {
    return {
        type: SET_LOADED_USER_INVITES,
        payload: result,
    };
}

export function setLoadedUser(user: any): Action {
    return {
        type: SET_LOADED_USER,
        payload: user
    };
}
