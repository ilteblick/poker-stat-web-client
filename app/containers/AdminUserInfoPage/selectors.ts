import { createSelector } from "reselect";

const selectAdmin = () => (state: any) => {
    return state.get("adminUserInfo");
};

const selectInitialLoad = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("initialLoad")
);

const selectInvites = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("invites")
);

const selectEmail = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("email")
);

export {
    selectAdmin,
    selectInitialLoad,
    selectInvites,
    selectEmail
};
