import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";

import { Button, Checkbox } from "antd";

import reducer from "./reducer";
import saga from "./sagas";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";
import admin from "../../utils/admin";
import { selectInitialLoad, selectEmail, selectInvites } from "./selectors";
import { loadUserInvites } from "./actions";

import UserListItem from "../AdminPage/containers/UserListItem";
import Select from "../../components/Select";

interface AdminUserInfoProps {
    initialLoad: boolean;
    match: any;

    onLoadUserInvites: Function;
    email: string;
    invites: Array<any>;
}

interface AdAdminUserInfoState {
    isShowActive: boolean;
    isShowAdmins: boolean;
    typeToSearch: boolean | number;
}

const TypeOptions = [
    {
        label: "All types",
        value: false,
    }, {
        label: "nl10-nl16",
        value: 0,
    }, {
        label: "nl10-nl50",
        value: 1,
    }, {
        label: "nl10-nl10K",
        value: 2,
    }
];

class AdminPage extends React.Component<AdminUserInfoProps, AdAdminUserInfoState> {
    constructor(props: AdminUserInfoProps) {
        super(props);

        this.state = {
            isShowAdmins: false,
            isShowActive: false,
            typeToSearch: false,
        };
    }

    componentWillMount() {
        const { email } = this.props.match.params;
        if (email) {
            this.props.onLoadUserInvites(email);
        }
    }

    onFindClick = () => {
        const { email } = this.props.match.params;
        const { isShowActive, isShowAdmins, typeToSearch } = this.state;
        this.props.onLoadUserInvites(email, isShowActive, isShowAdmins, typeToSearch);
    }

    onChangeActive = () => {
        this.setState((state) => ({ isShowActive: !state.isShowActive }));
    }

    onChangeAdmins = () => {
        this.setState((state) => ({ isShowAdmins: !state.isShowAdmins }));
    }

    onTypeChange = (type: any) => {
        this.setState({
            typeToSearch: type.value,
        });
    }

    // componentWillUpdate() {
    //     const { email } = this.props.match.params;
    //     console.log(this.props.email, email);
    //     if (this.props.email !== email) {
    //         this.props.onLoadUserInvites(email);
    //     }
    // }

    render() {
        const { initialLoad, email, invites } = this.props;
        const { isShowActive, isShowAdmins } = this.state;

        return (
            <div style={{ flex: 1, alignSelf: "flex-start" }}>
                {initialLoad ?
                    <div style={{ display: "flex", flexDirection: "column", background: "white", padding: 24 }}>
                        <div style={{ display: "flex", alignItems: "center", background: "white", padding: 24 }}>
                            <span style={{ fontSize: 18, fontWeight: "bold", margin: 24 }}>Email:{email}</span>
                            <div style={{ marginLeft: 6, display: "flex" }}>
                                <Checkbox
                                    value={isShowActive}
                                    onChange={this.onChangeActive}
                                />
                                <span>Active</span>
                            </div>
                            <div style={{ marginLeft: 6, display: "flex" }}>
                                <Checkbox
                                    value={isShowAdmins}
                                    onChange={this.onChangeAdmins}
                                />
                                <span>Admins</span>
                            </div>
                            <Select
                                options={TypeOptions}
                                defaultValue={TypeOptions[0]}
                                onChange={this.onTypeChange}
                            />
                            <Button onClick={this.onFindClick}>Find</Button>
                        </div>
                        {invites.map((x, index) => <UserListItem user={x} index={index + 1} />)}
                    </div> :
                    null}
            </div >
        );
    }
}

const mapStateToProps = createStructuredSelector({
    initialLoad: selectInitialLoad(),
    email: selectEmail(),
    invites: selectInvites(),
});

function mapDispatchToProps(dispach: any) {
    return {
        // onLoadUsers: (page: number, nickname: string) => dispach(loadUsers(page, nickname)),
        onLoadUserInvites: (email: string, isShowActive: boolean, isShowAdmins: boolean,
            typeToSearch: boolean | number) => dispach(loadUserInvites(email, isShowActive, isShowAdmins, typeToSearch)),
    };
}
const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "adminUserInfo", reducer });
const withSaga = injectSaga({ key: "adminUserInfo", saga });

const withAdmin = admin();

export default withRouter(compose(
    withAdmin,
    withReducer,
    withSaga,
    withConnect,
)(AdminPage) as any);
