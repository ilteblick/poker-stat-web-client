import { fromJS } from "immutable";

import { Action } from "../../basicClasses";
import { SET_LOADED_USER_INVITES, SET_LOADED_USER } from "./constants";

const initialState = fromJS({
    initialLoad: false,

    invites: false,
    email: false,
});

function setLoadedUser(state: any, action: Action) {
    const old = state.get("invites");
    const inv: Array<any> = old.slice();
    const index = inv.findIndex((x: any) => x.email === action.payload.email);
    inv[index] = action.payload;
    return state.set("invites", inv);
}

function adminUserInfoReducer(state = initialState, action: Action) {
    switch (action.type) {
        case SET_LOADED_USER_INVITES:
            return state.set("invites", action.payload.invites)
                .set("email", action.payload.email)
                .set("initialLoad", true);
        case SET_LOADED_USER:
            return setLoadedUser(state, action);
        default:
            return state;
    }
}

export default adminUserInfoReducer;
