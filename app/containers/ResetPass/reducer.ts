import { fromJS } from "immutable";

import { CHANGE_EMAIL } from "./constants";

import { Action } from "../../basicClasses";

const initialState = fromJS({
    email: "",
});

function homeReducer(state = initialState, action: Action) {
    switch (action.type) {
        case CHANGE_EMAIL:
            return state.set("email", action.payload.email);
        default:
            return state;
    }
}

export default homeReducer;
