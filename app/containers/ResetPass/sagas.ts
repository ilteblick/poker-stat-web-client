import { fork, take, put, race } from "redux-saga/effects";
import { apiService } from "../../api/actionService/api-service";

const reset_pass_url_segment = "reset-password";

const resetPassLyfecycle = apiService.postLifecycle(reset_pass_url_segment);

import { RESET_PASSWORD } from "./constants";

function* resetPass() {
    while (true) {
        const { payload: email } = yield take(RESET_PASSWORD);

        yield put(apiService.post(reset_pass_url_segment, email, false));

        const { result } = yield race({
            result: take(resetPassLyfecycle.RESOLVED),
            error: take(resetPassLyfecycle.REJECTED),
        });

    }
}


export default function* resetSaga() {
    yield fork(resetPass);
}
