import { CHANGE_EMAIL, RESET_PASSWORD } from "./constants";

import { Action } from "../../basicClasses";

export function changeEmail(email: string): Action {
    return {
        type: CHANGE_EMAIL,
        payload: {
            email,
        }
    };
}

export function resetPassword(email: string): Action {
    return {
        type: RESET_PASSWORD,
        payload: {
            email,
        }
    };
}
