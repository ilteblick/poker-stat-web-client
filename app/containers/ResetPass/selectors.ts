import { createSelector } from "reselect";

const selectHome = () => (state: any) => {
    return state.get("reset");
};

const selectEmail = () => createSelector(
    selectHome(),
    (homeState) => homeState.get("email")
);

export {
    selectHome,
    selectEmail,
};
