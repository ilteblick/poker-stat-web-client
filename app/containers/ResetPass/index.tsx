import * as React from "react";
import { Button } from "antd";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";

import { changeEmail, resetPassword } from "./actions";
import { selectEmail } from "./selectors";

import reducer from "./reducer";
import saga from "./sagas";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";

import Input from "../../components/Input";

import { StyledTitle, StyledLabel, StyledContainer, CenteredContainer } from "../../components/FormsItems";

import { emailFieldValidator } from "../../utils/validators";

export interface ResetPassProps {
    email: string;
    onChangeEmail?: Function;
    onResetPassword?: Function;
}

class ResetPass extends React.PureComponent<ResetPassProps, {}> {
    onChangeEmailType = (value: string) => {
        this.props.onChangeEmail(value);
    }

    onResetPasswordClick = () => {
        const { onResetPassword, email } = this.props;

        if (emailFieldValidator(email).ok) {
            onResetPassword(email);
        }

    }

    render() {
        const { email } = this.props;
        return (
            <CenteredContainer>
                <StyledContainer>
                    <StyledTitle>Reset password</StyledTitle>
                    <StyledLabel>Enter your email below</StyledLabel>
                    <Input
                        label="E-mail"
                        value={email}
                        onChange={this.onChangeEmailType}
                        finalValidate={emailFieldValidator}
                    />
                    <Button
                        size="large"
                        onClick={this.onResetPasswordClick}
                        type="primary"
                        style={{
                            width: "240px", height: "60px",
                            fontSize: "20px", borderRadius: "6px",
                            marginTop: 12, alignSelf: "flex-end"
                        }}>
                        Reset
                            </Button>
                </StyledContainer>
            </CenteredContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    email: selectEmail(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onChangeEmail: (email: string) => dispatch(changeEmail(email)),
        onResetPassword: (email: string) => dispatch(resetPassword(email)),
    };
}
const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "reset", reducer });
const withSaga = injectSaga({ key: "reset", saga });

export default withRouter(compose(
    withReducer,
    withSaga,
    withConnect,
)(ResetPass) as any);
