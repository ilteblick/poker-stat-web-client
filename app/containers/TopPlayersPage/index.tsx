import * as React from "react";
import { connect, Dispatch } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { Button } from "antd";

import injectReducer from "../../utilsTypes/redux/storeUtils/injectReducer";
import injectSaga from "../../utilsTypes/redux/storeUtils/injectSaga";

import Select from "../../components/Select";

import saga from "./sagas";
import reducer from "./reducer";
import {
    PokerGameTypeSelect,
    LimitSelect,
    TableSizeSelect,
    DateSelect,
    PokerSiteSelect,
} from "../StatPage/constants";

import { TOP_PLAYERS } from "../App/constants";
import { selectUser } from "../App/selectors";
import authorize from "../../utils/authorize";
import { showPage } from "../App/actions";
import { StyledContainer } from "../StatPage/styled";

// import {
//     findTopPlayers
// } from "./actions";

import { Actions } from "./actions";

import { selelctTopPlayers } from "./selectors";
import {
    StyledTd,
    StyledTable,
    StyledThead,
    StyledTh,
    StyledTr,
    StyledA,
    StyledContent,
    StyledTbody,
    StyledRate,
} from "./styled";

import SortableTh from "./components/SortableTh/index";
import { OwnTopPlayersPageProps, Props, TopPlayersPageState, StateProps, DispatchProps } from "./types";

class TopPplayersPage extends React.PureComponent<Props, TopPlayersPageState> {
    constructor(props: Props) {
        super(props);

        this.state = {
            date: false,
            limit: false,
            tableSize: false,
            pokerGameType: false,
            pokersite_id: 2,
            sort: "winnings desc",
        };

        this.props.onShowPage(TOP_PLAYERS);
        this.onFind();
    }

    onChangePokerGameTypeToFindType = (x: any) => {
        this.setState({
            pokerGameType: x.value,
        });
    }

    onChangeLimitToFindType = (x: any) => {
        this.setState({
            limit: x.value,
        });
    }

    onChangeTableSizeToFindType = (x: any) => {
        this.setState({
            tableSize: x.value,
        });
    }

    onChangeDateToFindType = (x: any) => {
        this.setState({
            date: x.value,
        });
    }

    onFind = () => {
        const { date, tableSize, pokerGameType, limit, sort, pokersite_id } = this.state;
        this.props.onFindTopPlayers(pokerGameType, limit, tableSize, date, sort, pokersite_id);
    }

    onSort = (val: string) => {
        this.setState({
            sort: val,
        }, this.onFind);
    }

    onChangeSite = (x: any) => {
        this.setState({
            pokersite_id: x.value,
        });
    }

    render() {
        const { user, topPlayers } = this.props;
        const { sort } = this.state;

        if (!user) {
            return null;
        }

        const { subscription_type, is_admin } = user;
        if (!is_admin && !Number.isInteger(subscription_type)) {
            return <div>Subscribe. Then you can get access this page.</div>;
        }

        const filteredLimitsToFind = LimitSelect.filter((x) => is_admin || subscription_type >= x.neededSubscriptionType);

        const pokerGameTypeSelectContent =
            <Select
                defaultValue={PokerGameTypeSelect[0]}
                onChange={this.onChangePokerGameTypeToFindType}
                options={PokerGameTypeSelect} />;

        const limitSelectContent =
            <Select
                defaultValue={LimitSelect[0]}
                onChange={this.onChangeLimitToFindType}
                options={filteredLimitsToFind} />;

        const tableSizeSelectContent =
            <Select
                defaultValue={TableSizeSelect[0]}
                onChange={this.onChangeTableSizeToFindType}
                options={TableSizeSelect} />;

        const dateSelectContent =
            <Select
                defaultValue={DateSelect[0]}
                onChange={this.onChangeDateToFindType}
                options={DateSelect} />;

        const pokerSiteSelectContent =
            <Select
                defaultValue={PokerSiteSelect[0]}
                onChange={this.onChangeSite}
                options={PokerSiteSelect} />;

        let content = null;
        if (topPlayers && topPlayers.length > 0) {
            const table = <StyledTbody>
                {topPlayers.map((x: any, index: number) => (
                    <StyledTr>
                        <StyledTd>
                            <StyledA target="_blank" href={`/statistics?nickname=${x.playername}`}>{x.playername}</StyledA>
                        </StyledTd>
                        <StyledTd>
                            {x.totalhands}
                        </StyledTd>
                        <StyledTd>
                            {x.winnings}
                        </StyledTd>
                        <StyledTd>
                            <StyledRate positive={x.winrate > 0}>{x.winrate}</StyledRate>
                        </StyledTd>
                        <StyledTd>
                            {x.home_limit}
                        </StyledTd>
                    </StyledTr>
                ))}
            </StyledTbody>;

            content = <StyledTable>
                <StyledThead>
                    <StyledTh>Players</StyledTh>
                    <SortableTh
                        label="Hands"
                        trueVal="totalhands desc"
                        falseVal="totalhands asc"
                        sort={sort}
                        onSort={this.onSort} />
                    <SortableTh
                        label="Winning"
                        trueVal="winnings desc"
                        falseVal="winnings asc"
                        sort={sort}
                        onSort={this.onSort} />
                    <StyledTh>Win rate</StyledTh>
                    <StyledTh>Home limit</StyledTh>
                </StyledThead>
                {table}
            </StyledTable>;
        }

        return (
            <StyledContainer style={{ maxWidth: 1100, flex: 1 }}>
                <div style={{ width: "100%", display: "flex", padding: "12px", paddingTop: 24 }}>
                    <StyledContent flex="row">
                        {pokerSiteSelectContent}
                        {pokerGameTypeSelectContent}
                        {limitSelectContent}
                        {tableSizeSelectContent}
                        {dateSelectContent}
                    </StyledContent>
                </div>
                <div style={{
                    width: "100%", display: "flex", padding: "12px", paddingTop: 24, flexDirection: "column",
                }}>
                    <StyledContent flex="column">
                        <Button
                            onClick={this.onFind}
                            size="large"
                            type="primary"
                            style={{
                                width: "170px", height: "50px",
                                fontSize: "18px", borderRadius: "6px",
                                alignSelf: "flex-start",
                                margin: "24px 12px 0 12px"
                            }}>Show results</Button>
                        <div style={{ width: "100%", padding: "12px", paddingTop: 24 }}>{content}</div>
                    </StyledContent>
                </div>
            </StyledContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector<any, StateProps>({
    user: selectUser(),
    topPlayers: selelctTopPlayers(),
});

export function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
    return {
        onShowPage: (key: string) => dispatch(showPage(key)),
        onFindTopPlayers: (pokerGameType: any, limit: any, tableSize: any, date: any, sort: string,
            pokersite_id: number) =>
            dispatch(Actions.findTopPlayers(pokerGameType, limit, tableSize, date, sort, pokersite_id)),
        dispatch,
    };
}


const withConnect = connect<StateProps, DispatchProps, OwnTopPlayersPageProps>(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "topPlayers", reducer });
const withSaga = injectSaga({ key: "topPlayers", saga });

const withAuth = authorize({ pageKey: TOP_PLAYERS });

export default withRouter(compose(
    withAuth,
    withReducer,
    withSaga,
    withConnect,
)(TopPplayersPage) as any);
