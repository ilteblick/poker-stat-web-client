import { BaseDispatchProps } from "../../types/redux/BaseDispatchProps";
import { BasePageStateProps, BasePageOwnProps } from "../../types/redux/BasePageProps";
import { IUser } from "../App/interfaces";

export interface OwnTopPlayersPageProps extends BasePageOwnProps {
}

export interface StateProps extends BasePageStateProps {
    user: IUser;
    topPlayers: any;
}

export interface DispatchProps extends BaseDispatchProps {
    onShowPage: (key: string) => any;
    onFindTopPlayers: (pokerGameType: any, limit: any, tableSize: any, date: any, sort: string, pokersite_id: number) => any;
}

export type Props = StateProps & DispatchProps & OwnTopPlayersPageProps;

export interface TopPlayersPageState {
    pokerGameType: boolean | string;
    limit: boolean | string;
    tableSize: boolean | string;
    date: boolean | string;
    sort: string;
    pokersite_id: number;
}
