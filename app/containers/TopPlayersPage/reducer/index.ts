import * as topPlayersActions from "../actions";
import { TopPlayersState } from "./types";

let initialState = new TopPlayersState();

function topPlayersReducer(state = initialState, action: topPlayersActions.Actions) {
    switch (action.type) {
        case topPlayersActions.SET_LOADED_TOP_PLAYERS: {
            return state.set("topPlayers", action.payload.topPlayers);
        }
        default:
            return state;
    }
}

export default topPlayersReducer;
