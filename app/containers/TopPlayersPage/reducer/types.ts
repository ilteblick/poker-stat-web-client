import { RecordFactory } from "../../../utilsTypes/immutableUtils/RecordFactory";
import { BasePageReducerState } from "../../../types/redux/BasePageReducerState";

export interface ITopPlayersState extends BasePageReducerState {
    topPlayers: boolean | Array<any>;
}

const topPlayersState = RecordFactory<ITopPlayersState>({
    topPlayers: false,
});

export class TopPlayersState extends topPlayersState {
}
