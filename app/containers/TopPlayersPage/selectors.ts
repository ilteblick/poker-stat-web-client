import { createSelector } from "reselect";
import { ITopPlayersState } from "./reducer/types";

const selectTopPlayersState = (state: any): ITopPlayersState => state.get("topPlayers");

const selelctTopPlayers = () => createSelector(
    selectTopPlayersState,
    (topPlayersState) => topPlayersState.topPlayers,
);

export {
    selelctTopPlayers
};
