import { ActionsUnion, createAction } from "../../utilsTypes/redux/actionUtils";

export const FIND_TOP_PLAYERS = "TopPLayersPage/FIND_TOP_PLAYERS";
export const SET_LOADED_TOP_PLAYERS = "TopPLayersPage/SET_LOADED_TOP_PLAYERS";

export const Actions = {
    findTopPlayers: (pokerGameType: any, limit: any, tableSize: any, date: any, sort: string, pokersite_id: number) =>
        createAction(FIND_TOP_PLAYERS, { pokerGameType, limit, tableSize, date, sort, pokersite_id }),
    setLoadedTopPlayers: (topPlayers: any) => createAction(SET_LOADED_TOP_PLAYERS, { topPlayers }),
};

export type Actions = ActionsUnion<typeof Actions>;
