import { fork, take, put, race } from "redux-saga/effects";
import { apiService } from "../../api/actionService/api-service";
import * as Actions from "./actions";
import { message } from "antd";

const get_top_players_url_segment = "ranking";

const findTopPlayerLyficycle = apiService.findLifecycle(get_top_players_url_segment);

function* watchFindTopPlayers() {
    while (true) {
        const { payload } = yield take(Actions.FIND_TOP_PLAYERS);

        yield put(apiService.find(get_top_players_url_segment, payload));

        const { result, error } = yield race({
            result: take(findTopPlayerLyficycle.RESOLVED),
            error: take(findTopPlayerLyficycle.REJECTED),
        });

        if (result) {
            yield put(Actions.Actions.setLoadedTopPlayers(result.payload.topPlayers));
        }

        if (error) {
            message.error("There is no data for these filters");
        }
    }
}

export default function* TopPlayersSaga() {
    yield fork(watchFindTopPlayers);
}
