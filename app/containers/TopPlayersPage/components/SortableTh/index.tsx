import * as React from "react";
import { StyledArrow, Th } from "./styled";

interface SortableThProps {
    label: string;
    trueVal: string;
    falseVal: string;
    sort: string;
    onSort: Function;
}

interface SortableThState {

}

export default class SortableTh extends React.PureComponent<SortableThProps, SortableThState> {
    onChangeSort = () => {
        const { sort, trueVal, falseVal, onSort } = this.props;

        if (sort === trueVal) {
            onSort(falseVal);
            return;
        }

        onSort(trueVal);

    }

    render() {
        const { label, sort, trueVal, falseVal } = this.props;

        const upArrow = sort === trueVal;
        const downArrow = sort === falseVal;
        return (
            <Th onClick={this.onChangeSort}>
                {label}
                {upArrow ? <StyledArrow type="positive" /> : null}
                {downArrow ? <StyledArrow type="negative" top /> : null}
                {!downArrow && !upArrow ? <StyledArrow type="inactive" /> : null}
            </Th>
        );
    }
}
