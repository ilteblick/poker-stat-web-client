import { fork, take, put, race, select } from "redux-saga/effects";
import { apiService } from "../../../app/api/actionService/api-service";
import * as ApiActions from "./actions";
import { ActionByType } from "../../utilsTypes/redux/actionUtils";

import { message } from "antd";
import { selectUser } from "../App/selectors";

export const inviteCodeUrlSegment = "invite_code";

const getInviteCodeLyfecyle = apiService.getLifecycle(inviteCodeUrlSegment);


function* watchInvitePageCode() {
    while (true) {
        (yield take(ApiActions.GET_INVITE_CODE)) as 
        ActionByType<ApiActions.Actions, typeof ApiActions.GET_INVITE_CODE>;

        const user = yield select(selectUser());

        yield put(apiService.get(inviteCodeUrlSegment, user.id));

        const { inviteCodeSuccessFromApi, inviteCodeErrorFromApi } = yield race({
            inviteCodeSuccessFromApi: take(getInviteCodeLyfecyle.RESOLVED),
            inviteCodeErrorFromApi: take(getInviteCodeLyfecyle.REJECTED),
        });

        if (inviteCodeErrorFromApi) {
            message.error(inviteCodeErrorFromApi.error.message);
            continue;
        }

        yield put(ApiActions.Actions.setInvitionCode(inviteCodeSuccessFromApi.payload.token));
    }
}

export default function* invitePageCodeSaga(): IterableIterator<any> {
    yield fork(watchInvitePageCode);
}
