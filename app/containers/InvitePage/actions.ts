import { ActionsUnion, createAction } from "../../utilsTypes/redux/actionUtils";

export const GET_INVITE_CODE = "InvitePage/GET_INVITE_CODE";
export const SET_INVITION_CODE = "InvitePage/SET_INVITION_CODE";

export const Actions = {
    getInviteCode: () => createAction(GET_INVITE_CODE, {}),
    setInvitionCode: (token: string) => createAction(SET_INVITION_CODE, { token }),
}


export type Actions = ActionsUnion<typeof Actions>;