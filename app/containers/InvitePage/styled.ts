import styled from "styled-components";
import {StyledThemeProps} from "../../utils/styleThemes";


export const CenteredContainer = styled.div`
    width: 900px;
    display: flex;
    align-items: flex-start;
    justify-content: center;
    flex: 1;

    @media (max-width: 900px){
        align-self: flex-start;
    }
`;

export const StyledContainer = styled<StyledThemeProps, "div">("div")`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin: 48px 64px;
    padding: 48px 64px;
    background: ${props => props.theme.contentBg}; 
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 64px;
    align-items: center;
    border-radius: 4px;
`;


export const StyledSpanBox = styled<StyledThemeProps, "span">("span")`
    width: 50%;
    font-size: 18px;
    font-weight: bold;
    color: ${props => props.theme.color};
    padding: 10px;
    margin-top: 20px;
    margin-bottom: 20px;
    border: 1px solid ${props => props.theme.inputBorderColor};
    border-radius: 4px;
    cursor: text;
    -webkit-transition: all .3s;
    transition: all .3s;

    &:hover {
        border-color: ${props => props.theme.buttonPrimary};
        border-right-width: 1px !important;
    }

    &::selection {
        background: ${props => props.theme.buttonPrimary};
    }
`;