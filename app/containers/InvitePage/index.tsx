import * as React from "react";
import { withRouter } from "react-router-dom";
import { connect, Dispatch } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Actions } from "./actions";
import { selectInvitionCode } from "./selectors";

import reducer from "./reducer";
import saga from "./sagas";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";

import { Button } from "antd";

import { StyledTitle } from "../../components/FormsItems";
import { OwnInvitePageProps, StateProps, DispatchProps, Props } from "./types";

import { CenteredContainer, StyledContainer, StyledSpanBox } from "./styled";
import { INVITE_CODE_PAGE } from "../App/constants";
import authorize from "../../utils/authorize";


class InviteCodePage extends React.PureComponent<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    onGetInviteCodeClick = () => {
        this.props.onGetInviteCode();
    }

    render() {
        const { invitionCode } = this.props;
        return (
            <CenteredContainer>
                <StyledContainer>
                    <StyledTitle>Invite Code</StyledTitle>
                    {invitionCode ? <StyledSpanBox>{invitionCode}</StyledSpanBox> : null}
                    <Button
                        size="large"
                        onClick={this.onGetInviteCodeClick}
                        type="primary"
                        style={{
                            width: "50%", height: "60px",
                            fontSize: "20px", borderRadius: "6px",
                            marginTop: 12
                        }}>
                        Get Invite Code
                    </Button>
                </StyledContainer>
            </CenteredContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector<any, StateProps>({
    invitionCode: selectInvitionCode(),
});

function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
    return {
        onGetInviteCode: () => dispatch(Actions.getInviteCode()),
        dispatch,
    };
}

const withConnect = connect<StateProps, DispatchProps, OwnInvitePageProps>(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "inviteCode", reducer });
const withSaga = injectSaga({ key: "inviteCode", saga });

const withAuth = authorize({ pageKey: INVITE_CODE_PAGE });

export default withRouter(compose(
    withAuth,
    withReducer,
    withConnect,
    withSaga,
)(InviteCodePage) as any);
