import * as InviteCodePageActions from "../actions";
import { InviteCodePageState } from "./types";

let initialState = new InviteCodePageState();

function inviteCodePageReducer(state = initialState, action: InviteCodePageActions.Actions) {
    switch (action.type) {
        case InviteCodePageActions.SET_INVITION_CODE:
            return state.set("invitionCode", action.payload.token);
        default:
            return state;
    }
}


export default inviteCodePageReducer;