import { RecordFactory } from "../../../utilsTypes/immutableUtils/RecordFactory";
import { BasePageReducerState } from "../../../types/redux/BasePageReducerState";

export interface IInviteCodePage extends BasePageReducerState {
    invitionCode: string;
}

const inviteCodePageState = RecordFactory<IInviteCodePage>({
    invitionCode: "",
});

export class InviteCodePageState extends inviteCodePageState {
}