import { createSelector } from "reselect";
import { IInviteCodePage } from "./reducer/types";

const selectInviteCodePageState = (state: any): IInviteCodePage => state.get("inviteCode");

const selectInvitionCode = () => createSelector(
    selectInviteCodePageState,
    (InviteCodePageState) => InviteCodePageState.invitionCode
);

export {
    selectInvitionCode,
};