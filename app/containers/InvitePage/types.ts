import { BaseDispatchProps } from "../../types/redux/BaseDispatchProps";
import { BasePageStateProps, BasePageOwnProps } from "../../types/redux/BasePageProps";

export interface OwnInvitePageProps extends BasePageOwnProps {
    invitionCode?: string;
    onGetInviteCode?: Function;
}

export interface StateProps extends BasePageStateProps {
    invitionCode: string;
}

export interface DispatchProps extends BaseDispatchProps {
    onGetInviteCode: () => any;
}

export type Props = StateProps & DispatchProps & OwnInvitePageProps;