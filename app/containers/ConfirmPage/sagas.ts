import { fork, put, race, take } from "redux-saga/effects";
import { apiService } from "../../api/actionService/api-service";
import { CONFIRM_REGISTRATION } from "./constants";

import { message } from "antd";
import { setCofirmResult } from "./actions";

const confirm_url_segment = "confirm";
const confirmLyfecyle = apiService.postLifecycle(confirm_url_segment);

function* watchConfirm(): IterableIterator<any> {
    while (true) {
        const { payload } = yield take(CONFIRM_REGISTRATION);

        yield put(apiService.post(confirm_url_segment, payload));

        const { confirmSuccessFromApi, confirmErrorFromApi } = yield race({
            confirmSuccessFromApi: take(confirmLyfecyle.RESOLVED),
            confirmErrorFromApi: take(confirmLyfecyle.REJECTED),
        });

        if (confirmErrorFromApi) {
            message.error(confirmErrorFromApi.error.message);
            yield put(setCofirmResult({
                ok: false,
            }));
            continue;
        }

        if (confirmSuccessFromApi) {
            message.success("Congratulations, you have successfully registered.");
            yield put(setCofirmResult({
                ok: true,
            }));
        }
    }
}

export default function* sagas() {
    yield fork(watchConfirm);
}
