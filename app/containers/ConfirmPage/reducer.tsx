import { fromJS } from "immutable";

import { Action } from "../../basicClasses";
import { SET_CONFIRM_RESULT } from "./constants";

const initialState = fromJS({
    result: null,
});

function confirmReducer(state = initialState, action: Action) {
    switch (action.type) {
        case SET_CONFIRM_RESULT:
            return state.set("result", action.payload);
        default:
            return state;
    }
}

export default confirmReducer;
