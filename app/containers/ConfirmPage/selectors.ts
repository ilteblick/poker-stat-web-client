import { createSelector } from "reselect";

const selectGlobal = () => (state: any) => {
    return state.get("confirm");
};

const selectResult = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("result")
);

export {
    selectResult,
};
