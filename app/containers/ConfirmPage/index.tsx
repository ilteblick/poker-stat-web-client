import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { push } from "react-router-redux";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";

import saga from "./sagas";
import reducer from "./reducer";

import { confirmRegistration } from "./actions";
import { selectResult } from "./selectors";

interface ConfirmProps {
    onConfirmRegistration: Function;
    location: any;
    redirect?: Function;
    result: any;
}

class ConfirmPage extends React.Component<ConfirmProps, {}> {
    constructor(props: ConfirmProps) {
        super(props);

        const { onConfirmRegistration, location } = props;

        const params = new URLSearchParams(location.search);
        const token = params.get("token");
        const email = params.get("email");
        if (token && email) {
            onConfirmRegistration(token, email);
        }
    }

    componentWillReceiveProps(nextProps: ConfirmProps) {
        if (nextProps.result) {
            setTimeout(() => this.props.redirect("/"), 2000);
        }
    }

    render() {
        return (
            <div style={{ width: 1100, maxWidth: 1100, flex: 1 }}>
                confirm
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    result: selectResult(),
});

export function mapDispatchToProps(dispatch: Function) {
    return {
        onConfirmRegistration: (token: string, email: string) => dispatch(confirmRegistration(token, email)),
        redirect: (url: string) => dispatch(push(url)),
    };
}


const withConnect = connect<{}, {}, ConfirmProps>(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "confirm", reducer });
const withSaga = injectSaga({ key: "confirm", saga });

export default withRouter(compose(
    withReducer,
    withSaga,
    withConnect,
)(ConfirmPage) as any);
