import { Action } from "../../basicClasses";
import { CONFIRM_REGISTRATION, SET_CONFIRM_RESULT } from "./constants";

export function confirmRegistration(token: string, email: string): Action {
    return {
        type: CONFIRM_REGISTRATION,
        payload: {
            token, email
        }
    };
}

export function setCofirmResult(message: any): Action {
    return {
        type: SET_CONFIRM_RESULT,
        payload: message
    };
}
