import "whatwg-fetch";
import { fork, put, race, take } from "redux-saga/effects";
import resisterSaga from "./containers/RegisterModal/sagas";
import signInSaga, { signInUrlSegment } from "./containers/SignInModal/sagas";
import { apiService } from "../../api/actionService/api-service";
import { setVerifiedUser } from "./containers/RegisterModal/containers/VerifyTokenForm/actions";
import { LOGOUT } from "./constants";
import { logoutSucces, initialLoadProcessed } from "./actions";
import inviteSaga from "./containers/InviteModal/sagas";

const logout_url_segment = "logout";
const signInLyfecyle = apiService.postLifecycle(signInUrlSegment);
const logoutLyfecyle = apiService.postLifecycle(logout_url_segment);


function* watchLogout(): IterableIterator<any> {
    while (true) {
        yield take(LOGOUT);

        yield put(apiService.post(logout_url_segment));

        const { logoutSuccessFromApi, logoutErrorFromApi } = yield race({
            logoutSuccessFromApi: take(logoutLyfecyle.RESOLVED),
            logoutErrorFromApi: take(logoutLyfecyle.REJECTED),
        });


        if (logoutErrorFromApi) {
            // message.error(signInErrorFromApi.error.message);
            continue;
        }
        localStorage.setItem("old-poker-statistic-auth", localStorage.getItem("poker-statistic-auth"));
        localStorage.removeItem("poker-statistic-auth");
        yield put(logoutSucces());
    }
}

// function makeGitReq() {
//     return fetch("https://api.github.com/users/ilteblick/repos?type=all&sort=updated")
//         .then((resp) => resp.json());
// }

// function makeGet() {
//     return fetch("http://localhost:2999/node/poker-stat-api/invite_code/1")
//         .then((resp) => resp.json());
// }

export default function* appSaga(): IterableIterator<any> {
    // const git = yield call(makeGitReq);
    // const get = yield call(makeGet);
    // console.log(git, get);

    const auth = localStorage.getItem("poker-statistic-auth");
    const oldAuth = localStorage.getItem("old-poker-statistic-auth");

    document.cookie = `poker-statistic-auth=${localStorage.getItem("poker-statistic-auth")}`;
    document.cookie = `old-poker-statistic-auth=${localStorage.getItem("old-poker-statistic-auth")}`;

    yield put(apiService.post(signInUrlSegment, { auth, oldAuth }));

    const { signInSuccessFromApi, signInErrorFromApi } = yield race({
        signInSuccessFromApi: take(signInLyfecyle.RESOLVED),
        signInErrorFromApi: take(signInLyfecyle.REJECTED),
    });

    if (signInErrorFromApi) {
        // message.error(signInErrorFromApi.error.message);
    }

    if (signInSuccessFromApi) {
        yield put(setVerifiedUser(signInSuccessFromApi.payload.user));
    }

    yield fork(watchLogout);
    yield fork(resisterSaga);
    yield fork(signInSaga);
    yield fork(inviteSaga);

    yield put(initialLoadProcessed(true));
}
