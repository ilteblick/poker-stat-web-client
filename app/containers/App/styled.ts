import styled from "styled-components";
import { Layout } from "antd";
import { ComponentType } from "react";
import { StyledThemeProps } from "../../utils/styleThemes";
import { StyledLayoutProps, StyledContentProps } from "./types";

const { Content } = Layout;

export const StyledLayout = styled<Partial<StyledLayoutProps>>(Layout as ComponentType)`
    height: 100%;
    background-color: ${props => props.isportable ? props.theme.bg : props.theme.bg};
`;

export const StyledFooter = styled<StyledThemeProps, "div">("div")`
    padding: 10px;
    background: ${ptops => ptops.theme.contentBg};
    color: ${ptops => ptops.theme.footerColor};
    width: 100%;
    font-size: 18px;
    margin-top: auto;
`;

export const StyledContent = styled<Partial<StyledContentProps>>(Content as ComponentType)`
    height: 100%;
    background-color: ${props => props.isportable ? props.theme.bg : props.theme.bg};
    ${props => props.isportable
        ? ""
        : `
        display: flex;
        overflow: auto;
        background: url(${props.theme.images.appBg});
        box-shadow: 0 0 1px rgba(0,0,0,0.1) inset;
        flex: 1;
        align-items: center;
        flex-direction: column;
    `}
`;


