import * as React from "react";
import { Link, withRouter } from "react-router-dom";
import { push } from "react-router-redux";
import { compose } from "redux";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import {
    HeaderProps,
} from "./types";
import { selectAutopast, selectCurrentPage, selectPortableMode, selectUser, selectTheme } from "../../selectors";
import { logout, openInviteModal, setAutopast, setPortableMode, setTheme } from "../../actions";
import {
    StyledClickHere,
    StyledForgot,
    StyledHeaderLogo,
    StyledImg,
    StyledLogoSpan,
    StyledUserBar,
    StyledHeader,
    StyledHeaderLeftPart,
} from "./styled";
import HeaderIcon from "./components/HeaderIcon";
import ZoomApp from "../../../../components/ZoomApp";
import { CONTACT_US, HOME_PAGE, PRICE_PAGE, PROFILE_PAGE, STAT_PAGE, TOP_PLAYERS, INVITE_CODE_PAGE } from "../../constants";
import { checkPortableMode } from "../../utils";
import MenuButton from "./components/MenuButton";
import { ThemeType } from "../../../../utils/styleThemes";

const menuItems = [{
    to: "/",
    label: "Home",
    alias: HOME_PAGE,
}, {
    to: "/statistics",
    label: "Statistics",
    alias: STAT_PAGE,
}, {
    to: "/top-players",
    label: "Top players",
    alias: TOP_PLAYERS,
}, {
    to: "/price",
    label: "Price",
    alias: PRICE_PAGE,
    disabled: true,
}, {
    to: "/contact",
    label: "Contact us",
    alias: CONTACT_US,
}];

class Header extends React.Component<HeaderProps> {

    onLogoutClick = () => {
        this.props.onLogout();
    }

    onOpenInviteModalClick = () => {
        this.props.onOpenInviteModal();
    }

    onSetAutopastClick = () => {
        this.props.onSetAutopast(!this.props.autopast);
    }

    onSetPortableModeClick = () => {
        this.props.onSetPortableMode(true);
        if (this.props.location.pathname.indexOf("/statistics") === -1) {
            this.props.goTo("/statistics");
        }
    }

    onSetThemeMode = () => {
        this.props.onSetTheme(this.props.appTheme === "light" ? "dark" : "light");
    }

    renderButton = () => {
        const {
            user,
            currentPage,
            autopast,
            appTheme,
        } = this.props;
        let authComponent = null, dateToDisplay = null;
        if (user) {
            const date = new Date(parseInt(user.expiration, 10));
            dateToDisplay = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
        }

        if (user && user.email) {
            authComponent = <StyledUserBar>
                <HeaderIcon icon="clock" label={`Up to ${dateToDisplay}`} drop sizeIcon="small" first />
                <HeaderIcon
                    customItem={
                        <ZoomApp />
                    }
                    sizeIcon="big"
                />
                <HeaderIcon icon="portable_mode" onClick={this.onSetPortableModeClick} sizeIcon="big" />
                <HeaderIcon onClick={this.onSetAutopastClick} label="Autopast" isSwitch active={autopast} />
                <HeaderIcon icon={`theme_${appTheme}`} onClick={this.onSetThemeMode} size="big" />
                <HeaderIcon
                    icon="setting"
                    label="Profile"
                    link="/profile"
                    active={currentPage === PROFILE_PAGE}
                    sizeIcon="small"
                    size="big"
                />
                <HeaderIcon
                    icon="invite"
                    label="Invite"
                    sizeIcon="small"
                    link="/invite-code"
                    active={currentPage === INVITE_CODE_PAGE}
                />
                <HeaderIcon icon="logout" label="Logout" onClick={this.onLogoutClick} sizeIcon="small" last />
            </StyledUserBar>;
        } else {
            authComponent = <div>
                <StyledForgot>Forgot your password? </StyledForgot>
                <StyledClickHere to="/reset"> Click Here</StyledClickHere>
            </div>;
        }
        return authComponent;
    }

    render() {
        const {
            user,
            currentPage,
            portableMode,
        } = this.props;

        if (checkPortableMode(currentPage, portableMode)) {
            return null;
        }
        return (
            <StyledHeader>
                <StyledHeaderLeftPart>
                    <StyledHeaderLogo>
                        <Link to="/"><StyledImg /></Link>
                        <StyledLogoSpan>Hand4Hand</StyledLogoSpan>
                    </StyledHeaderLogo>
                    {user
                        ? (
                            <MenuButton
                                items={menuItems}
                                activePage={currentPage}
                            />
                        ) : null}
                </StyledHeaderLeftPart>
                {this.renderButton()}
            </StyledHeader>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    user: selectUser(),
    currentPage: selectCurrentPage(),
    autopast: selectAutopast(),
    portableMode: selectPortableMode(),
    appTheme: selectTheme(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onOpenInviteModal: () => dispatch(openInviteModal()),
        onLogout: () => dispatch(logout()),
        onSetAutopast: (state: boolean) => dispatch(setAutopast(state)),
        onSetPortableMode: (state: boolean) => dispatch(setPortableMode(state)),
        onSetTheme: (state: ThemeType) => dispatch(setTheme(state)),
        goTo: (location: string) => dispatch(push(location)),
    };
}

const withConnect = connect<{}, {}, HeaderProps>(mapStateToProps, mapDispatchToProps);

export default withRouter(compose(
    withConnect
)(Header) as any);
