import { IUser } from "../../interfaces";

export interface HeaderProps {
    autopast?: boolean;
    portableMode?: boolean;
    currentPage?: string;
    appTheme?: string;
    location?: any;
    user?: IUser;
    onLogout?: Function;
    onOpenInviteModal?: Function;
    onSetAutopast?: Function;
    onSetPortableMode?: Function;
    onSetTheme?: Function;
    goTo?: Function;
}
