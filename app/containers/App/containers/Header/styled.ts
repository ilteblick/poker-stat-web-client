import styled from "styled-components";
import { Link } from "react-router-dom";
import { Layout } from "antd";
import { StyledThemeProps } from "../../../../utils/styleThemes";
import { ComponentType } from "react";

const {Header} = Layout;

export const StyledImg = styled.img.attrs({
    src: props => props.theme.images.logo
})`
    cursor: pointer;
    width: 40px;
`;

export const StyledLogoSpan = styled<StyledThemeProps, "span">("span")`
    font-weight: bold;
    font-size: 20px;
    color: ${props => props.theme.color};
    margin-left: 12px;

    @media (max-width: 950px){
        display: none;
    }
`;

export const StyledForgot = styled.span`
    color: #acacac;
    font-size: 18px;
`;

export const StyledClickHere = styled<Partial<StyledThemeProps>>(Link as ComponentType)`
    color: ${props => props.theme.hrefColor};
    font-size: 18px;
    cursor: pointer;

    &:hover {
        color: ${props => props.theme.hrefColor_hover};
    }

    &:focus{
        text-decoration:none;
    }
`;

export const StyledHeader = styled<Partial<StyledThemeProps>>(Header as ComponentType)`
    background: ${props => props.theme.contentBg};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    z-index: 1;
    box-shadow: 8px 0 35px rgba(0,0,0,0.08);
`;

export const StyledHeaderLeftPart = styled.div`
    display: flex;
    align-items: center;
    flex: 1;
    max-width: 700px;
`;

export const StyledUserBar = styled.div`
    display: flex;
`;

export const StyledHeaderLogo = styled.div`
    display: flex;
    align-items: center;
    margin-right: 50px;

    @media (max-width: 1200px){
        margin-right: 10px;
    }
`;
