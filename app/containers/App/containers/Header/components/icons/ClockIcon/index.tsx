import * as React from "react";

import {StyledIcon} from "./styled";

export default () => (
    <StyledIcon width="24" height="24" viewBox="0 0 24 24">
        <path
            d="M1219,29a12,12,0,1,0,12,12A12.012,12.012,0,0,0,1219,29Zm0,21.447A9.447,9.447,0,1,1,1228.45,41,9.46,9.46,0,0,1,1219,50.447Zm6.25-9.818h-5.39V34.145a0.99,0.99,0,0,0-1.98,0v7.472a0.993,0.993,0,0,0,.99.988h6.38A0.988,0.988,0,1,0,1225.25,40.629Z"
            transform="translate(-1207 -29)"
        />
    </StyledIcon>
);
