import * as React from "react";

import {StyledIcon} from "./styled";

export default () => (
    <StyledIcon width="47" height="31" viewBox="0 0 47 31">
        <path
            d="M398,35h47v3H398V35Zm0,14h47v3H398V49Zm0,14h24v3H398V63Z"
            transform="translate(-398 -35)"
        />
    </StyledIcon>
);
