import * as React from "react";

import {StyledIcon} from "./styled";

export default () => (
    <StyledIcon width="20" height="22" viewBox="0 0 20 22">
        <path
            d="M1742,33.672a0.909,0.909,0,0,0-1.2.473,0.921,0.921,0,0,0,.47,1.207,8.18,8.18,0,1,1-6.54,0,0.916,0.916,0,0,0-.73-1.68A10,10,0,1,0,1742,33.672Zm-4,7.411a0.915,0.915,0,0,0,.91-0.917v-8.25a0.91,0.91,0,1,0-1.82,0v8.25A0.915,0.915,0,0,0,1738,41.083Z"
            transform="translate(-1728 -31)"
        />
    </StyledIcon>
);
