import * as React from "react";
import { SizeType, SizeIconType, TypeIcon } from "./types";
import InviteIcon from "../icons/InviteIcon";
import ClockIcon from "../icons/ClockIcon";
import FullModeIcon from "../icons/FullModeIcon";
import LogoutIcon from "../icons/LogoutIcon";
import PortableModeIcon from "../icons/PortableModeIcon";
import ThemeModeIcon from "../icons/ThemeModeIcon";
import ThemeSunIcon from "../icons/SunIcon";
import SettingsIcon from "../icons/SettingsIcon";

const icons = {
    invite: InviteIcon,
    clock: ClockIcon,
    full_mode: FullModeIcon,
    logout: LogoutIcon,
    portable_mode: PortableModeIcon,
    theme_dark: ThemeSunIcon,
    theme_light: ThemeModeIcon,
    setting: SettingsIcon,
};

import { StyledWrapper, StyledImgContainer, StyledSpan, StyledWrapperLink, StyledContent, StyledSwitch } from "./styled";

interface HeaderProps {
    icon?: TypeIcon;
    label?: string;
    link?: string;
    drop?: boolean;
    active?: boolean;
    revers?: boolean;
    onClick?: any;
    sizeIcon?: SizeIconType;
    size?: SizeType;
    isSwitch?: boolean;
    customItem?: any;
    last?: boolean;
    first?: boolean;
}

export default class HeaderIcon extends React.PureComponent<HeaderProps, {}> {

    static defaultProps: Partial<HeaderProps> = {
        sizeIcon: "big",
        size: "big",
    };

    renderContent = () => {
        const { icon, label, sizeIcon, isSwitch, customItem, size, revers, last, first } = this.props;
        const Icon = icon ? (icons[icon] || null) : undefined;

        if (customItem) {
            return (
                <StyledContent size={size} revers={revers}>
                    {customItem}
                </StyledContent>
            );
        }
        return (
            <StyledContent size={size} revers={revers} last={last} first={first}>
                {Icon !== undefined && <StyledImgContainer size={size} sizeIcon={sizeIcon}>
                    {<Icon />}
                </StyledImgContainer>}
                {label && <StyledSpan>{label}</StyledSpan>}
                {isSwitch && <StyledSwitch size="small" checked={/*!!active*/false} />}
            </StyledContent>
        );
    }

    render() {
        const { onClick, drop, link, active, size } = this.props;

        if (link) {
            return (
                <StyledWrapperLink to={link} active={active ? active.toString() : undefined} onClick={onClick} size={size}>
                    {this.renderContent()}
                </StyledWrapperLink>
            );
        }
        return (
            <StyledWrapper onClick={onClick} drop={drop} active={active ? active.toString() : undefined} size={size} to={""}>
                {this.renderContent()}
            </StyledWrapper>
        );
    }
}
