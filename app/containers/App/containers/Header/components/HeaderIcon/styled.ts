import styled from "styled-components";
import { Link } from "react-router-dom";
import { Switch } from "antd";
import { StyledWrapperProps, StyledImageContainerProps, StyledContentProps } from "./types";
import { ComponentType } from "react";
import { SwitchProps } from "react-router";

const container = (props: StyledWrapperProps) => (`
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    color: ${props.active === "true" ? props.theme.menuColor_active : props.theme.menuColor};
    svg {
        transition: 100ms all;
        fill: ${props.active === "true" ? props.theme.menuColor_active : props.theme.menuColor};
    }
    transition: 100ms all;
    :not(:first-child):before {
        content: '';
        width: 1px;
        height: ${props.size === "big" ? "48px" : "23px"};
        align-self: center;
        background: ${props.theme.menuSeparatorBg};
    }
    :hover {
        color: ${props.theme.menuColor_hover};
        svg {
            fill: ${props.theme.menuColor_hover};
        }
    }
    ${props.drop ? `
    svg {
        fill: ${props.theme.menuColor_drop};
    }
    :hover {
        color: ${props.theme.menuColor};
        svg {
            fill: ${props.theme.menuColor_drop};
        }
    }
    ` : ""}
    @media (max-width: 720px){
        ${props.drop ? "display: none" : ""}
    }
`);

/*
Узнать нодо ли эти отступы
@media (max-width: 1060px){
    margin: 0 12px;
}
@media (max-width: 960px){
    margin: 0 6px;
}
 */

export const StyledWrapper = styled<StyledWrapperProps, "div">("div")`${props => container(props)}`;

export const StyledWrapperLink = styled<StyledWrapperProps>(Link)`${props => container(props)}`;

export const StyledImgContainer = styled<StyledImageContainerProps, "div">("div")`
    margin: 0 auto;
    text-align: center;
    line-height: 1em;
    ${props => props.size === "big"
        ? props.sizeIcon === "big"
            ? `width: 32px; height: 32px;`
            : `width: 22px; height: 22px;`
        : props.sizeIcon === "big"
            ? `width: 23px; height: 23px;`
            : `width: 15px; height: 15px;`
    }
    > svg {
       max-width: 100%;
       max-height: 100%;
    }
`;

export const StyledSpan = styled.span`
    line-height: initial;
    font-weight: bold;
    font-size: 12px;
`;

export const StyledSwitch = styled<SwitchProps>(Switch as ComponentType)`
    max-width: 35px;
    margin: 0 auto;
`;

export const StyledContent = styled<StyledContentProps, "div">("div")`
    display: flex;
    flex-direction: ${props => props.size === "big"
        ? props.revers
            ? "column-reverse"
            : "column"
        : props.revers
            ? "row-reverse"
            : "row"
    };
    padding: ${props => props.size === "big" ? "0 24px" : "0 13px"};


    & > * {
        :not(:first-child) {
            ${props => props.size === "big"
        ? props.revers
            ? "margin-bottom: 2px;"
            : "margin-top: 2px;"
        : props.revers
            ? "margin-right: 2px;"
            : "margin-left: 2px;"
    }
            &${StyledSwitch} {
                ${props => props.size === "big"
        ? props.revers
            ? "margin-bottom: 8px;"
            : "margin-top: 8px;"
        : props.revers
            ? "margin-right: 8px;"
            : "margin-left: 8px;"
    }
            }
        }
    }

        ${props => props.last ? "padding-right: 0px" : ""}
        ${props => props.first ? "padding-left: 0px" : ""}

        @media (max-width: 1200px){
        padding: ${props => props.size === "big" ? "0 18px" : "0 9px"};
        ${props => props.last ? "padding-right: 0px" : ""}
        ${props => props.first ? "padding-left: 0px" : ""}

        @media (max-width: 1000px){
        padding: ${props => props.size === "big" ? "0 12px" : "0 4px"};
        ${props => props.last ? "padding-right: 0px" : ""}
        ${props => props.first ? "padding-left: 0px" : ""}
    }
`;
