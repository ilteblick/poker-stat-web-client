import { StyledThemeProps } from "../../../../../../utils/styleThemes";
import { LinkProps } from "react-router-dom";

export type SizeIconType = "big" | "small";
export type SizeType = "big" | "small";
export type TypeIcon = "invite" | "clock" | "full_mode" | "logout" | "portable_mode" | "theme_light" | "theme_dark" | "setting";

export interface StyledWrapperProps extends StyledThemeProps, LinkProps {
    drop?: boolean;
    active?: string;
    size?: SizeType;
}

export interface StyledContentProps extends StyledThemeProps {
    size?: SizeType;
    revers?: boolean;
    last?: boolean;
    first?: boolean;
}

export interface StyledImageContainerProps extends StyledThemeProps {
    sizeIcon?: SizeIconType;
    size?: SizeType;
}
