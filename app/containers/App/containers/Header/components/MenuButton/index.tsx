import * as React from "react";
import { StatsItemState, StatsItemProps } from "./types";

import { StyledContainer, StyledButton, StyledItem, StyledItems, StyledItemLi, StyledLabel } from "./styled";

import MenuIcon from "../icons/MenuIcon/index";


class StatsItem extends React.PureComponent<StatsItemProps, {}> {
    componentDidMount() {
        window.addEventListener("click", this.handleOutsideClick);
    }

    handleOutsideClick = (e: MouseEvent) => {
        if (this.state.active && !this.containerRef.contains(e.target as Node)) {
            this.setState({ active: false });
        }
    }

    containerRef: HTMLDivElement;
    state: StatsItemState = {
        active: false
    };

    static defaultProps: Partial<StatsItemProps> = {
        items: [],
    };

    onClick = () => {
        this.setState((prevState: StatsItemState) => ({ active: !prevState.active }));
    }

    getContainerRef = (ref: HTMLDivElement) => {
        this.containerRef = ref;
    }

    onLinkClick = () => {
        this.setState({ active: false });
    }

    render() {
        const {
            props: {
                items,
                activePage,
            },
            state: {
                active,
            }
        } = this;
        return (
            <StyledContainer active={active} ref={this.getContainerRef}>
                <StyledButton onClick={this.onClick}>
                    <MenuIcon /><span>MENU</span>
                </StyledButton>
                <StyledItems active={active}>
                    {items.map((item, key) => (
                        <StyledItemLi key={key}>
                            {item.disabled ? <StyledLabel>{item.label}</StyledLabel> :
                                <StyledItem activepage={(activePage === item.alias).toString()}
                                    to={item.to} onClick={this.onLinkClick}>
                                    {item.label}
                                </StyledItem>
                            }
                        </StyledItemLi>
                    ))}
                </StyledItems>
            </StyledContainer>
        );
    }
}

export default StatsItem;

