import styled from "styled-components";
import { Link, LinkProps } from "react-router-dom";
import { StyledThemeProps } from "../../../../../../utils/styleThemes";
import { ComponentType } from "react";

interface StyledMenuProps extends StyledThemeProps {
    active?: boolean;
}

interface StyledItemProps extends StyledThemeProps, LinkProps {
    activepage?: string;
}

export const StyledButton = styled.div`
    display: flex;
    align-items: center;
    padding: 0 30px;
    color: ${(props) => props.theme.color};
    > svg {
        margin-right: 20px;
    }

    span {
        @media (max-width: 1100px){
            display: none;
        }
    }

    @media (max-width: 950px){
        padding: 0 10px;
        }
`;

export const StyledItems = styled.ul`
    z-index: 10;
    transition: all 300ms;
    list-style: none;
    width: 320px;
    top: 100%;
    left: 0;
    position: absolute;
    background: ${(props: StyledMenuProps) => props.theme.menuBg};
    padding: 15px 0;
    opacity: 0;
    visibility: hidden;
    ${(props: StyledMenuProps) => props.active ? "opacity: 1; visibility: visible;" : ""}
`;

export const StyledItemLi = styled.li`
    line-height: 1em;
`;

export const StyledItem = styled<StyledItemProps>(Link)`
    padding: 10px 15px;
    display: inline-block;
    width: 100%;
    line-height: 1em;
    text-align: center;
    color: ${(props) => props.theme.color};
    ${(props) => props.activepage === "true" ? `color: ${props.theme.menuColor_active};` : ""}
    :hover {
        color: ${(props) => props.theme.menuColor_hover};
    }
`;

export const StyledLabel = styled.span`
    padding: 10px 15px;
    display: inline-block;
    width: 100%;
    line-height: 1em;
    text-align: center;
    color: ${(props) => props.theme.color};
    cursor: no-drop;
    color: gray;
`;

export const StyledContainer = styled.div`
    transition: all 300ms;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    cursor: pointer;
    user-select: none;
    font-weight: bold;
    height: 100%;
    :hover {
        background-color: ${(props: StyledMenuProps) => props.theme.menuBg};
    }
    ${(props: StyledMenuProps) => props.active ? `background-color: ${props.theme.menuBg};` : ""}
`;
