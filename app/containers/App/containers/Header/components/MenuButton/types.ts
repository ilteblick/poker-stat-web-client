import { StyledThemeProps } from "../../../../../../utils/styleThemes";

export interface TypeItem {
    to: string;
    label: string;
    alias: string;
    disabled?: boolean;
}

export interface StatsItemProps {
    activePage?: string;
    items?: TypeItem[];
}

export interface StatsItemState {
    active: boolean;
}
