import { createSelector } from "reselect";

import { selectGlobal } from "../../selectors";

const selectSignInModal = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("signInModal")
);

const selectNeedToShowSignInModel = () => createSelector(
    selectSignInModal(),
    (signInModal) => {
        return signInModal.get("needToShowSignInModal");
    }
);

const selectCurrentSignInStep = () => createSelector(
    selectSignInModal(),
    (registerModal) => {
        return registerModal.get("currentSignInStep");
    }
);

export {
    selectSignInModal,
    selectNeedToShowSignInModel,
    selectCurrentSignInStep,
};
