import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Modal, Button } from "antd";

import SignInForm from "./containers/SignInForm";

import { selectNeedToShowSignInModel, selectCurrentSignInStep } from "./selectors";
import { signIn, closeSignInModal } from "./actions";
import { SignInModel } from "./containers/SignInForm/interfaces";

import VerifyTokenForm from "../RegisterModal/containers/VerifyTokenForm";

export interface SignInModalProps {
    needToShowSignInModal?: boolean;
    currentSignInStep?: number;
    onSignIn?: Function;
    onCloseSignInModal?: Function;
}

class SignInModal extends React.PureComponent<SignInModalProps, {}> {

    onCloseSignInModalClick = () => {
        this.props.onCloseSignInModal();
    }
    onSignInClick = (signInModel: SignInModel) => {
        this.props.onSignIn(signInModel);
    }

    render() {
        const { needToShowSignInModal, currentSignInStep } = this.props;

        let content = null;
        switch (currentSignInStep) {
            case 0: {
                content = <SignInForm onSignIn={this.onSignInClick} />;
                break;
            }
            case 1: {
                content = <VerifyTokenForm />;
            }
        }
        return (
            <Modal
                title="Sign in"
                visible={needToShowSignInModal}
                footer={[
                    <Button key="back" onClick={this.onCloseSignInModalClick}>Cancel</Button>,
                ]}
                onCancel={this.onCloseSignInModalClick}>
                {content}
            </Modal>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    needToShowSignInModal: selectNeedToShowSignInModel(),
    currentSignInStep: selectCurrentSignInStep(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onCloseSignInModal: () => dispatch(closeSignInModal()),
        onSignIn: (registerModel: SignInModel) => dispatch(signIn(registerModel)),
    };
}

const withConnect = connect<{}, {}, SignInModalProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(SignInModal);
