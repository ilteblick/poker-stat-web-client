import { SignInModel } from "./containers/SignInForm/interfaces";
import { Action } from "../../../../basicClasses";
import { SIGN_IN, CLOSE_SIGN_IN_MODAL, SHOW_VERIFY_AFTER_SIGN_IN } from "./constants";
import { IUser } from "../../interfaces";

export function closeSignInModal(): Action {
    return {
        type: CLOSE_SIGN_IN_MODAL,
    };
}

export function signIn(signInModel: SignInModel): Action {
    return {
        type: SIGN_IN,
        payload: signInModel,
    };
}

export function showVerifyAfterSignIn(user: IUser) {
    return {
        type: SHOW_VERIFY_AFTER_SIGN_IN,
        payload: user,
    };
}
