import * as React from "react";

import { Input, Form, Button } from "antd";
import { FormComponentProps } from "antd/lib/form";
import { SignInModel } from "./interfaces";
const FormItem = Form.Item;


const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

interface SignInFormProps extends FormComponentProps {
    onSignIn: Function;
}

class SignInForm extends React.PureComponent<SignInFormProps, {}> {
    handleSubmit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err: Error, values: any) => {
            if (!err) {
                const signInModel: SignInModel = {
                    email: values.email,
                    password: values.password,
                };
                this.props.onSignIn(signInModel);
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem
                    {...formItemLayout}
                    label="E-mail">
                    {getFieldDecorator("email", {
                        rules: [{
                            type: "email", message: "The input is not valid E-mail!",
                        }, {
                            required: true, message: "Please input your E-mail!",
                        }],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="Password"
                >
                    {getFieldDecorator("password", {
                        rules: [{
                            required: true, message: "Please input your password!",
                        }],
                    })(
                        <Input type="password" />
                    )}
                </FormItem>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Sign In</Button>
                </FormItem>
            </Form>
        );
    }
}

const WrappedSignInForm = Form.create()(SignInForm);
export default WrappedSignInForm;

