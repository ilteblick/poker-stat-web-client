import { fork, take, put, race } from "redux-saga/effects";
import { SIGN_IN } from "./constants";
import { apiService } from "../../../../api/actionService/api-service";
import { IUser } from "../../interfaces";


// const QRCode = require("qrcode");

export const signInUrlSegment = "sign_in";

const signInLyfecyle = apiService.postLifecycle(signInUrlSegment);

import { message } from "antd";
// import { showGeneratedQrcodeAtferRegister } from "../RegisterModal/actions";
// import { showVerifyAfterSignIn } from "./actions";
import { setVerifiedUser } from "../RegisterModal/containers/VerifyTokenForm/actions";


function* watchSignIn() {
    while (true) {
        const { payload: signInModel } = yield take(SIGN_IN);

        yield put(apiService.post(signInUrlSegment, signInModel));

        const { signInSuccessFromApi, signInErrorFromApi } = yield race({
            signInSuccessFromApi: take(signInLyfecyle.RESOLVED),
            signInErrorFromApi: take(signInLyfecyle.REJECTED),
        });

        const name = "poker-statistic-auth=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(";");
        let cookie;
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === " ") {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                cookie = c.substring(name.length, c.length);
            }
        }

        if (cookie === undefined || cookie === null || cookie === "" || cookie === "undefined" || cookie === "null") {
            if (signInSuccessFromApi) {
                cookie = signInSuccessFromApi.payload.cookie;
            }
            if (signInErrorFromApi) {
                cookie = signInErrorFromApi.error.cookie;
            }
        }
        localStorage.setItem("poker-statistic-auth", cookie);

        if (signInErrorFromApi) {
            message.error(signInErrorFromApi.error.message);
            continue;
        }


        // const { user, otpauth_url }: { user: IUser, otpauth_url: string } = signInSuccessFromApi.payload;


        const { user }: { user: IUser, otpauth_url: string } = signInSuccessFromApi.payload;
        yield put(setVerifiedUser(user));

        // if (user.is_registered) {
        //     yield put(showVerifyAfterSignIn(user));
        // } else {
        //     try {
        //         const image_data = yield QRCode.toDataURL(otpauth_url);
        //         yield put(showGeneratedQrcodeAtferRegister(image_data, user));
        //     } catch (err) {
        //         console.log(err);
        //     }
        // }


        // try {
        //     const image_data = yield QRCode.toDataURL(otpauth_url);
        //     yield put(showGeneratedQrcodeAtferRegister(image_data, user));
        // } catch (err) {
        //     console.log(err);
        // }
    }
}

export default function* signInSaga(): IterableIterator<any> {
    yield fork(watchSignIn);
}
