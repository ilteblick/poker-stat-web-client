import * as React from "react";

import { Input, Form, Button } from "antd";
import { FormComponentProps } from "antd/lib/form";
import { RegisterModel } from "./interfaces";
const FormItem = Form.Item;


const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

interface RegisterFormProps extends FormComponentProps {
    onRegister: Function;
}

interface RegisterFormState {
    confirmDirty: boolean;
}

class RegisterForm extends React.PureComponent<RegisterFormProps, RegisterFormState> {
    constructor(props: RegisterFormProps) {
        super(props);

        this.state = {
            confirmDirty: false,
        };
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err: Error, values: any) => {
            if (!err) {
                const registerModel: RegisterModel = {
                    email: values.email,
                    confirm: values.confirm,
                    password: values.password,
                    token: values.invitionCode,
                };
                this.props.onRegister(registerModel);
            }
        });
    }

    compareToFirstPassword = (rule: any, value: any, callback: any) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue("password")) {
            callback("Two passwords that you enter is inconsistent!");
        } else {
            callback();
        }
    }
    validateToNextPassword = (rule: any, value: any, callback: any) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(["confirm"], { force: true }, () => console.log("error"));
        }
        callback();
    }

    handleConfirmBlur = (e: any) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem
                    {...formItemLayout}
                    label="E-mail">
                    {getFieldDecorator("email", {
                        rules: [{
                            type: "email", message: "The input is not valid E-mail!",
                        }, {
                            required: true, message: "Please input your E-mail!",
                        }],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="Password"
                >
                    {getFieldDecorator("password", {
                        rules: [{
                            required: true, message: "Please input your password!",
                        }, {
                            validator: this.validateToNextPassword,
                        }],
                    })(
                        <Input type="password" />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="Confirm Password"
                >
                    {getFieldDecorator("confirm", {
                        rules: [{
                            required: true, message: "Please confirm your password!",
                        }, {
                            validator: this.compareToFirstPassword,
                        }],
                    })(
                        <Input type="password" onBlur={this.handleConfirmBlur} />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="Invition code"
                >
                    {getFieldDecorator("invitionCode", {
                        rules: [{
                            required: true, message: "Please input your invition code!",
                        }],
                    })(
                        <Input type="invitionCode" />
                    )}
                </FormItem>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Register</Button>
                </FormItem>
            </Form>
        );
    }
}

const WrappedRegistrationForm = Form.create()(RegisterForm);
export default WrappedRegistrationForm;

