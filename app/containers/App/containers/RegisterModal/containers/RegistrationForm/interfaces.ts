export interface RegisterModel {
    email: string;
    password: string;
    confirm: string;
    token: string;

}
