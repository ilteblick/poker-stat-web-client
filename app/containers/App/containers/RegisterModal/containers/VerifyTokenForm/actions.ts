import { Action } from "../../../../../../basicClasses";
import { CHANGE_VERIFY_INPUT, VERIFY_TOKEN, SET_VERIFIED_USER } from "./constants";
import { IUser } from "../../../../interfaces";

export function changeVerifyInput(value: string): Action {
    return {
        type: CHANGE_VERIFY_INPUT,
        payload: value,
    };
}

export function verifyToken(token: string): Action {
    return {
        type: VERIFY_TOKEN,
        payload: token,
    };
}

export function setVerifiedUser(user: IUser): Action {
    return {
        type: SET_VERIFIED_USER,
        payload: user,
    };
}
