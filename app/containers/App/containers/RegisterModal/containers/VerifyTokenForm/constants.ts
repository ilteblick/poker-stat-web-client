export const CHANGE_VERIFY_INPUT = "app/App/CHANGE_VERIFY_INPUT";

export const VERIFY_TOKEN = "app/App/VERIFY_TOKEN";
export const SET_VERIFIED_USER = "app/App/SET_VERIFIED_USER";
