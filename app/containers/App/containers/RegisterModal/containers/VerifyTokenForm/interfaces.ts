export interface IVerifyToken {
    token: string;
    email: string;
}
