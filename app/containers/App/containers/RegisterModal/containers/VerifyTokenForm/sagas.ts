import { take, put, race, select } from "redux-saga/effects";
import { message } from "antd";

import { VERIFY_TOKEN } from "../VerifyTokenForm/constants";
import { apiService } from "../../../../../../api/actionService/api-service";
import { IVerifyToken } from "./interfaces";
import { selectUser } from "../../../../selectors";
import { setVerifiedUser } from "./actions";

const verifyTokenUrlSegment = `verify_token`;

const verifyTokenLyfecyle = apiService.postLifecycle(verifyTokenUrlSegment);

export function* watchVerifyQRCode() {
    while (true) {
        const { payload: token } = yield take(VERIFY_TOKEN);

        const user = yield select(selectUser());

        const body: IVerifyToken = {
            token,
            email: user.email,
        };

        yield put(apiService.post(verifyTokenUrlSegment, body));

        const { verifySuccessFromApi, verifyErrorFromApi } = yield race({
            verifySuccessFromApi: take(verifyTokenLyfecyle.RESOLVED),
            verifyErrorFromApi: take(verifyTokenLyfecyle.REJECTED),
        });

        if (verifyErrorFromApi) {
            message.error(verifyErrorFromApi.error.message);
            continue;
        }

        message.success("Success verify");
        yield put(setVerifiedUser(verifySuccessFromApi.payload));
    }
}
