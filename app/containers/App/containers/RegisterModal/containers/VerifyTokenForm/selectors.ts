import { createSelector } from "reselect";

import { selectRegisterModal } from "../../selectors";

const selectVerifyQrcodeInputValue = () => createSelector(
    selectRegisterModal(),
    (registerModal) => {
        return registerModal.get("verifyQrcodeInputValue");
    }
);

export {
    selectVerifyQrcodeInputValue,
};
