import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Button, Input } from "antd";
import { changeVerifyInput, verifyToken } from "./actions";
import { selectVerifyQrcodeInputValue } from "./selectors";

export interface QRCodeProps {
    value?: string;
    onChangeVerifyInput?: Function;
    onVerifyToken?: Function;
}

class QRCode extends React.Component<QRCodeProps, {}> {
    onChangeVerifyInputType = (e: any) => {
        const { value } = e.target;
        const reg = /^(\d+)$/;
        if ((!isNaN(value) && reg.test(value)) || value === "") {
            this.props.onChangeVerifyInput(value);
        }
    }

    onVerifyTokenClick = () => {
        const { value, onVerifyToken } = this.props;
        onVerifyToken(value);
    }

    render() {
        const { value } = this.props;
        return (
            <div>
                <Input
                    value={value}
                    onChange={this.onChangeVerifyInputType}
                    placeholder="Input a code"
                    maxLength={6}
                />
                <Button style={{ marginTop: "12px" }} onClick={this.onVerifyTokenClick}>Next</Button>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    value: selectVerifyQrcodeInputValue(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onChangeVerifyInput: (value: string) => dispatch(changeVerifyInput(value)),
        onVerifyToken: (key: string) => dispatch(verifyToken(key)),
    };
}

const withConnect = connect<{}, {}, QRCodeProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(QRCode);
