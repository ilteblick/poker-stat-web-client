import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Button } from "antd";
import { selectQRCodeImage } from "./selectors";
import { qrcodeScanned } from "./actions";

export interface QRCodeProps {
    qrcodeImage?: any;
    onQrcodeScanned?: Function;
}

class QRCode extends React.Component<QRCodeProps, {}> {
    onQrcodeScannedClick = () => {
        this.props.onQrcodeScanned();
    }
    render() {
        const { qrcodeImage } = this.props;
        return (
            <div>
                <img src={qrcodeImage} />
                <Button onClick={this.onQrcodeScannedClick}>Next</Button>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    qrcodeImage: selectQRCodeImage(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onQrcodeScanned: () => dispatch(qrcodeScanned()),
    };
}

const withConnect = connect<{}, {}, QRCodeProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(QRCode);
