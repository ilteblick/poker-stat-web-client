import { QRCODE_SCANNED } from "./constants";
import { Action } from "../../../../../../basicClasses";

export function qrcodeScanned(): Action {
    return {
        type: QRCODE_SCANNED,
    };
}
