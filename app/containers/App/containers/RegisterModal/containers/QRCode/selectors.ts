import { createSelector } from "reselect";

import { selectRegisterModal } from "../../selectors";

const selectQRCodeImage = () => createSelector(
    selectRegisterModal(),
    (registerModal) => {
        return registerModal.get("qrcodeImage");
    }
);

export {
    selectQRCodeImage,
};
