import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Modal, Button, Steps } from "antd";
const Step = Steps.Step;

import RegistrationForm from "./containers/RegistrationForm";
import QRCode from "./containers/QRCode";
import VerifyTokenForm from "./containers/VerifyTokenForm";

import { selectNeedToShowRegisterModal, selectCurrentRegisterStep, selectRegisterStepLabels } from "./selectors";
import { closeRegisterModal, register } from "./actions";
import { RegisterModel } from "./containers/RegistrationForm/interfaces";
import { List } from "immutable";
import { IRegisterStep } from "./interfaces";

export interface RegisterModalProps {
    needToShowRegisterModal?: boolean;
    onCloseRegisterModal?: Function;
    onRegister?: Function;

    currentRegisterStep?: number;
    registerStepLabels?: List<IRegisterStep>;
}

class RegisterModal extends React.PureComponent<RegisterModalProps, {}> {
    onCloseRegisterModalClick = () => {
        this.props.onCloseRegisterModal();
    }

    onRegisterClick = (registerModel: RegisterModel) => {
        this.props.onRegister(registerModel);
    }

    render() {
        const { needToShowRegisterModal, currentRegisterStep, registerStepLabels } = this.props;

        const registerSteps = (
            <Steps current={currentRegisterStep} style={{ paddingBottom: 12 }}>
                {registerStepLabels.map((step: IRegisterStep) => <Step key={`reg-step-${step.id}`} title={step.title} />)}
            </Steps>
        );

        let content = null;
        switch (currentRegisterStep) {
            case 0: {
                content = <RegistrationForm onRegister={this.onRegisterClick} />;
                break;
            }
            case 1: {
                content = <QRCode />;
                break;
            }
            case 2: {
                content = <VerifyTokenForm />;
                break;
            }
        }

        return (
            <Modal
                title="Register"
                visible={needToShowRegisterModal}
                onCancel={this.onCloseRegisterModalClick}
                footer={[
                    <Button key="back" onClick={this.onCloseRegisterModalClick}>Cancel</Button>,
                ]}>
                {registerSteps}
                {content}
            </Modal>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    needToShowRegisterModal: selectNeedToShowRegisterModal(),
    currentRegisterStep: selectCurrentRegisterStep(),
    registerStepLabels: selectRegisterStepLabels(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onCloseRegisterModal: () => dispatch(closeRegisterModal()),
        onRegister: (registerModel: RegisterModel) => dispatch(register(registerModel)),
    };
}

const withConnect = connect<{}, {}, RegisterModalProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(RegisterModal);
