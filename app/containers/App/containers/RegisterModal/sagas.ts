import { fork, take, put, race } from "redux-saga/effects";
import { REGISTER } from "./constants";
import { apiService } from "../../../../api/actionService/api-service";
import { closeRegModal } from "./actions";
import { watchVerifyQRCode } from "./containers/VerifyTokenForm/sagas";
import { IUser } from "../../interfaces";


// const QRCode = require("qrcode");

export const registerUrlSegment = "register";

const registerLyfecyle = apiService.postLifecycle(registerUrlSegment);

import { message } from "antd";


function* watchRegister() {
    while (true) {
        const { payload: registerModel } = yield take(REGISTER);

        yield put(apiService.post(registerUrlSegment, registerModel));

        const { registerSuccessFromApi, registerErrorFromApi } = yield race({
            registerSuccessFromApi: take(registerLyfecyle.RESOLVED),
            registerErrorFromApi: take(registerLyfecyle.REJECTED),
        });

        if (registerErrorFromApi) {
            message.error(registerErrorFromApi.error.message);
            continue;
        }

        const { }: { user: IUser, otpauth_url: string } = registerSuccessFromApi.payload;

        message.success(`Registration was completed successfully. Please check the email "spam"`);

        yield put(closeRegModal());
        // try {
        //     const image_data = yield QRCode.toDataURL(otpauth_url);
        //     yield put(showGeneratedQrcodeAtferRegister(image_data, user));
        // } catch (err) {
        //     console.log(err);
        // }
    }
}

export default function* resisterSaga(): IterableIterator<any> {
    yield fork(watchRegister);
    yield fork(watchVerifyQRCode);
}
