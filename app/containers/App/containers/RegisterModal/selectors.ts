import { createSelector } from "reselect";

import { selectGlobal } from "../../selectors";

const selectRegisterModal = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("registerModal")
);

const selectNeedToShowRegisterModal = () => createSelector(
    selectRegisterModal(),
    (registerModal) => {
        return registerModal.get("needToShowRegisterModal");
    }
);

const selectCurrentRegisterStep = () => createSelector(
    selectRegisterModal(),
    (registerModal) => {
        return registerModal.get("currentRegisterStep");
    }
);

const selectRegisterStepLabels = () => createSelector(
    selectRegisterModal(),
    (registerModal) => {
        return registerModal.get("registerStepsLabels");
    }
);

export {
    selectRegisterModal,
    selectNeedToShowRegisterModal,
    selectCurrentRegisterStep,
    selectRegisterStepLabels,
};
