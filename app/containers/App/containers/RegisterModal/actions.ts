import { CLOSE_REGISTER_MODAL, REGISTER, SHOW_GENERATED_QRCODE_AFTER_REGISTER, CLOSE_REG_MODAL } from "./constants";
import { RegisterModel } from "./containers/RegistrationForm/interfaces";
import { Action } from "../../../../basicClasses";
import { IUser } from "../../interfaces";

export function closeRegisterModal(): Action {
    return {
        type: CLOSE_REGISTER_MODAL,
    };
}

export function register(registerModel: RegisterModel): Action {
    return {
        type: REGISTER,
        payload: registerModel,
    };
}

export function showGeneratedQrcodeAtferRegister(qrCode: string, user: IUser): Action {
    return {
        type: SHOW_GENERATED_QRCODE_AFTER_REGISTER,
        payload: {
            qrCode,
            user
        }
    };
}

export function closeRegModal(): Action {
    return {
        type: CLOSE_REG_MODAL,
    };
}
