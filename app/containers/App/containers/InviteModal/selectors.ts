import { createSelector } from "reselect";

import { selectGlobal } from "../../selectors";

const selectInviteModal = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("inviteModal")
);

const selectNeedToShowInviteModal = () => createSelector(
    selectInviteModal(),
    (globalState) => globalState.get("needToShowInviteModal")
);

const selectInvitionCode = () => createSelector(
    selectInviteModal(),
    (globalState) => globalState.get("invitionCode")
);

export {
    selectNeedToShowInviteModal,
    selectInvitionCode,
};
