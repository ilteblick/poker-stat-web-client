export const CLOSE_INVITE_MODAL = "app/InviteModal/CLOSE_INVITE_MODAL";
export const GET_INVITE_CODE = "app/InviteModal/GET_INVITE_CODE";

export const SET_INVITION_CODE = "app/InviteModal/SET_INVITION_CODE";
