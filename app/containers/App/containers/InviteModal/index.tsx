import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Modal, Button, Input } from "antd";
import { closeInviteModal, getInviteCode } from "./actions";
import { selectNeedToShowInviteModal, selectInvitionCode } from "./selectors";


export interface SignInModalProps {
    needToShowInviteModal?: boolean;
    invitionCode?: string;

    onCloseInviteModal?: Function;
    onGetInviteCode?: Function;
}

class SignInModal extends React.PureComponent<SignInModalProps, {}> {

    onCloseInviteModalClick = () => {
        this.props.onCloseInviteModal();
    }

    onGetInviteCodeClick = () => {
        this.props.onGetInviteCode();
    }

    render() {
        const { needToShowInviteModal, invitionCode } = this.props;
        return (
            <Modal
                title="Invite"
                visible={needToShowInviteModal}
                onCancel={this.onCloseInviteModalClick}
                footer={[
                    <Button key="back" onClick={this.onCloseInviteModalClick}>Cancel</Button>,
                ]}>
                <Button size="large" onClick={this.onGetInviteCodeClick}>Get invite code</Button>
                {invitionCode ? <Input style={{ marginTop: "12px" }} value={invitionCode} /> : null}
            </Modal>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    needToShowInviteModal: selectNeedToShowInviteModal(),
    invitionCode: selectInvitionCode(),
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onCloseInviteModal: () => dispatch(closeInviteModal()),
        onGetInviteCode: () => dispatch(getInviteCode()),
    };
}

const withConnect = connect<{}, {}, SignInModalProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(SignInModal);
