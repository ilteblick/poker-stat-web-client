import { Action } from "../../../../basicClasses";
import { CLOSE_INVITE_MODAL, GET_INVITE_CODE, SET_INVITION_CODE } from "./constants";

export function closeInviteModal(): Action {
    return {
        type: CLOSE_INVITE_MODAL,
    };
}

export function getInviteCode(): Action {
    return {
        type: GET_INVITE_CODE,
    };
}

export function setInvitionCode(token: string): Action {
    return {
        type: SET_INVITION_CODE,
        payload: token,
    };
}
