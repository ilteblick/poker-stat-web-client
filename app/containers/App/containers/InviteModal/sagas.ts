import { fork, take, put, race, select } from "redux-saga/effects";
import { apiService } from "../../../../api/actionService/api-service";

export const inviteCodeUrlSegment = "invite_code";

const getInviteCodeLyfecyle = apiService.getLifecycle(inviteCodeUrlSegment);

import { message } from "antd";
import { GET_INVITE_CODE } from "./constants";
import { selectUser } from "../../selectors";
import { setInvitionCode } from "./actions";


function* watchInvite() {
    while (true) {
        yield take(GET_INVITE_CODE);

        const user = yield select(selectUser());

        yield put(apiService.get(inviteCodeUrlSegment, user.id));

        const { inviteCodeSuccessFromApi, inviteCodeErrorFromApi } = yield race({
            inviteCodeSuccessFromApi: take(getInviteCodeLyfecyle.RESOLVED),
            inviteCodeErrorFromApi: take(getInviteCodeLyfecyle.REJECTED),
        });

        if (inviteCodeErrorFromApi) {
            message.error(inviteCodeErrorFromApi.error.message);
            continue;
        }

        yield put(setInvitionCode(inviteCodeSuccessFromApi.payload.token));
    }
}

export default function* inviteSaga(): IterableIterator<any> {
    yield fork(watchInvite);
}
