import { createSelector } from "reselect";

const selectGlobal = () => (state: any) => {
    return state.get("global");
};

const selectInitialLoad = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("initialLoad")
);

const selectApiCallSpinner = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("apiCallSpinner")
);

const selectUser = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("user")
);

const selectIsUserVerified = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("isUserVerified")
);

const selectCurrentPage = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("currentPage")
);

const selectAutopast = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("autopast")
);

const selectPortableMode = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("portableMode")
);

const selectTheme = () => createSelector(
    selectGlobal(),
    (globalState) => globalState.get("appTheme")
);

export {
    selectGlobal,
    selectInitialLoad,
    selectApiCallSpinner,
    selectUser,
    selectIsUserVerified,
    selectCurrentPage,
    selectAutopast,
    selectPortableMode,
    selectTheme,
};
