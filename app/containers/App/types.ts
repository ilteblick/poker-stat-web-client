import { StyledThemeProps } from "../../utils/styleThemes";

export interface StyledLayoutProps extends StyledThemeProps {
    isportable: boolean;
}

export interface StyledContentProps extends StyledThemeProps {
    isportable: boolean;
}

