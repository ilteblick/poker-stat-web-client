import { fromJS } from "immutable";

import { Action } from "../../basicClasses";

import {
    OPEN_SIGN_IN_MODAL,
    OPEN_REGISTER_MODAL,
    LOGOUT_SUCCES,
    OPEN_INVITE_MODAL,
    HOME_PAGE,
    SHOW_PAGE,
    SHOW_API_CALL_SPINNER,
    HIDE_API_CALL_SPINNER,
    INITIAL_LOAD,
    SET_AUTOPAST,
    SET_PORTABLE_MODE,
    STAT_PAGE,
    SET_THEME,
} from "./constants";
import { CLOSE_REGISTER_MODAL, SHOW_GENERATED_QRCODE_AFTER_REGISTER, CLOSE_REG_MODAL } from "./containers/RegisterModal/constants";
import { registerStepsLabels, checkPortableMode } from "./utils";
import { QRCODE_SCANNED } from "./containers/RegisterModal/containers/QRCode/constants";
import { CHANGE_VERIFY_INPUT, SET_VERIFIED_USER } from "./containers/RegisterModal/containers/VerifyTokenForm/constants";
import { CLOSE_SIGN_IN_MODAL, SHOW_VERIFY_AFTER_SIGN_IN } from "./containers/SignInModal/constants";
import { IUser } from "./interfaces";
import { CLOSE_INVITE_MODAL, SET_INVITION_CODE } from "./containers/InviteModal/constants";

const initialState = fromJS({
    initialLoad: false,
    apiCallSpinner: false,

    user: undefined,
    isUserVerified: false,

    currentPage: HOME_PAGE,

    signInModal: fromJS({
        currentSignInStep: 0,

        needToShowSignInModal: false,
    }),

    inviteModal: fromJS({
        needToShowInviteModal: false,
        invitionCode: undefined,
    }),


    registerModal: fromJS({
        currentRegisterStep: 0,
        registerStepsLabels: registerStepsLabels,
        qrcodeImage: undefined,

        verifyQrcodeInputValue: "",

        needToShowRegisterModal: false,
    }),

    pageNumber: 0,
    autopast: false,
    portableMode: localStorage.getItem("portableMode") === "true",
    appTheme: localStorage.getItem("appTheme") || "dark",
});

const realPath = {
    needToShowSignInModal: ["signInModal", "needToShowSignInModal"],
    currentSignInStep: ["signInModal", "currentSignInStep"],

    needToShowRegisterModal: ["registerModal", "needToShowRegisterModal"],
    currentRegisterStep: ["registerModal", "currentRegisterStep"],
    registerStepsLabels: ["registerModal", "registerStepsLabels"],
    qrcodeImage: ["registerModal", "qrcodeImage"],
    verifyQrcodeInputValue: ["registerModal", "verifyQrcodeInputValue"],

    needToShowInviteModal: ["inviteModal", "needToShowInviteModal"],
    invitionCode: ["inviteModal", "invitionCode"],
};

function appReducer(state = initialState, action: Action) {
    switch (action.type) {
        case INITIAL_LOAD:
            return state.set("initialLoad", action.payload);
        case SET_AUTOPAST:
            return state.set("autopast", action.payload);
        case SET_PORTABLE_MODE: {
            localStorage.setItem("portableMode", action.payload);
            return state.set("portableMode", action.payload);
        }
        case SET_THEME: {
            localStorage.setItem("appTheme", action.payload);
            return state.set("appTheme", action.payload);
        }
        case SHOW_API_CALL_SPINNER:
            return state.set("apiCallSpinner", true);
        case HIDE_API_CALL_SPINNER:
            return state.set("apiCallSpinner", false);
        case OPEN_SIGN_IN_MODAL: {
            return openSignInModal(state);
        }
        case CLOSE_SIGN_IN_MODAL: {
            return closeSignInModal(state);
        }
        case SHOW_VERIFY_AFTER_SIGN_IN: {
            return showVerifyAfterSignIn(state, action);
        }
        case OPEN_REGISTER_MODAL: {
            return openRegisterModal(state);
        }
        case CLOSE_REGISTER_MODAL: {
            return closeRegisterModal(state);
        }
        case SHOW_GENERATED_QRCODE_AFTER_REGISTER: {
            return showGeneratedQRCode(state, action);
        }
        case QRCODE_SCANNED:
            return qrcodeScanned(state);
        case CHANGE_VERIFY_INPUT:
            return changeVerifyInput(state, action);
        case SET_VERIFIED_USER: {
            return setVerifiedUser(state, action);
        }
        case LOGOUT_SUCCES:
            return logoutSucces(state);

        case OPEN_INVITE_MODAL:
            return openInviteModal(state);
        case CLOSE_INVITE_MODAL:
            return closeInviteModal(state);
        case SET_INVITION_CODE:
            return setInvitionCode(state, action);
        case SHOW_PAGE:
            return state.set("currentPage", action.payload);
        case CLOSE_REG_MODAL: {
            return state.setIn(realPath.needToShowRegisterModal, false);
        }
        default:
            return state;
    }
}

function setVerifiedUser(state: any, action: Action) {

    state = state
        .set("isUserVerified", true)
        .setIn(realPath.needToShowSignInModal, false)
        .setIn(realPath.currentSignInStep, 0)
        .setIn(realPath.verifyQrcodeInputValue, "")
        .setIn(realPath.needToShowRegisterModal, false);
    return setUser(state, action.payload);
}

function openSignInModal(state: any) {
    return state.setIn(realPath.needToShowSignInModal, true);
}

function closeSignInModal(state: any) {
    return state.setIn(realPath.needToShowSignInModal, false);
}

function showVerifyAfterSignIn(state: any, action: Action) {
    state = state.setIn(realPath.currentSignInStep, 1);
    return setUser(state, action.payload);
}

function openRegisterModal(state: any) {
    return state.setIn(realPath.needToShowRegisterModal, true);
}

function closeRegisterModal(state: any) {
    return state.setIn(realPath.needToShowRegisterModal, false);
}

function showGeneratedQRCode(state: any, action: Action) {
    const {qrCode, user} = action.payload;
    state = state
        .setIn(realPath.needToShowSignInModal, false)
        .setIn(realPath.needToShowRegisterModal, true)
        .setIn(realPath.qrcodeImage, qrCode)
        .setIn(realPath.currentRegisterStep, 1);

    return setUser(state, user);
}

function qrcodeScanned(state: any) {
    return state.setIn(realPath.currentRegisterStep, 2);
}

function changeVerifyInput(state: any, action: Action) {
    return state.setIn(realPath.verifyQrcodeInputValue, action.payload);
}

function setUser(state: any, user: IUser) {
    return state.set("user", user);
}

function logoutSucces(state: any) {
    return state.set("user", undefined)
        .set("isUserVerified", false)
        .set("currentPage", HOME_PAGE);
}

function openInviteModal(state: any) {
    return state.setIn(realPath.needToShowInviteModal, true);
}

function closeInviteModal(state: any) {
    return state.setIn(realPath.needToShowInviteModal, false);
}

function setInvitionCode(state: any, action: Action) {
    return state.setIn(realPath.invitionCode, action.payload);
}

export default appReducer;
