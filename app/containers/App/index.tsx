import * as React from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { ThemeProvider } from "styled-components";
import { Spin, Modal } from "antd";

import HomePage from "../HomePage";
import StatPage from "../StatPage";
import UserProfilePage from "../UserProfilePage";
import AdminPage from "../AdminPage";
import PricePage from "../PricePage";
import ResetPassPage from "../ResetPass";
import ConfirmPage from "../ConfirmPage";
import AdminUserInfoPage from "../AdminUserInfoPage";
import TopPlayersPage from "../TopPlayersPage";
import Header from "./containers/Header";
import ApiCallSpinner from "../../components/ApiCallSpinner";
import MenuButton from "./components/MenuButton";
import ZoomApp from "../../components/ZoomApp";
import RegisterPage from "../RegisterPage";
import InvitePage from "../InvitePage";

import {
    selectIsUserVerified,
    selectCurrentPage,
    selectApiCallSpinner,
    selectInitialLoad,
    selectPortableMode,
    selectTheme,
} from "./selectors";

import SignInModal from "./containers/SignInModal";
import RegisterModal from "./containers/RegisterModal";
import InviteModal from "./containers/InviteModal";

import ContactUsPage from "../ContactUs";
import { themesLight, themesDark, ThemeType } from "../../utils/styleThemes";
import { checkPortableMode } from "./utils";

import {
    StyledLayout,
    StyledContent,
    StyledFooter,
} from "./styled";

import { GlobalStyle } from "../../global-styles";

export interface AppProps {
    isUserVerified?: boolean;
    apiCallSpinner?: boolean;
    initialLoad?: boolean;
    portableMode: boolean;
    currentPage?: string;
    location?: any;
    appTheme?: ThemeType;
}




class App extends React.Component<AppProps, {}> {

    render() {
        const { currentPage, apiCallSpinner, initialLoad, portableMode, appTheme } = this.props;

        return (
            <ThemeProvider theme={(appTheme === "dark" || portableMode) ? themesDark : themesLight}>
                <React.Fragment>
                    <GlobalStyle theme={appTheme} />
                    <StyledLayout
                        isportable={checkPortableMode(currentPage, portableMode) ? true : undefined}
                    >
                        {!checkPortableMode(currentPage, portableMode) && apiCallSpinner ?
                            <ApiCallSpinner /> : null}
                        <SignInModal />
                        <RegisterModal />
                        <InviteModal />
                        <Header />
                        <StyledContent
                            isportable={checkPortableMode(currentPage, portableMode) ? true : undefined}
                        >
                            {initialLoad ? <Switch>
                                <Route exact path="/" component={HomePage} />
                                <Route exact path="/statistics" component={StatPage} />
                                <Route exact path="/top-players" component={TopPlayersPage} />
                                <Route exact path="/profile" component={UserProfilePage} />
                                <Route exact path="/admin" component={AdminPage} />
                                <Route exact path="/price" component={PricePage} />
                                <Route exact path="/contact" component={ContactUsPage} />
                                <Route exact path="/reset" component={ResetPassPage} />
                                <Route exact path="/confirm" component={ConfirmPage} />
                                <Route exact path="/user-info/:email" component={AdminUserInfoPage} />
                                <Route exact path="/register" component={RegisterPage} />
                                <Route exact path="/invite-code" component={InvitePage} />
                            </Switch> : null}
                            {!checkPortableMode(currentPage, portableMode) && <StyledFooter>
                                <span>Hand4hand © 2017-2018</span>
                                {/*<div>
                                <Tag
                                    color="#757575"
                                    closable>
                                    Dear customers! After registration during 24 hours you will recieve a trial period up to 10th of september.
                                     In case you didn't recieve a trial refresh the page (press F5) or logout.
                            </Tag>
                            </div>*/}
                            </StyledFooter>}
                        </StyledContent>
                    </StyledLayout>
                </React.Fragment>
            </ThemeProvider>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    isUserVerified: selectIsUserVerified(),
    currentPage: selectCurrentPage(),
    apiCallSpinner: selectApiCallSpinner(),
    initialLoad: selectInitialLoad(),
    portableMode: selectPortableMode(),
    appTheme: selectTheme(),
});

const withConnect = connect<{}, {}, AppProps>(mapStateToProps);

export default withRouter(compose(
    withConnect
)(App) as any);
