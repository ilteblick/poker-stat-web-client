export const INITIAL_LOAD = "app/App/INITIAL_LOAD";

export const SHOW_API_CALL_SPINNER = "app/App/SHOW_API_CALL_SPINNER";
export const HIDE_API_CALL_SPINNER = "app/App/HIDE_API_CALL_SPINNER";

export const LOGOUT = "app/App/LOGOUT";
export const LOGOUT_SUCCES = "app/App/LOGOUT_SUCCES";

export const OPEN_SIGN_IN_MODAL = "app/App/OPEN_SIGN_IN_MODAL";

export const OPEN_REGISTER_MODAL = "app/App/OPEN_REGISTER_MODAL";

export const COOKIE_SIGN_IN = "app/App/COOKIE_SIGN_IN";

export const OPEN_INVITE_MODAL = "app/App/OPEN_INVITE_MODAL";
export const SET_AUTOPAST = "app/App/SET_AUTOPAST";
export const SET_PORTABLE_MODE = "app/App/SET_PORTABLE_MODE";
export const SET_THEME = "app/App/SET_THEME";


export const HOME_PAGE = "HomePage";
export const STAT_PAGE = "StatPage";
export const PROFILE_PAGE = "ProfilePage";
export const PRICE_PAGE = "PricePage";
export const CONTACT_US = "ContactUsPage";
export const TOP_PLAYERS = "TopPlayersPage";
export const INVITE_CODE_PAGE = "InviteCodePage";

export const SHOW_PAGE = "app/App/SHOW_PAGE";
