import {
    OPEN_SIGN_IN_MODAL,
    OPEN_REGISTER_MODAL,
    COOKIE_SIGN_IN,
    LOGOUT,
    LOGOUT_SUCCES,
    OPEN_INVITE_MODAL,
    SHOW_PAGE,
    SHOW_API_CALL_SPINNER,
    HIDE_API_CALL_SPINNER,
    INITIAL_LOAD,
    SET_AUTOPAST,
    SET_PORTABLE_MODE,
    SET_THEME,
} from "./constants";
import { ThemeType } from "../../utils/styleThemes";

import { Action } from "../../basicClasses";

export function initialLoadProcessed(loaded: boolean) {
    return {
        type: INITIAL_LOAD,
        payload: loaded,
    };
}

export function setAutopast(state: boolean) {
    return {
        type: SET_AUTOPAST,
        payload: state,
    };
}

export function setPortableMode(state: boolean) {
    return {
        type: SET_PORTABLE_MODE,
        payload: state,
    };
}

export function setTheme(state: ThemeType) {
    return {
        type: SET_THEME,
        payload: state,
    };
}

export function showApiCallSpinner(): Action {
    return {
        type: SHOW_API_CALL_SPINNER,
    };
}

export function hideApiCallSpinner(): Action {
    return {
        type: HIDE_API_CALL_SPINNER,
    };
}

export function logout(): Action {
    return {
        type: LOGOUT,
    };
}

export function logoutSucces(): Action {
    return {
        type: LOGOUT_SUCCES,
    };
}

export function openSignInModal(): Action {
    return {
        type: OPEN_SIGN_IN_MODAL,
    };
}

export function openRegisterModal(): Action {
    return {
        type: OPEN_REGISTER_MODAL,
    };
}

export function cookieSignIn(): Action {
    return {
        type: COOKIE_SIGN_IN,
    };
}

export function openInviteModal(): Action {
    return {
        type: OPEN_INVITE_MODAL,
    };
}

export function showPage(key: string): Action {
    return {
        type: SHOW_PAGE,
        payload: key
    };
}
