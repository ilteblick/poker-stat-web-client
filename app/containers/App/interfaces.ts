export interface IUser {
    id: string;
    email: string;
    is_registered: boolean;
    is_admin: boolean;
    subscription_type: number;
    expiration: number;
}
