import { List } from "immutable";
import {STAT_PAGE} from "./constants";

export const registerStepsLabels = List([
    {
        id: 0,
        title: "Data",
    }
]);

export const checkPortableMode = (page: string, state?: boolean) => {
    return (state === "true" || localStorage.getItem("portableMode") === "true") && page === STAT_PAGE;
}
