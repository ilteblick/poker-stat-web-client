export interface ChangePasswordModel {
    oldPassword: string;
    password: string;
    confirm: string;
}
