import * as React from "react";

import { Form, Button } from "antd";
import { FormComponentProps } from "antd/lib/form";
import { ChangePasswordModel } from "./interfaces";

import Input from "../../../../components/Input";
import { requiredValidator } from "../../../../utils/validators";

import PasswordConfirm from "./icons/PasswordConfirm";
import PasswordNew from "./icons/PasswordNew";
import PasswordOld from "./icons/PasswordOld";


interface ChangePasswordFormProps extends FormComponentProps {
    changePassword: Function;
}

interface ChangePasswordFormState {
    confirmDirty: boolean;

    old: string;
    pass: string;
    confirm: string;
}

class ChangePasswordForm extends React.PureComponent<ChangePasswordFormProps, ChangePasswordFormState> {
    constructor(props: ChangePasswordFormProps) {
        super(props);

        this.state = {
            confirmDirty: false,
            old: "",
            pass: "",
            confirm: "",
        };
    }

    compareToFirstPassword = (rule: any, value: any, callback: any) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue("password")) {
            callback("Two passwords that you enter is inconsistent!");
        } else {
            callback();
        }
    }
    validateToNextPassword = (rule: any, value: any, callback: any) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(["confirm"], { force: true }, () => console.log("error"));
        }
        callback();
    }

    onChangeOld = (value: string) => {
        this.setState({
            old: value,
        });
    }

    onChangePass = (value: string) => {
        this.setState({
            pass: value,
        });
    }

    onChangeConfirm = (value: string) => {
        this.setState({
            confirm: value,
        });
    }

    handleSubmit = () => {
        const { confirm, old, pass } = this.state;
        if (requiredValidator(old).ok &&
            requiredValidator(pass).ok &&
            requiredValidator(confirm).ok &&
            pass === confirm) {
            const changePasswordModel: ChangePasswordModel = {
                oldPassword: old,
                confirm: confirm,
                password: pass,
            };
            this.props.changePassword(changePasswordModel);
        }

    }

    render() {
        const { old, pass, confirm } = this.state;

        return (
            <div style={{ display: "flex", flexDirection: "column" }}>
                <Input
                    value={old}
                    onChange={this.onChangeOld}
                    label="Old password"
                    isPass
                    finalValidate={requiredValidator}
                    customSuffix={PasswordOld} />
                <Input
                    value={pass}
                    onChange={this.onChangePass}
                    label="New password"
                    isPass
                    finalValidate={requiredValidator}
                    customSuffix={PasswordNew} />
                <Input value={confirm}
                    onChange={this.onChangeConfirm}
                    label="Confirm password"
                    isPass
                    finalValidate={requiredValidator}
                    customSuffix={PasswordConfirm} />
                <Button
                    size="large"
                    onClick={this.handleSubmit}
                    type="primary"
                    style={{
                        width: "240px", height: "60px",
                        fontSize: "20px", borderRadius: "6px",
                        marginTop: 12, alignSelf: "flex-end"
                    }}>
                    Change password
                </Button>
            </div>
        );
    }
}

const WrappedRegistrationForm = Form.create()(ChangePasswordForm);
export default WrappedRegistrationForm;

