import { ActionsUnion, createAction } from "../../utilsTypes/redux/actionUtils";

import { ChangePasswordModel } from "./containers/ChangePasswordForm/interfaces";

export const CHANGE_PASSWORD = "App/UserProfilePage/CHANGE_PASSWORD";

export const Actions = {
    changePassword: (changePasswordModel: ChangePasswordModel) =>
        createAction(CHANGE_PASSWORD, changePasswordModel),
};

export type Actions = ActionsUnion<typeof Actions>;
