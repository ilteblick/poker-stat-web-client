import { fork, take, put, race, select } from "redux-saga/effects";
import { message } from "antd";
import * as Actions from "./actions";
import { apiService } from "../../api/actionService/api-service";
import { selectUser } from "../App/selectors";
import { logoutSucces } from "../App/actions";

const changePasswordUrlSegment = "change_password";
const changePasswordLyfecycleLyfecycle = apiService.postLifecycle(changePasswordUrlSegment);

function* watchChangePassword() {
    while (true) {
        const { payload: changePasswordModel } = yield take(Actions.CHANGE_PASSWORD);

        const user = yield select(selectUser());

        const body = {
            email: user.email,
            changePasswordModel,
        };

        yield put(apiService.post(changePasswordUrlSegment, body));

        const { changePasswordError } = yield race({
            changePasswordSucces: take(changePasswordLyfecycleLyfecycle.RESOLVED),
            changePasswordError: take(changePasswordLyfecycleLyfecycle.REJECTED),
        });

        if (changePasswordError) {
            message.error(changePasswordError.error.message);
            continue;
        }

        message.success("Password changing succes. Sign in now.");
        yield put(logoutSucces());
    }
}

export default function* userProfileSaga(): IterableIterator<any> {
    yield fork(watchChangePassword);
}
