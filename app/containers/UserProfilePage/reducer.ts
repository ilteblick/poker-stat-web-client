import { fromJS } from "immutable";

import { Action } from "../../basicClasses";

const initialState = fromJS({

});

function userProfileReducer(state = initialState, action: Action) {
    switch (action.type) {
        default:
            return state;
    }
}

export default userProfileReducer;
