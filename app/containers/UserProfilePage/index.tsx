import * as React from "react";
import { connect } from "react-redux";
import { compose, Dispatch } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";
import { selectUser } from "../App/selectors";

import reducer from "./reducer";
import saga from "./sagas";
import { PROFILE_PAGE } from "../App/constants";

import ChangePasswordForm from "./containers/ChangePasswordForm";
import { Actions } from "./actions";
import { ChangePasswordModel } from "./containers/ChangePasswordForm/interfaces";
import authorize from "../../utils/authorize";

import { StyledTitle, StyledLabel, StyledContainer, CenteredContainer } from "../../components/FormsItems";

import { Props, State, StateProps, DispatchProps, OwnPageProps } from "./types";

class UserProfilePage extends React.PureComponent<Props, State> {

    onChangePasswordClick = (changePasswordModel: ChangePasswordModel) => {
        this.props.onChangePassword(changePasswordModel);
    }

    render() {
        const { user } = this.props;
        if (!user) {
            return null;
        }
        return (
            <CenteredContainer>
                <StyledContainer>
                    <StyledTitle>Reset password</StyledTitle>
                    <StyledLabel>{user.email}</StyledLabel>
                    <ChangePasswordForm changePassword={this.onChangePasswordClick} />
                </StyledContainer>
            </CenteredContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector<any, StateProps>({
    user: selectUser(),
});

function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
    return {
        onChangePassword: (changePasswordModel: ChangePasswordModel) => dispatch(Actions.changePassword(changePasswordModel)),
        dispatch,
    };
}


const withConnect = connect<StateProps, DispatchProps, OwnPageProps>(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "profile", reducer });
const withSaga = injectSaga({ key: "profile", saga });

const withAuth = authorize({ pageKey: PROFILE_PAGE });

export default withRouter(compose(
    withAuth,
    withReducer,
    withSaga,
    withConnect,
)(UserProfilePage) as any);
