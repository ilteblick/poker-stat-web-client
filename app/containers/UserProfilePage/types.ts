import { BaseDispatchProps } from "../../types/redux/BaseDispatchProps";
import { BasePageStateProps, BasePageOwnProps } from "../../types/redux/BasePageProps";
import { IUser } from "../App/interfaces";
import { ChangePasswordModel } from "./containers/ChangePasswordForm/interfaces";

export interface OwnPageProps extends BasePageOwnProps {
}

export interface StateProps extends BasePageStateProps {
    user: IUser;
}

export interface DispatchProps extends BaseDispatchProps {
    onChangePassword: (changePasswordModel: ChangePasswordModel) => any;
}

export type Props = StateProps & DispatchProps & OwnPageProps;

export interface State {
}
