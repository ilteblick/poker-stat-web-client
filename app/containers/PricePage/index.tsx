import * as React from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import authorize from "../../utils/authorize";

import {
    StyledContainer,
    StyledPriceContainer, StyledPriceWrapper, StyledWrapper,
    StyledLimit, StyledProfile, StyledMonthContainer, StyledMonth, StyledGray, StyledBlack,
    StyledMonthWrapper, StyledPayContainer, StyledHowToPplay, StyledStep, StyledSkypeContainer,
    StyledSkypeImg, StyledSkypeLabel, StyledPaymentWrapper, StyledPayLabel, StyledLastWrapper,
    Horiz, Vert, HorizPrice, OnePriceBox, AllPriceBox
} from "./styled";
import { PRICE_PAGE } from "../App/constants";

const SkypeImg = require("../../../static/Skype_1.png");
const WMZ = require("../../../static/WMZ.png");
const Skrill = require("../../../static/Skrill.png");
const BTC = require("../../../static/Bitcoin.png");

interface PriceProps {
    onShowPage: Function;
}

class PricePage extends React.PureComponent<PriceProps, {}> {
    render() {
        return (
            <StyledContainer>
                <StyledPriceWrapper>
                    <StyledPriceContainer>
                        <div style={{ display: "flex", alignItems: "center" }}>
                            <StyledLimit>NL10-16</StyledLimit>
                        </div>
                        <Horiz />
                        <StyledProfile>Profiles up to NL16</StyledProfile>
                        <StyledMonthWrapper>
                            <StyledMonthContainer>
                                <StyledMonth>1 month</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $0</StyledGray>
                                        <StyledBlack>$7,5</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>100</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                            <HorizPrice />
                            <StyledMonthContainer>
                                <StyledMonth>3 months</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $2,5</StyledGray>
                                        <StyledBlack>$20</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>unlim</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                            <HorizPrice />
                            <StyledMonthContainer>
                                <StyledMonth>6 months</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $10</StyledGray>
                                        <StyledBlack>$35</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>unlim</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                        </StyledMonthWrapper>
                    </StyledPriceContainer>
                    <StyledPriceContainer>
                        <StyledLimit>NL10-50</StyledLimit>
                        <Horiz />
                        <StyledProfile>Profiles up to NL50</StyledProfile>
                        <StyledMonthWrapper>
                            <StyledMonthContainer>
                                <StyledMonth>1 month</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $0</StyledGray>
                                        <StyledBlack>$15</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>100</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                            <HorizPrice />
                            <StyledMonthContainer>
                                <StyledMonth>3 months</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $3</StyledGray>
                                        <StyledBlack>$42</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>unlim</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                            <HorizPrice />
                            <StyledMonthContainer>
                                <StyledMonth>6 months</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $15</StyledGray>
                                        <StyledBlack>$75</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>unlim</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                        </StyledMonthWrapper>
                    </StyledPriceContainer>
                    <StyledPriceContainer>
                        <StyledLimit>NL10-20K</StyledLimit>
                        <Horiz />
                        <StyledProfile>All profiles</StyledProfile>
                        <StyledMonthWrapper>
                            <StyledMonthContainer>
                                <StyledMonth>1 month</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $0</StyledGray>
                                        <StyledBlack>$25</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>100</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                            <HorizPrice />
                            <StyledMonthContainer>
                                <StyledMonth>3 months</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $5</StyledGray>
                                        <StyledBlack>$70</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>unlim</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                            <HorizPrice />
                            <StyledMonthContainer>
                                <StyledMonth>6 months</StyledMonth>
                                <AllPriceBox>
                                    <OnePriceBox>
                                        <StyledGray>discount $20</StyledGray>
                                        <StyledBlack>$130</StyledBlack>
                                    </OnePriceBox>
                                    <OnePriceBox>
                                        <StyledGray>Searches/day</StyledGray>
                                        <StyledBlack>unlim</StyledBlack>
                                    </OnePriceBox>
                                </AllPriceBox>
                            </StyledMonthContainer>
                        </StyledMonthWrapper>
                    </StyledPriceContainer>
                </StyledPriceWrapper>
                <StyledWrapper>
                    <StyledPaymentWrapper>
                        <StyledSkypeImg src={WMZ} />
                        <StyledPayLabel>Z401508785416</StyledPayLabel>
                    </StyledPaymentWrapper>
                    <Vert />
                    <StyledPaymentWrapper>
                        <StyledSkypeImg src={Skrill} />
                        <StyledPayLabel>Contact support</StyledPayLabel>
                    </StyledPaymentWrapper>
                    <Vert />
                    <StyledPaymentWrapper>
                        <StyledSkypeImg src={BTC} />
                        <StyledPayLabel>1M2pwXcy3zYsyTi2JpvP36MKRjvLPuHN5k</StyledPayLabel>
                    </StyledPaymentWrapper>
                </StyledWrapper>
                <StyledLastWrapper>
                    <StyledPayContainer>
                        <StyledHowToPplay>How to pay?</StyledHowToPplay>
                        <StyledStep>1. Select a payment method</StyledStep>
                        <StyledStep>2. Pay for a subscribtion (put your login into<br />the message)</StyledStep>
                        <StyledStep>3. During 24 hours you will get a subscription</StyledStep>
                        <StyledStep>4. In case of problems - contact us<br />on skype</StyledStep>
                        <StyledSkypeContainer
                            target="_blank"
                            href="skype:live:h4h_supp_eng?chat">
                            <StyledSkypeImg src={SkypeImg} />
                            <StyledSkypeLabel>Contact support</StyledSkypeLabel>
                        </StyledSkypeContainer>
                    </StyledPayContainer>
                    <StyledPayContainer>
                        <StyledHowToPplay>Как произвести оплату?</StyledHowToPplay>
                        <StyledStep>1. Выбрать способ оплаты (WMZ\SKRILL\bitcoin)</StyledStep>
                        <StyledStep>2. Произвести оплату, указав при отправке, в сообщении,
                             свой логин (ваш email для входа на сайт)</StyledStep>
                        <StyledStep>3. В течении суток у вас появится подписка</StyledStep>
                        <StyledStep>4. При возникновении трудностей - свяжитесь с нашей <br />службой</StyledStep>
                        <StyledSkypeContainer
                            target="_blank"
                            href="skype:live:h4h_supp_rus?chat">
                            <StyledSkypeImg src={SkypeImg} />
                            <StyledSkypeLabel>Contact support</StyledSkypeLabel>
                        </StyledSkypeContainer>
                    </StyledPayContainer>
                </StyledLastWrapper>
            </StyledContainer>
        );
    }
}

const withAuth = authorize({ pageKey: PRICE_PAGE });

export default withRouter(compose(
    withAuth,
)(PricePage) as any);
