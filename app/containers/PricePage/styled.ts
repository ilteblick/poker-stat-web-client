import styled from "styled-components";
import {StyledThemeProps} from "../../utils/styleThemes";

export const StyledContainer = styled<StyledThemeProps, "div">("div")`
    @media (max-width: 900px) {
        align-self: flex-start;
        width: 900px;
    }
    color: ${props => props.theme.color};
`;

export const StyledPriceWrapper = styled.div`
    width: 100%;
    display: flex;
    padding: 18px 0;
`;

export const StyledPriceContainer = styled<StyledThemeProps, "div">("div")`
    width: 33%;
    margin-right: 18px;
    &:last-child{
        margin-right: 0px;
    }
    display: flex;
    flex-direction: column;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 64px;
    padding: 18px 28px;
    border-radius: 4px;
    background: ${props => props.theme.contentBg};
`;

export const StyledLimit = styled.span`
    font-size: 26px;
    font-weight: bold;
    margin-right: 8px;
    line-height: 26px;
`;

export const StyledMonthWrapper = styled.div`
    margin-top: 12px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
`;
export const StyledMonthContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

export const StyledMonth = styled.span`
    font-size: 22px;
    font-weight: bold;
    margin-bottom: 4px;
`;

export const StyledGray = styled.span`
    font-size: 16px;
    opacity: .7;
`;

export const StyledBlack = styled.span`
    font-size: 16px;
`;

export const StyledProfile = styled.span`
    font-size: 18px;
`;

export const StyledWrapper = styled<StyledThemeProps, "div">("div")`
    width: 100%;
    display: flex;
    padding: 18px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 64px;
    border-radius: 4px;
    background: ${props => props.theme.contentBg};
`;

export const StyledLastWrapper = styled<StyledThemeProps, "div">("div")`
    width: 100%;
    display: flex;
    margin: 18px 0;
    padding: 18px 28px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 64px;
    border-radius: 4px;
    background: ${props => props.theme.contentBg};
`;

export const StyledPayContainer = styled.div`
    width: 50%;
    display: flex;
    flex-direction:column;
`;

export const StyledHowToPplay = styled.span`
    font-size: 24px;
    font-weight: bold;
    margin-bottom: 12px;
`;

export const StyledStep = styled.span`
    font-size: 16px;
    margin-bottom: 8px;
`;

export const StyledSkypeContainer = styled<StyledThemeProps, "a">("a")`
    padding: 12px 18px;
    border-radius: 4px;
    min-height: 60px;
    min-width: 280px;
    width: 280px;
    display: flex;
    align-items: center;
    cursor: pointer;
    margin-top: 6px;
    transition: 100ms background-color;
    background: ${props => props.theme.buttonPrimary};
    :hover {
        background: ${props => props.theme.buttonPrimary_hover};
    }
`;

export const StyledSkypeImg = styled.img`

`;

export const StyledSkypeLabel = styled<StyledThemeProps, "span">("span")`
    font-size: 18px;
    margin-left: 12px;
    color: ${props => props.theme.color};
`;

export const StyledPaymentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 33%;
    justify-content: center;
    align-items: center;
`;

export const StyledPayLabel = styled<StyledThemeProps, "span">("span")`
    opacity: .7;
    font-size: 14px;
`;

export const Horiz = styled<StyledThemeProps, "div">("div")`
    height: 2px;
    width: 78px;
    margin: 8px 0;
    background: ${props => props.theme.colorMain};
`;

export const Vert = styled<StyledThemeProps, "div">("div")`
    height: 2px;
    background: ${props => props.theme.contentBg};
`;

export const HorizPrice = styled<StyledThemeProps, "div">("div")`
    height: 2px;
    width: 100%;
    margin:12px 0;
    background: ${props => props.theme.contentBg};
`;

export const OnePriceBox = styled.div`
    display: flex;
    flex-direction: column;
    min-width: 95px;

    &:first-child{
        margin-right: 16px;
    }
`;

export const AllPriceBox = styled.div`
    display: flex;
`;



