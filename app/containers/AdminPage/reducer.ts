import { fromJS } from "immutable";

import { Action } from "../../basicClasses";
import { SET_LOADED_USERS, SET_LOADED_USER } from "./constants";

const initialState = fromJS({
    initialLoad: false,

    users: false,
    currentPage: 1,
    total: undefined,
    pageSize: undefined,
    activeCount: undefined,
});

function adminReducer(state = initialState, action: Action) {
    switch (action.type) {
        case SET_LOADED_USERS:
            return setLoadedUsers(state, action);
        case SET_LOADED_USER:
            return setLoadedUser(state, action);
        default:
            return state;
    }
}

function setLoadedUsers(state: any, action: Action) {
    const { users, currentPage, pageSize, count, activeCount } = action.payload;
    return state.set("users", users)
        .set("currentPage", currentPage)
        .set("total", parseInt(count, 10))
        .set("pageSize", pageSize)
        .set("initialLoad", true)
        .set("activeCount", parseInt(activeCount, 10));
}

function setLoadedUser(state: any, action: Action) {
    const user = action.payload;

    return state.update("users", (users: any) => {
        const index = users.findIndex((x: any) => x.id === user.id);
        users[index] = user;
        return users;
    });
}

export default adminReducer;
