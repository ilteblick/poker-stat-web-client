import { createSelector } from "reselect";

const selectAdmin = () => (state: any) => {
    return state.get("admin");
};

const selectInitialLoad = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("initialLoad")
);

const selectUsers = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("users")
);

const selectCurrentPage = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("currentPage")
);

const selectTotal = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("total")
);

const selectPageSize = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("pageSize")
);

const selectActiveCount = () => createSelector(
    selectAdmin(),
    (globalState) => globalState.get("activeCount")
);

export {
    selectAdmin,
    selectInitialLoad,
    selectUsers,
    selectTotal,
    selectCurrentPage,
    selectPageSize,
    selectActiveCount,
};
