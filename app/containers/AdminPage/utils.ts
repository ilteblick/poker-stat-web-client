import { Record } from "immutable";

export interface IUserToAdminPage {
    id: number;
    email: string;
    expiration_subscription_date: string;
    subscription_type: number;
    is_admin: boolean;
    count_free_tokens: number;
    inviter: string;
    is_unlimit: boolean;
}

const UserToAdminRecord = Record({
    id: undefined,
    email: undefined,
    expiration_subscription_date: undefined,
    subscription_type: undefined,
    is_admin: false,
    count_free_tokens: undefined,
    inviter: undefined,
    is_unlimit: undefined,
});

export function prepareLoadedUsers(loadedUsers: Array<IUserToAdminPage>) {
    let users: any = false;

    if (loadedUsers && loadedUsers.length > 0) {
        users = loadedUsers.map((x) => {
            const date = new Date(parseInt(x.expiration_subscription_date, 10));
            const dateToDisplay = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
            return new UserToAdminRecord({
                id: x.id,
                email: x.email,
                expiration_subscription_date: dateToDisplay,
                subscription_type: x.subscription_type,
                is_admin: x.is_admin,
                count_free_tokens: x.count_free_tokens,
                inviter: x.inviter,
                is_unlimit: x.is_unlimit,
            });
        });
    }

    return users;
}

export function prepareLoadedUser(loadedUser: IUserToAdminPage) {
    const date = new Date(parseInt(loadedUser.expiration_subscription_date, 10));
    const dateToDisplay = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    return new UserToAdminRecord({
        id: loadedUser.id,
        email: loadedUser.email,
        expiration_subscription_date: dateToDisplay,
        subscription_type: loadedUser.subscription_type,
        is_admin: loadedUser.is_admin,
        count_free_tokens: loadedUser.count_free_tokens,
        inviter: loadedUser.inviter,
        is_unlimit: loadedUser.is_unlimit,
    });
}

