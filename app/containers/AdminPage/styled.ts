import styled from "styled-components";

export const StyledContainer = styled.div`
    padding: 12px;
    margin: 12px;
    border-radius: 4px;
    background: white;
    align-self: flex-start;
    input {
        color: #000;
    }
`;
