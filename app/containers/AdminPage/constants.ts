export const SET_LOADED_USERS = "App/Admin/SET_LOADED_USERS";
export const LOAD_USERS = "App/Admin/LOAD_USERS";

export const SET_LOADED_USER = "App/Admin/SET_LOADED_USER";
