import { ISubscriptionType } from "./containers/SubscriptionBlock/interfaces";

export const SubscriptionTypes: ISubscriptionType[] = [
    {
        key: "subscribe-<-nl-16",
        label: "nl10-n16",
        subscription_type: 0,
    },
    {
        key: "subscribe-<-nl-50",
        label: "nl10-nl50",
        subscription_type: 1,
    },
    {
        key: "subscribe->-nl-10K",
        label: "nl10-nl10K",
        subscription_type: 2,
    }
];

export const SUBSCRIBE_USER = "App/Admin/SUBSCRIBE_USER";
export const MAKE_ADMIN = "App/Admin/MAKE_ADMIN";
export const MAKE_SIMPLE_USER = "App/Admin/MAKE_SIMPLE_USER";
export const GIVE_INVITE_CODES_TO_USER = "App/Admin/GIVE_INVITE_CODES_TO_USER";
export const MAKE_UNLIMIT = "App/Admin/MAKE_UNLIMIT";
export const MAKE_LIMIT = "App/Admin/MAKE_LIMIT";


