import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { Button, InputNumber } from "antd";

import { IUserToAdminPage } from "../../utils";
import { SubscriptionTypes } from "./constants";
import { ISubscriptionType } from "./containers/SubscriptionBlock/interfaces";
import SubscriptionBlock from "./containers/SubscriptionBlock";
import { subscribeUser, makeAdmin, makeSimpleUser, giveInviteCodesToUser, makeUnlimit, makeLimit } from "./actions";


export interface UserListItemProps {
    user: IUserToAdminPage;
    onSubscribeUser?: Function;
    onMakeAdmin?: Function;
    onMakeSimpleUser?: Function;
    onGiveInviteCodesToUser?: Function;
    onMakeUnlimit?: Function;
    onMakeLimit?: Function;
    index: number;
}

interface UserListItemState {
    inviteCodesToGive: number;
}

class UserListItem extends React.PureComponent<UserListItemProps, UserListItemState> {
    constructor(props: UserListItemProps) {
        super(props);

        this.state = {
            inviteCodesToGive: 0,
        };
    }

    onInviteCodesInputType = (value: number) => {
        this.setState({
            inviteCodesToGive: value,
        });
    }

    onSubscribeClick = (type: number, days: number) => {
        const { user, onSubscribeUser } = this.props;
        onSubscribeUser(user.id, type, days);
    }

    onMakeAdminClick = () => {
        const { onMakeAdmin, user } = this.props;
        onMakeAdmin(user.id);
    }

    onMakeSimpleUserClick = () => {
        const { onMakeSimpleUser, user } = this.props;
        onMakeSimpleUser(user.id);
    }

    onGiveInviteCodesToUserClick = () => {
        const { onGiveInviteCodesToUser, user } = this.props;
        const { inviteCodesToGive } = this.state;
        onGiveInviteCodesToUser(user.id, inviteCodesToGive);
    }

    onMakeUnlimitClick = () => {
        const { onMakeUnlimit, user } = this.props;
        onMakeUnlimit(user.id);
    }

    onMakeLimitClick = () => {
        const { onMakeLimit, user } = this.props;
        onMakeLimit(user.id);
    }

    render() {
        const { user, index } = this.props;

        return (
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", flex: 1 }}>
                <span style={{ marginRight: 6 }}>{index}</span>
                <span style={{ marginRight: 6 }}>{user.id}</span>
                <div style={{ display: "flex", flexDirection: "column", width: 250, alignItems: "center" }}>
                    <span>E-mail: {user.email}</span>
                    <span>Subscription: {user.expiration_subscription_date}</span>
                    <span>Subscr. type: {user.subscription_type}</span>
                    <a href={`/user-info/${user.email}`} target="blank">Invites</a>
                </div>
                <div style={{ display: "flex", alignItems: "center", padding: 6, border: "1px solid #e8e8e8", marginRight: 6 }}>
                    <span style={{ padding: 6 }}>{`isAdmin? ${user.is_admin}`}</span>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                        <Button type="primary" onClick={this.onMakeAdminClick}>Make admin</Button>
                        <Button onClick={this.onMakeSimpleUserClick}>Make simple</Button>
                    </div>
                </div>

                <div style={{ display: "flex", alignItems: "center", padding: 6, border: "1px solid #e8e8e8", marginRight: 6 }}>
                    <span style={{ padding: 6 }}>{`isUnlim? ${user.is_unlimit}`}</span>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                        <Button type="primary" onClick={this.onMakeUnlimitClick}>Make unlimit</Button>
                        <Button onClick={this.onMakeLimitClick}>Make limit</Button>
                    </div>
                </div>

                <div style={{ display: "flex", alignItems: "center", padding: 6, border: "1px solid #e8e8e8", marginRight: 6 }}>
                    <span style={{ padding: 6, flex: 1 }}>{`Оставшиеся инвайты:  ${user.count_free_tokens}`}</span>
                    <div style={{ display: "flex", flexDirection: "column", flex: 1 }}>
                        <span>Give invite codes</span>
                        <InputNumber value={this.state.inviteCodesToGive} onChange={this.onInviteCodesInputType} />
                        <Button onClick={this.onGiveInviteCodesToUserClick}>Give</Button>
                    </div>
                </div>

                <span style={{ padding: 6 }}>{`Invited by: ${user.inviter}`}</span>

                {SubscriptionTypes.map((type: ISubscriptionType) => <SubscriptionBlock
                    key={type.key}
                    type={type}
                    onSubscribe={this.onSubscribeClick} />
                )}
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps(dispatch: Function) {
    return {
        onSubscribeUser: (id: string, type: number, days: number) => dispatch(subscribeUser(id, type, days)),
        onMakeAdmin: (id: string) => dispatch(makeAdmin(id)),
        onMakeSimpleUser: (id: string) => dispatch(makeSimpleUser(id)),
        onGiveInviteCodesToUser: (id: string, count: number) => dispatch(giveInviteCodesToUser(id, count)),
        onMakeUnlimit: (id: string) => dispatch(makeUnlimit(id)),
        onMakeLimit: (id: string) => dispatch(makeLimit(id)),
    };
}
const withConnect = connect<{}, {}, UserListItemProps>(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(UserListItem);








