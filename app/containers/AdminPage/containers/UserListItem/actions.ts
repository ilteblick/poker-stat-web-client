import { SUBSCRIBE_USER, MAKE_ADMIN, MAKE_SIMPLE_USER, GIVE_INVITE_CODES_TO_USER, MAKE_UNLIMIT, MAKE_LIMIT } from "./constants";
import { Action } from "../../../../basicClasses";

export function subscribeUser(id: string, type: number, days: number): Action {
    return {
        type: SUBSCRIBE_USER,
        payload: {
            id, type, days
        }
    };
}

export function makeAdmin(id: string): Action {
    return {
        type: MAKE_ADMIN,
        payload: {
            id,
        }
    };
}

export function makeSimpleUser(id: string): Action {
    return {
        type: MAKE_SIMPLE_USER,
        payload: {
            id,
        }
    };
}

export function giveInviteCodesToUser(id: string, count: number): Action {
    return {
        type: GIVE_INVITE_CODES_TO_USER,
        payload: {
            id, count
        }
    };
}

export function makeUnlimit(id: string): Action {
    return {
        type: MAKE_UNLIMIT,
        payload: {
            id,
        }
    };
}

export function makeLimit(id: string): Action {
    return {
        type: MAKE_LIMIT,
        payload: {
            id,
        }
    };
}
