import * as React from "react";
import { InputNumber, Button } from "antd";
import { ISubscriptionType } from "./interfaces";

interface SubscriptionBlockProps {
    type: ISubscriptionType;
    onSubscribe: Function;
}

interface SubscriptionBlockState {
    days: number;
}

class SubscriptionBlock extends React.PureComponent<SubscriptionBlockProps, SubscriptionBlockState> {
    constructor(props: SubscriptionBlockProps) {
        super(props);

        this.state = {
            days: 0,
        };
    }

    onInputType = (value: number) => {
        this.setState({
            days: value
        });
    }

    onClick = () => {
        const { type, onSubscribe } = this.props;
        onSubscribe(type.subscription_type, this.state.days);
    }

    render() {
        const { type }: { type: ISubscriptionType } = this.props;
        return (
            <div style={{ display: "flex", flexDirection: "column" }}>
                <span>{type.label}</span>
                <InputNumber value={this.state.days} onChange={this.onInputType} />
                <Button onClick={this.onClick}>Subscribe</Button>
            </div>
        );
    }
}

export default SubscriptionBlock;
