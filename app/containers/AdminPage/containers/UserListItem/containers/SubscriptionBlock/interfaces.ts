export interface ISubscriptionType {
    key: string;
    label: string;
    subscription_type: number;
}
