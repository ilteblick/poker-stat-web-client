import * as React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { List, Pagination, Input, Button, Checkbox } from "antd";

import reducer from "./reducer";
import saga from "./sagas";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";
import admin from "../../utils/admin";
import { selectInitialLoad, selectUsers, selectCurrentPage, selectTotal, selectPageSize, selectActiveCount } from "./selectors";
import { loadUsers } from "./actions";

import UserListItem from "./containers/UserListItem";
import Select from "../../components/Select";

import { StyledContainer } from "./styled";

interface AdminProps {
    initialLoad: boolean;
    users: any;
    total: number;
    currentPage: number;
    pageSize: number;

    onLoadUsers: Function;
    activeCount: number;
}

interface AdminState {
    nickname: string;
    isShowActive: boolean;
    isShowAdmins: boolean;
    typeToSearch: boolean | number;
}

const TypeOptions = [
    {
        label: "All types",
        value: false,
    }, {
        label: "nl10-nl16",
        value: 0,
    }, {
        label: "nl10-nl50",
        value: 1,
    }, {
        label: "nl10-nl10K",
        value: 2,
    }
];

class AdminPage extends React.PureComponent<AdminProps, AdminState> {
    constructor(props: AdminProps) {
        super(props);

        this.state = {
            nickname: "",
            isShowActive: false,
            isShowAdmins: false,
            typeToSearch: false,
        };
    }

    onPageClick = (pageNumber: number) => {
        const { isShowActive, isShowAdmins, nickname, typeToSearch } = this.state;
        this.props.onLoadUsers(pageNumber, nickname, isShowActive, isShowAdmins, typeToSearch);
    }

    onInputType = (e: any) => {
        this.setState({
            nickname: e.target.value,
        });
    }

    onFindClick = () => {
        const { isShowActive, isShowAdmins, nickname, typeToSearch } = this.state;
        this.props.onLoadUsers(1, nickname, isShowActive, isShowAdmins, typeToSearch);
    }

    onChangeActive = () => {
        this.setState((state) => ({ isShowActive: !state.isShowActive }));
    }

    onChangeAdmins = () => {
        this.setState((state) => ({ isShowAdmins: !state.isShowAdmins }));
    }

    onTypeChange = (type: any) => {
        this.setState({
            typeToSearch: type.value,
        });
    }

    render() {
        const { initialLoad, users, currentPage, total, pageSize, activeCount } = this.props;
        const { isShowActive, isShowAdmins, nickname } = this.state;

        return (
            <StyledContainer>
                {initialLoad ?
                    [<div key="users-find" style={{ margin: "12px 0", display: "flex", width: 900, alignItems: "center" }}>
                        <Input
                            value={nickname}
                            onChange={this.onInputType}
                        />
                        <Button onClick={this.onFindClick}>Find</Button>

                        <span style={{ marginLeft: 6 }}>Active:{activeCount}/{total}</span>

                        <div style={{ marginLeft: 6, display: "flex" }}>
                            <Checkbox
                                value={isShowActive}
                                onChange={this.onChangeActive}
                            />
                            <span>Active</span>
                        </div>
                        <div style={{ marginLeft: 6, display: "flex" }}>
                            <Checkbox
                                value={isShowAdmins}
                                onChange={this.onChangeAdmins}
                            />
                            <span>Admins</span>
                        </div>
                        <Select
                            options={TypeOptions}
                            defaultValue={TypeOptions[0]}
                            onChange={this.onTypeChange}
                        />
                    </div>,
                        <List
                            key="users-list"
                            bordered
                            dataSource={users}
                            renderItem={(item: any, index: number) => (<List.Item key={item.id}>
                                <UserListItem user={item} index={index + 1}/></List.Item>)}
                        />,
                        <Pagination
                            key="users-pagination"
                            current={currentPage}
                            total={total}
                            defaultPageSize={pageSize}
                            onChange={this.onPageClick}/>]
                    : null}
            </StyledContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    initialLoad: selectInitialLoad(),
    users: selectUsers(),
    currentPage: selectCurrentPage(),
    total: selectTotal(),
    pageSize: selectPageSize(),
    activeCount: selectActiveCount(),
});

function mapDispatchToProps(dispach: any) {
    return {
        onLoadUsers: (page: number, nickname: string,
                      isShowActive: boolean, isShowAdmins: boolean,
                      typeToSearch: boolean | number) => dispach(loadUsers(page, nickname, isShowActive, isShowAdmins, typeToSearch)),
    };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "admin", reducer });
const withSaga = injectSaga({ key: "admin", saga });

const withAdmin = admin();

export default withRouter(compose(
    withAdmin,
    withReducer,
    withSaga,
    withConnect,
)(AdminPage) as any);
