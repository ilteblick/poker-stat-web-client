import { List } from "lodash";
import { Action } from "../../basicClasses";
import { SET_LOADED_USERS, LOAD_USERS, SET_LOADED_USER } from "./constants";

export function setLoadedUsers(users: List<any>, currentPage: number, pageSize: number, count: number,
    activeCount: number): Action {
    return {
        type: SET_LOADED_USERS,
        payload: {
            users,
            currentPage,
            pageSize,
            count,
            activeCount,
        }
    };
}

export function loadUsers(page: number, nickname: string, isShowActive: boolean, isShowAdmins: boolean,
    typeToSearch: boolean | number): Action {
    return {
        type: LOAD_USERS,
        payload: {
            page,
            nickname,
            isShowActive,
            isShowAdmins,
            typeToSearch,
        }
    };
}

export function setLoadedUser(user: any): Action {
    return {
        type: SET_LOADED_USER,
        payload: user
    };
}
