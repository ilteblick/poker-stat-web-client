import { put, race, take, fork } from "redux-saga/effects";
import { message } from "antd";

import { apiService } from "../../api/actionService/api-service";
import { prepareLoadedUsers, prepareLoadedUser } from "./utils";
import { setLoadedUsers, setLoadedUser } from "./actions";
import { LOAD_USERS } from "./constants";
import { SUBSCRIBE_USER, MAKE_ADMIN, MAKE_SIMPLE_USER, GIVE_INVITE_CODES_TO_USER, MAKE_LIMIT, MAKE_UNLIMIT } from "./containers/UserListItem/constants";

const pageSize = 10;
const adminPrefix = "admin";
const users_url_segment = `${adminPrefix}/users`;
const subscribe_url_segment = `${adminPrefix}/subscribe`;
const make_admin_url_segment = `${adminPrefix}/make_admin`;
const make_simple_user_url_segment = `${adminPrefix}/make_simple_user`;
const give_invite_code_url_segment = `${adminPrefix}/create_invite_code`;
const make_unlimit_url_segment = `${adminPrefix}/make_unlimit`;
const make_limit_url_segment = `${adminPrefix}/make_limit`;

const getUsersLyfecycle = apiService.findLifecycle(users_url_segment);
const subscribeLyfecycle = apiService.postLifecycle(subscribe_url_segment);
const makeAdminLyfecycle = apiService.postLifecycle(make_admin_url_segment);
const makeSimpleUserLyfecycle = apiService.postLifecycle(make_simple_user_url_segment);
const giveInviteCodeLyfecycle = apiService.postLifecycle(give_invite_code_url_segment);
const makeLimitLyfecycle = apiService.postLifecycle(make_limit_url_segment);
const makeUnlimitLyfecycle = apiService.postLifecycle(make_unlimit_url_segment);

function* watchLoadUsers() {
    while (true) {
        const { payload } = yield take(LOAD_USERS);

        const { page, nickname, isShowActive, isShowAdmins, typeToSearch } = payload;

        const query = {
            page,
            nickname,
            pageSize,
            isShowAdmins,
            isShowActive,
            typeToSearch
        };

        yield put(apiService.find(users_url_segment, query));

        const { usersFindSuccessFromApi, usersFindErrorFromApi } = yield race({
            usersFindSuccessFromApi: take(getUsersLyfecycle.RESOLVED),
            usersFindErrorFromApi: take(getUsersLyfecycle.REJECTED),
        });

        if (usersFindErrorFromApi) {
            message.error(usersFindErrorFromApi.error.message);
        }

        if (usersFindSuccessFromApi) {
            const preparedData = prepareLoadedUsers(usersFindSuccessFromApi.payload.users);
            yield put(setLoadedUsers(preparedData, page, pageSize, usersFindSuccessFromApi.payload.count,
                usersFindSuccessFromApi.payload.activeCount));
        }
    }
}

function* watchSubscribeUser() {
    while (true) {
        const { payload } = yield take(SUBSCRIBE_USER);

        const { id, type, days } = payload;

        const body = {
            id,
            type,
            days
        };

        yield put(apiService.post(subscribe_url_segment, body));

        const { subscribeUserSuccessFromApi, subscribeUserErrorFromApi } = yield race({
            subscribeUserSuccessFromApi: take(subscribeLyfecycle.RESOLVED),
            subscribeUserErrorFromApi: take(subscribeLyfecycle.REJECTED),
        });

        if (subscribeUserErrorFromApi) {
            // console.log(subscribeUserErrorFromApi);
            message.error(subscribeUserErrorFromApi.error.message);
        }

        if (subscribeUserSuccessFromApi) {
            const preparedData = prepareLoadedUser(subscribeUserSuccessFromApi.payload);
            yield put(setLoadedUser(preparedData));
        }
    }
}

function* watchMakeAdmin() {
    while (true) {
        const { payload } = yield take(MAKE_ADMIN);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_admin_url_segment, body));

        const { makeAdminFromApi, makeAdminErrorFromApi } = yield race({
            makeAdminFromApi: take(makeAdminLyfecycle.RESOLVED),
            makeAdminErrorFromApi: take(makeAdminLyfecycle.REJECTED),
        });

        if (makeAdminErrorFromApi) {
            message.error(makeAdminErrorFromApi.error.message);
        }

        if (makeAdminFromApi) {
            const preparedData = prepareLoadedUser(makeAdminFromApi.payload);
            yield put(setLoadedUser(preparedData));
        }
    }
}

function* watchGiveInviteCodes() {
    while (true) {
        const { payload } = yield take(GIVE_INVITE_CODES_TO_USER);
        const { id, count } = payload;

        if (count > 0) {
            const body = {
                id,
                count
            };

            yield put(apiService.post(give_invite_code_url_segment, body));

            const { giveInviteCodeFromApi, giveInviteCodeErrorFromApi } = yield race({
                giveInviteCodeFromApi: take(giveInviteCodeLyfecycle.RESOLVED),
                giveInviteCodeErrorFromApi: take(giveInviteCodeLyfecycle.REJECTED),
            });

            if (giveInviteCodeErrorFromApi) {
                message.error(giveInviteCodeErrorFromApi.error.message);
            }

            if (giveInviteCodeFromApi) {
                const preparedData = prepareLoadedUser(giveInviteCodeFromApi.payload);
                yield put(setLoadedUser(preparedData));
            }
        }
    }
}

function* watchMakeSimpleUser() {
    while (true) {
        const { payload } = yield take(MAKE_SIMPLE_USER);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_simple_user_url_segment, body));

        const { makeSimpleUserFromApi, makeSimpleUserErrorFromApi } = yield race({
            makeSimpleUserFromApi: take(makeSimpleUserLyfecycle.RESOLVED),
            makeSimpleUserErrorFromApi: take(makeSimpleUserLyfecycle.REJECTED),
        });

        if (makeSimpleUserErrorFromApi) {
            message.error(makeSimpleUserErrorFromApi.error.message);
        }

        if (makeSimpleUserFromApi) {
            const preparedData = prepareLoadedUser(makeSimpleUserFromApi.payload);
            yield put(setLoadedUser(preparedData));
        }
    }
}

function* watchMakeUnlimit() {
    while (true) {
        const { payload } = yield take(MAKE_UNLIMIT);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_unlimit_url_segment, body));

        const { makeAdminFromApi, makeAdminErrorFromApi } = yield race({
            makeAdminFromApi: take(makeUnlimitLyfecycle.RESOLVED),
            makeAdminErrorFromApi: take(makeUnlimitLyfecycle.REJECTED),
        });

        if (makeAdminErrorFromApi) {
            message.error(makeAdminErrorFromApi.error.message);
        }

        if (makeAdminFromApi) {
            const preparedData = prepareLoadedUser(makeAdminFromApi.payload);
            yield put(setLoadedUser(preparedData));
        }
    }
}

function* watchMakeLimit() {
    while (true) {
        const { payload } = yield take(MAKE_LIMIT);
        const { id } = payload;

        const body = {
            id,
        };

        yield put(apiService.post(make_limit_url_segment, body));

        const { makeAdminFromApi, makeAdminErrorFromApi } = yield race({
            makeAdminFromApi: take(makeLimitLyfecycle.RESOLVED),
            makeAdminErrorFromApi: take(makeLimitLyfecycle.REJECTED),
        });

        if (makeAdminErrorFromApi) {
            message.error(makeAdminErrorFromApi.error.message);
        }

        if (makeAdminFromApi) {
            const preparedData = prepareLoadedUser(makeAdminFromApi.payload);
            yield put(setLoadedUser(preparedData));
        }
    }
}

export default function* adminSaga(): IterableIterator<any> {
    const query = {
        page: 1,
        pageSize,
        nickname: ""
    };

    yield put(apiService.find(users_url_segment, query));

    const { usersFindSuccessFromApi, usersFindErrorFromApi } = yield race({
        usersFindSuccessFromApi: take(getUsersLyfecycle.RESOLVED),
        usersFindErrorFromApi: take(getUsersLyfecycle.REJECTED),
    });

    if (usersFindErrorFromApi) {
        message.error(usersFindErrorFromApi.error.message);
    }

    if (usersFindSuccessFromApi) {
        const preparedData = prepareLoadedUsers(usersFindSuccessFromApi.payload.users);
        yield put(setLoadedUsers(preparedData, 1, pageSize, usersFindSuccessFromApi.payload.count,
            usersFindSuccessFromApi.payload.activeCount));
    }

    yield fork(watchLoadUsers);
    yield fork(watchSubscribeUser);
    yield fork(watchMakeAdmin);
    yield fork(watchMakeSimpleUser);
    yield fork(watchGiveInviteCodes);
    yield fork(watchMakeUnlimit);
    yield fork(watchMakeLimit);
}
