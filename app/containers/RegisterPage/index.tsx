import * as React from "react";
import { withRouter } from "react-router-dom";
import { connect, Dispatch } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Actions } from "./actions";
import { selectEmail, selectPass, selectConfirmPass, selectInviteCode } from "./selectors";

import reducer from "./reducer";
import saga from "./sagas";

import injectReducer from "../../utils/injectReducer";
import injectSaga from "../../utils/injectSaga";

import Input from "../../components/Input";
import { Button, message } from "antd";
import { StyledTitle, StyledLabel, StyledContainer, CenteredContainer } from "../../components/FormsItems";

import {
    registerPageEmailFieldValidator, registerPageRequiredValidator,
    registerPageComparePasswordValidator, registerPageInviteCodeValidator
} from "../../utils/validators";

import { OwnRegisterPageProps, StateProps, DispatchProps, Props } from "./types";


export function callEmailError(msg: string){


message.error(<>{msg}<a href="skype:live:h4h_supp_eng?chat">Contact support</a></>, 15);
}

class RegisterPage extends React.PureComponent<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    onRegisterPageEmail = (value: string) => {
        this.props.onChangeEmail(value);
    }

    onRegisterPagePassword = (value: string) => {
        this.props.onChangePassword(value);
    }

    onRegisterPageConfirmPassword = (value: string) => {
        this.props.onChangeConfirmPassword(value);
    }

    onRegisterPageInviteCode = (value: string) => {
        this.props.onChangeInviteCode(value);
    }

    onRegistrationClick = () => {
        const { email, pass, confirm, inviteCode } = this.props;
        this.props.onRegistration(email, pass, confirm, inviteCode);
    }


    render() {
        const { email, pass, confirm, inviteCode } = this.props;
        return (
            <CenteredContainer>
                <StyledContainer>
                    <StyledTitle>Registration</StyledTitle>
                    <StyledLabel>Enter your details below</StyledLabel>
                    <Input
                        label="E-mail"
                        value={email}
                        onChange={this.onRegisterPageEmail}
                        finalValidate={registerPageEmailFieldValidator}
                    />
                    <Input
                        label="Password"
                        value={pass}
                        isPass
                        onChange={this.onRegisterPagePassword}
                        finalValidate={registerPageRequiredValidator}
                    />
                    <Input
                        label="Confirm password"
                        value={confirm}
                        isPass
                        onChange={this.onRegisterPageConfirmPassword}
                        finalValidate={(confirmPass: string) => registerPageComparePasswordValidator(pass, confirmPass)}
                    />
                    <Input
                        label="Invition code (not necessary)"
                        value={inviteCode}
                        onChange={this.onRegisterPageInviteCode}
                        // finalValidate={registerPageInviteCodeValidator}
                    />
                    <Button
                        size="large"
                        onClick={this.onRegistrationClick}
                        type="primary"
                        style={{
                            width: "240px", height: "60px",
                            fontSize: "20px", borderRadius: "6px",
                            marginTop: 12, alignSelf: "flex-end"
                        }}>
                        Register
                            </Button>
                </StyledContainer>
            </CenteredContainer>
        );
    }
}

const mapStateToProps = createStructuredSelector<any, StateProps>({
    email: selectEmail(),
    pass: selectPass(),
    confirm: selectConfirmPass(),
    inviteCode: selectInviteCode(),
});

function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
    return {
        onChangeEmail: (email: string) => dispatch(Actions.changeEmail(email)),
        onChangePassword: (pass: string) => dispatch(Actions.changePassword(pass)),
        onChangeConfirmPassword: (confirm: string) => dispatch(Actions.changeConfirmPassword(confirm)),
        onChangeInviteCode: (inviteCode: string) => dispatch(Actions.changeInviteCode(inviteCode)),
        onRegistration: (email: string, password: string, confirm: string, token: string) =>
            dispatch(Actions.registration(email, password, confirm, token)),
        dispatch,
    };
}

const withConnect = connect<StateProps, DispatchProps, OwnRegisterPageProps>(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "register", reducer });
const withSaga = injectSaga({ key: "register", saga });

export default withRouter(compose(
    withReducer,
    withSaga,
    withConnect,
)(RegisterPage) as any);
