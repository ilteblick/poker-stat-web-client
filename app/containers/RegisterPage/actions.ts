import { ActionsUnion, createAction } from "../../utilsTypes/redux/actionUtils";

export const CHANGE_EMAIL = "RegisterPage/CHANGE_EMAIL";
export const CHANGE_PASSWORD = "RegisterPage/CHANGE_PASSWORD";
export const CONFIRM_PASSWORD = "RegisterPage/CONFIRM_PASSWORD";
export const INVITE_CODE = "RegisterPage/INVITE_CODE";
export const REGISTER = "RegisterPage/REGISTER";

export const Actions = {
    changeEmail: (email: string) => createAction(CHANGE_EMAIL, { email }),
    changePassword: (pass: string) => createAction(CHANGE_PASSWORD, { pass }),
    changeConfirmPassword: (confirm: string) => createAction(CONFIRM_PASSWORD, { confirm }),
    changeInviteCode: (inviteCode: string) => createAction(INVITE_CODE, { inviteCode }),
    registration: (email: string, password: string, confirm: string, token: string) =>
        createAction(REGISTER, { email, password, confirm, token }),
};

export type Actions = ActionsUnion<typeof Actions>;
