import * as registerPageActions from "../actions";
import { RegisterPageState } from "./types";

let initialState = new RegisterPageState();

function registerPageReducer(state = initialState, action: registerPageActions.Actions) {
    switch (action.type) {
        case registerPageActions.CHANGE_EMAIL:
            return state.set("email", action.payload.email);
        case registerPageActions.CHANGE_PASSWORD:
            return state.set("password", action.payload.pass);
        case registerPageActions.CONFIRM_PASSWORD:
            return state.set("confirmPass", action.payload.confirm);
        case registerPageActions.INVITE_CODE:
            return state.set("inviteCode", action.payload.inviteCode);
        default:
            return state;
    }
}


export default registerPageReducer;
