import { RecordFactory } from "../../../utilsTypes/immutableUtils/RecordFactory";
import { BasePageReducerState } from "../../../types/redux/BasePageReducerState";

export interface IRegisterPageState extends BasePageReducerState {
    email: string;
    password: string;
    confirmPass: string;
    inviteCode: string;
}

const registerPageState = RecordFactory<IRegisterPageState>({
    email: "",
    password: "",
    confirmPass: "",
    inviteCode: "",
});

export class RegisterPageState extends registerPageState {
}
