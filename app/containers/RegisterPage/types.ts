import { BaseDispatchProps } from "../../types/redux/BaseDispatchProps";
import { BasePageStateProps, BasePageOwnProps } from "../../types/redux/BasePageProps";

export interface OwnRegisterPageProps extends BasePageOwnProps {
    email: string;
    pass: string;
    confirm: string;
    inviteCode: string;
    onChangeEmail?: Function;
    onChangePassword?: Function;
    onChangeConfirmPassword?: Function;
    onChangeInviteCode?: Function;
    onRegistration?: Function;
}

export interface StateProps extends BasePageStateProps {
    email: string;
    pass: string;
    confirm: string;
    inviteCode: string;
}

export interface DispatchProps extends BaseDispatchProps {
    onChangeEmail: (email: string) => any;
    onChangePassword: (pass: string) => any;
    onChangeConfirmPassword: (confirm: string) => any;
    onChangeInviteCode: (inviteCode: string) => any;
    onRegistration: (email: string, password: string, confirm: string, token: string) => any;
}

export type Props = StateProps & DispatchProps & OwnRegisterPageProps;
