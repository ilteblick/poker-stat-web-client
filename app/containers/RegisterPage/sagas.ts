import { fork, take, put, race } from "redux-saga/effects";
import { apiService } from "../../api/actionService/api-service";
import * as ApiActions from "./actions";
import { ActionByType } from "../../utilsTypes/redux/actionUtils";
import { message } from "antd";
import { push } from "react-router-redux";
import { callEmailError } from ".";

const register_url_segment = "register";

const registerLyfecycle = apiService.postLifecycle(register_url_segment);

function* watchRegister() {
    while (true) {
        const { payload } = (yield take(ApiActions.REGISTER)) as
            ActionByType<ApiActions.Actions, typeof ApiActions.REGISTER>;

        yield put(apiService.post(register_url_segment, payload));

        const { error } = yield race({
            result: take(registerLyfecycle.RESOLVED),
            error: take(registerLyfecycle.REJECTED),
        });

        if (error) {
            if(error.error.showSkype === 'yes'){
                callEmailError(error.error.message);
            } else {
                message.error(error.error.message);
            }
            
            continue;
        }

        message.success(`Registration was completed successfully. Please check the email "spam"`);
        yield put(push("/"));
    }
}




export default function* registerSaga() {
    yield fork(watchRegister);
}
