import { createSelector } from "reselect";
import { IRegisterPageState } from "./reducer/types";

const selectRegisterPageState = (state: any): IRegisterPageState => state.get("register");

const selectEmail = () => createSelector(
    selectRegisterPageState,
    (registerPageState) => registerPageState.email
);

const selectPass = () => createSelector(
    selectRegisterPageState,
    (registerPageState) => registerPageState.password
);

const selectConfirmPass = () => createSelector(
    selectRegisterPageState,
    (registerPageState) => registerPageState.confirmPass
);

const selectInviteCode = () => createSelector(
    selectRegisterPageState,
    (registerPageState) => registerPageState.inviteCode
);

export {
    selectRegisterPageState,
    selectEmail,
    selectPass,
    selectConfirmPass,
    selectInviteCode,
};