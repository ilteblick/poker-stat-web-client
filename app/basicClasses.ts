export interface Action {
    type: string;
    payload?: any;
    error?: any;
    showSpinner?: boolean;
}
