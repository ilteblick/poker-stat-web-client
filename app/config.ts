let server;
switch (process.env.NODE_ENV) {
    case "development": {
        server = "http://localhost:3001/api";
        // server = "https://hand4hand.info/api";
        break;
    }
    case "test": {
        server = "http://85.93.145.2/api";
        break;
    }
    case "production": {
        server = "https://hand4hand.info/api";
        break;
    }
    default:
        server = "https://hand4hand.info/api";
}

export const SERVER_ADDRESSES = {
    DEFAULT: server,
};
