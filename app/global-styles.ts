import { createGlobalStyle } from "styled-components";
// import Font from "./Fonts/Roboto-Regular.ttf";
import { GlobalDarkStyle, GlobalLightStyle } from "./utils/styleThemes";

export interface GlobalStyleProps {
  theme: string;
}

export const GlobalStyle = createGlobalStyle`
    body {
      font-family: 'Roboto', sans-serif;
      height: 100%;
    }
    #app {
      height: 100%;
    }

    .ant-layout-header {
        padding: 0 120px;

        @media (max-width: 1400px){
          padding: 0 60px;
        }

        @media (max-width: 1250px){
          padding: 0 40px;
        }

        @media (max-width: 1050px){
          padding: 0 15px;
        }

        @media (max-width: 900px){
          padding-right: 0;
        }
    }

    ${(props: GlobalStyleProps) => props.theme === "light" ? GlobalLightStyle : GlobalDarkStyle}
`;
