const path = require('path');
const webpack = require('webpack');

module.exports = (options) => ({
    mode: options.mode,
    entry: options.entry,
    output: Object.assign({
        path: path.resolve(process.cwd(), 'dist'),
        publicPath: '/',
    }, options.output),

    optimization: options.optimization,
    devtool: options.devtool,
    target: 'web',

    resolve: {
        modules: [
            'node_modules',
            'app',
        ],
        extensions: [
            '.ts',
            '.tsx',
            '.js',
            '.jsx',
            '.react.js',
        ],
        mainFields: [
            'browser',
            'jsnext:main',
            'main',
        ],
    },

    node: {
        fs: 'empty',
    },

    module: {
        rules: [
            {
                test: /\.(ts|tsx)?$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                'react',
                                'stage-0',
                            ],

                            plugins: [].concat(options.babelPlugins),
                        },
                    },
                    {
                        loader: 'ts-loader',
                        options: options.tsLoaderOptions,
                    },
                ],
            },
            {
                // Preprocess our own .css files
                // This is the place to add your own loaders (e.g. sass/less etc.)
                // for a list of loaders, see https://webpack.js.org/loaders/#styling
                test: /\.css$/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader'],
            },
            {
                // Preprocess 3rd party .css files located in node_modules
                test: /\.css$/,
                include: /node_modules/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(jpg|png|gif|eot|otf|ttf|woff|woff2|svg)$/,
                use: 'file-loader',
            },
            // {
            //     test: /\.(jpg|png|gif)$/,
            //     use: [
            //         {
            //             loader: 'url-loader',
            //             options: {
            //                 limit: 10000,
            //             },
            //         },
            //         {
            //             loader: 'image-webpack-loader',
            //             options: {
            //                 mozjpeg: {
            //                     progressive: true,
            //                 },
            //                 gifsicle: {
            //                     interlaced: false,
            //                 },
            //                 optipng: {
            //                     optimizationLevel: 7,
            //                 },
            //                 pngquant: {
            //                     quality: '65-90',
            //                     speed: 4,
            //                 },
            //             },
            //         },
            //     ],
            // },
        ],
    },
    plugins: options.plugins.concat([
        // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
        // inside your code for any environment checks; UglifyJS will automatically
        // drop any unreachable code.
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),
    ]),
    performance: options.performance || {},
});
